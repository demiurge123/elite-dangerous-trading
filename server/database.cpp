#include <QFile>
#include <QSqlError>
#include <QSqlQuery>
#include <QVariant>
#include "database.h"

#include "QLogger.h"

Database *Database::_instance = NULL;
const char databaseName[]="starsystems.sqlite";

Database::~Database()
{
    QSqlDatabase::removeDatabase(databaseName);
}

QStringList Database::getAllNameCommodities()
{
    QStringList list;
    QSqlQuery query;
    query.prepare("SELECT name FROM Commodities ORDER BY name;");
    query.exec();
    while (query.next()) {
        QString name = query.value(0).toString();
        list.append(name);
    }
    return list;
}

QStringList Database::getAllNameStations()
{
    QStringList list;
    QSqlQuery query;
    query.prepare("SELECT st.id, st.name, sy.name FROM Stations AS st, Systems AS sy WHERE st.system=sy.id ORDER BY st.name;");
    query.exec();
    while (query.next()) {
        int id = query.value(0).toInt();
        QString station = query.value(1).toString();
        QString system = query.value(2).toString();
        list.append(QString::number(id)+"|"+station+"|"+system);
    }
    return list;
}

QStringList Database::getAllNameSystems()
{
    QStringList list;
    QSqlQuery query;
    query.prepare("SELECT name FROM Systems ORDER BY name;");
    query.exec();
    while (query.next()) {
        QString name = query.value(0).toString();
        list.append(name);
    }
    return list;
}

QStringList Database::getAllCommoditiesInStation(const int &index)
{
    QStringList list;
    QSqlQuery query;

    query.prepare("SELECT max(timestamp) AS max_timestamp, com.name, sellPrice, buyPrice, supply FROM Market AS mar, Commodities AS com WHERE mar.station=:station AND com.id=mar.commodity GROUP BY commodity ORDER BY com.name;");
    query.bindValue(":station",index);
    query.exec();
    while (query.next()) {
        QString timestamp = query.value(0).toString();
        QString name = query.value(1).toString();
        int sellPrice = query.value(2).toInt();
        int buyPrice = query.value(3).toInt();
        int supply = query.value(4).toInt();
        list.append(timestamp+"|"+name+"|"+QString::number(buyPrice)+"|"+QString::number(sellPrice)+"|"+QString::number(supply));
    }
    return list;
}

QMap<QString, int> Database::getlistAllAvgPrices()
{
    QMap<QString, int> avgPrices;
    QSqlQuery query;

    query.exec("SELECT name, price FROM Commodities, AVGPrices WHERE Commodities.id=AVGPrices.commodity;");
    while (query.next()) {
        QString commodity = query.value(0).toString();
        int avgPrice = query.value(1).toInt();
        avgPrices.insert(commodity,avgPrice);
    }
    return avgPrices;
}

bool Database::registration(const QString &station, const QString &commodity, const int &sell, const int &buy, const int &supply)
{
    bool isAddRecord=false;
    QSqlQuery query;
    if (m_db.transaction()) {
        query.prepare("SELECT id FROM Stations WHERE name=:name;");
        query.bindValue(":name",station);
        query.exec();
        int id_station=0;
        if (query.next()) {
            id_station = query.value(0).toInt();
        }

        query.prepare("SELECT id FROM Commodities WHERE name=:name;");
        query.bindValue(":name",commodity);
        query.exec();
        int id_commodity=0;
        if (query.next()) {
            id_commodity = query.value(0).toInt();
        }

        if (id_station>0 && id_commodity>0) {
            query.prepare("SELECT max(timestamp) as max_time, sellPrice, buyPrice, supply FROM Market WHERE station=:idStation AND commodity=:idCommodity ORDER BY max_time DESC LIMIT 1;");
            query.bindValue(":idStation",id_station);
            query.bindValue(":idCommodity",id_commodity);
            query.exec();
            int lastSell=sell;
            int lastBuy=buy;
            int lastSupply=supply;

            if (query.next()) {
                lastSell = query.value(1).toInt();
                lastBuy = query.value(2).toInt();
                lastSupply = query.value(3).toInt();
            }

            if (sell!=lastSell || buy!=lastBuy || supply!=lastSupply) {
                query.prepare("INSERT INTO Market(station,commodity,sellPrice,buyPrice,supply) VALUES (?,?,?,?,?)");
                query.addBindValue(id_station);
                query.addBindValue(id_commodity);
                query.addBindValue(sell);
                query.addBindValue(buy);
                query.addBindValue(supply);

                isAddRecord=query.exec();
            }
        }
        m_db.commit();
    }
    return isAddRecord;
}

Database::Database()
{
    bool existingData = QFile::exists(databaseName);
    if (createConnection()&&!existingData)
        createData();
}

bool Database::createConnection()
{
    m_db = QSqlDatabase::addDatabase("QSQLITE");

    m_db.setDatabaseName(databaseName);
    if (!m_db.open()) {
        QLogger::QLog_Warning("Database", "Error connection with database: " + m_db.lastError().text());
        return false;
    }
    return true;
}

void Database::createData()
{
    QSqlQuery query;

    if (m_db.transaction()) {
        query.exec("CREATE TABLE Commodities (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, name TEXT NOT NULL UNIQUE);");
        query.exec("CREATE TABLE Market (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, station INTEGER REFERENCES Stations (id), commodity INTEGER REFERENCES Commodities (id), sellPrice INTEGER, buyPrice INTEGER, supply INTEGER, timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL);");
        query.exec("CREATE TABLE Stations (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, name TEXT  NOT NULL, system INTEGER REFERENCES Systems (id));");
        query.exec("CREATE TABLE Systems (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, name TEXT NOT NULL UNIQUE);");
        query.exec("CREATE TABLE AVGPrices (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, commodity INTEGER REFERENCES Commodities (id), price INTEGER);");
        query.exec("CREATE INDEX idx_commodities1 ON Commodities(name);");
        query.exec("CREATE INDEX idx_commodities2 ON Commodities(id);");
        query.exec("CREATE INDEX idx_market ON Market(station, commodity);");
        query.exec("CREATE INDEX idx_stations1 ON Stations(name);");
        query.exec("CREATE INDEX idx_stations2 ON Stations(system);");
        query.exec("CREATE INDEX idx_systems ON Systems(id);");

        query.exec("INSERT INTO Commodities ( id,name ) VALUES (  1, 'Advanced Catalysers' );");
        query.exec("INSERT INTO Commodities ( id,name ) VALUES (  2, 'Agri-Medicines' );");
        query.exec("INSERT INTO Commodities ( id,name ) VALUES (  3, 'Algae' );");
        query.exec("INSERT INTO Commodities ( id,name ) VALUES (  4, 'Alloys' );");
        query.exec("INSERT INTO Commodities ( id,name ) VALUES (  5, 'Aluminium' );");
        query.exec("INSERT INTO Commodities ( id,name ) VALUES (  6, 'Animal Meat' );");
        query.exec("INSERT INTO Commodities ( id,name ) VALUES (  7, 'Animal Monitors' );");
        query.exec("INSERT INTO Commodities ( id,name ) VALUES (  8, 'Aquaponic Systems' );");
        query.exec("INSERT INTO Commodities ( id,name ) VALUES (  9, 'Auto-Fabricators' );");
        query.exec("INSERT INTO Commodities ( id,name ) VALUES ( 10, 'Basic Medicines' );");
        query.exec("INSERT INTO Commodities ( id,name ) VALUES ( 11, 'Bauxite' );");
        query.exec("INSERT INTO Commodities ( id,name ) VALUES ( 12, 'Bertrandite' );");
        query.exec("INSERT INTO Commodities ( id,name ) VALUES ( 13, 'Bioreducing Lichen' );");
        query.exec("INSERT INTO Commodities ( id,name ) VALUES ( 14, 'Biowaste' );");
        query.exec("INSERT INTO Commodities ( id,name ) VALUES ( 15, 'Clothing' );");
        query.exec("INSERT INTO Commodities ( id,name ) VALUES ( 16, 'Cobalt' );");
        query.exec("INSERT INTO Commodities ( id,name ) VALUES ( 17, 'Coffee' );");
        query.exec("INSERT INTO Commodities ( id,name ) VALUES ( 18, 'Coltan' );");
        query.exec("INSERT INTO Commodities ( id,name ) VALUES ( 19, 'Combat Stabilisers' );");
        query.exec("INSERT INTO Commodities ( id,name ) VALUES ( 20, 'Computer Components' );");
        query.exec("INSERT INTO Commodities ( id,name ) VALUES ( 21, 'Consumer Technology' );");
        query.exec("INSERT INTO Commodities ( id,name ) VALUES ( 22, 'Cotton' );");
        query.exec("INSERT INTO Commodities ( id,name ) VALUES ( 23, 'Crop Harvesters' );");
        query.exec("INSERT INTO Commodities ( id,name ) VALUES ( 24, 'Dom. Appliances' );");
        query.exec("INSERT INTO Commodities ( id,name ) VALUES ( 25, 'Explosives' );");
        query.exec("INSERT INTO Commodities ( id,name ) VALUES ( 26, 'Fish' );");
        query.exec("INSERT INTO Commodities ( id,name ) VALUES ( 27, 'Food Cartridges' );");
        query.exec("INSERT INTO Commodities ( id,name ) VALUES ( 28, 'Gallite' );");
        query.exec("INSERT INTO Commodities ( id,name ) VALUES ( 29, 'Gold' );");
        query.exec("INSERT INTO Commodities ( id,name ) VALUES ( 30, 'Grain' );");
        query.exec("INSERT INTO Commodities ( id,name ) VALUES ( 31, 'H.E. Suits' );");
        query.exec("INSERT INTO Commodities ( id,name ) VALUES ( 32, 'Hel-Static Furnaces' );");
        query.exec("INSERT INTO Commodities ( id,name ) VALUES ( 33, 'Hydrogen Fuels' );");
        query.exec("INSERT INTO Commodities ( id,name ) VALUES ( 34, 'Indite' );");
        query.exec("INSERT INTO Commodities ( id,name ) VALUES ( 35, 'Leather' );");
        query.exec("INSERT INTO Commodities ( id,name ) VALUES ( 36, 'Lepidolite' );");
        query.exec("INSERT INTO Commodities ( id,name ) VALUES ( 37, 'Liquor' );");
        query.exec("INSERT INTO Commodities ( id,name ) VALUES ( 38, 'Marine Supplies' );");
        query.exec("INSERT INTO Commodities ( id,name ) VALUES ( 39, 'Mineral Extractors' );");
        query.exec("INSERT INTO Commodities ( id,name ) VALUES ( 40, 'Mineral Oil' );");
        query.exec("INSERT INTO Commodities ( id,name ) VALUES ( 41, 'Narcotics' );");
        query.exec("INSERT INTO Commodities ( id,name ) VALUES ( 42, 'Non-Lethal Wpns' );");
        query.exec("INSERT INTO Commodities ( id,name ) VALUES ( 43, 'Performance Enhancers' );");
        query.exec("INSERT INTO Commodities ( id,name ) VALUES ( 44, 'Personal Weapons' );");
        query.exec("INSERT INTO Commodities ( id,name ) VALUES ( 45, 'Pesticides' );");
        query.exec("INSERT INTO Commodities ( id,name ) VALUES ( 46, 'Plastics' );");
        query.exec("INSERT INTO Commodities ( id,name ) VALUES ( 47, 'Progenitor Cells' );");
        query.exec("INSERT INTO Commodities ( id,name ) VALUES ( 48, 'Reactive Armour' );");
        query.exec("INSERT INTO Commodities ( id,name ) VALUES ( 49, 'Resonating Separators' );");
        query.exec("INSERT INTO Commodities ( id,name ) VALUES ( 50, 'Robotics' );");
        query.exec("INSERT INTO Commodities ( id,name ) VALUES ( 51, 'Rutile' );");
        query.exec("INSERT INTO Commodities ( id,name ) VALUES ( 52, 'Scrap' );");
        query.exec("INSERT INTO Commodities ( id,name ) VALUES ( 53, 'Tantalum' );");
        query.exec("INSERT INTO Commodities ( id,name ) VALUES ( 54, 'Tea' );");
        query.exec("INSERT INTO Commodities ( id,name ) VALUES ( 55, 'Terrain Enrichment Sys.' );");
        query.exec("INSERT INTO Commodities ( id,name ) VALUES ( 56, 'Titanium' );");

        query.exec("INSERT INTO AVGPrices ( commodity,price ) VALUES (  1, 2733 );");
        query.exec("INSERT INTO AVGPrices ( commodity,price ) VALUES (  2, 880 );");
        query.exec("INSERT INTO AVGPrices ( commodity,price ) VALUES (  3, 45 );");
        query.exec("INSERT INTO AVGPrices ( commodity,price ) VALUES (  4, 1088 );");
        query.exec("INSERT INTO AVGPrices ( commodity,price ) VALUES (  5, 200 );");
        query.exec("INSERT INTO AVGPrices ( commodity,price ) VALUES (  6, 1175 );");
        query.exec("INSERT INTO AVGPrices ( commodity,price ) VALUES (  7, 173 );");
        query.exec("INSERT INTO AVGPrices ( commodity,price ) VALUES (  8, 148 );");
        query.exec("INSERT INTO AVGPrices ( commodity,price ) VALUES (  9, 3577 );");
        query.exec("INSERT INTO AVGPrices ( commodity,price ) VALUES (  10, 187 );");
        query.exec("INSERT INTO AVGPrices ( commodity,price ) VALUES (  11, 36 );");
        query.exec("INSERT INTO AVGPrices ( commodity,price ) VALUES (  12, 2367 );");
        query.exec("INSERT INTO AVGPrices ( commodity,price ) VALUES (  13, 864 );");
        query.exec("INSERT INTO AVGPrices ( commodity,price ) VALUES (  14, 39 );");
        query.exec("INSERT INTO AVGPrices ( commodity,price ) VALUES (  15, 187 );");
        query.exec("INSERT INTO AVGPrices ( commodity,price ) VALUES (  16, 572 );");
        query.exec("INSERT INTO AVGPrices ( commodity,price ) VALUES (  17, 1175 );");
        query.exec("INSERT INTO AVGPrices ( commodity,price ) VALUES (  18, 1256 );");
        query.exec("INSERT INTO AVGPrices ( commodity,price ) VALUES (  19, 2733 );");
        query.exec("INSERT INTO AVGPrices ( commodity,price ) VALUES (  20, 396 );");
        query.exec("INSERT INTO AVGPrices ( commodity,price ) VALUES (  21, 6733 );");
        query.exec("INSERT INTO AVGPrices ( commodity,price ) VALUES (  22, 274 );");
        query.exec("INSERT INTO AVGPrices ( commodity,price ) VALUES (  23, 2070 );");
        query.exec("INSERT INTO AVGPrices ( commodity,price ) VALUES (  24, 396 );");
        query.exec("INSERT INTO AVGPrices ( commodity,price ) VALUES (  25, 173 );");
        query.exec("INSERT INTO AVGPrices ( commodity,price ) VALUES (  26, 572 );");
        query.exec("INSERT INTO AVGPrices ( commodity,price ) VALUES (  27, 46 );");
        query.exec("INSERT INTO AVGPrices ( commodity,price ) VALUES (  28, 2070 );");
        query.exec("INSERT INTO AVGPrices ( commodity,price ) VALUES (  29, 9076 );");
        query.exec("INSERT INTO AVGPrices ( commodity,price ) VALUES (  30, 84 );");
        query.exec("INSERT INTO AVGPrices ( commodity,price ) VALUES (  31, 148 );");
        query.exec("INSERT INTO AVGPrices ( commodity,price ) VALUES (  32, 77 );");
        query.exec("INSERT INTO AVGPrices ( commodity,price ) VALUES (  33, 21 );");
        query.exec("INSERT INTO AVGPrices ( commodity,price ) VALUES (  34, 2214 );");
        query.exec("INSERT INTO AVGPrices ( commodity,price ) VALUES (  35, 55 );");
        query.exec("INSERT INTO AVGPrices ( commodity,price ) VALUES (  36, 457 );");
        query.exec("INSERT INTO AVGPrices ( commodity,price ) VALUES (  37, 495 );");
        query.exec("INSERT INTO AVGPrices ( commodity,price ) VALUES (  38, 4207 );");
        query.exec("INSERT INTO AVGPrices ( commodity,price ) VALUES (  39, 457 );");
        query.exec("INSERT INTO AVGPrices ( commodity,price ) VALUES (  40, 72 );");
        query.exec("INSERT INTO AVGPrices ( commodity,price ) VALUES (  41, 5099 );");
        query.exec("INSERT INTO AVGPrices ( commodity,price ) VALUES (  42, 1676 );");
        query.exec("INSERT INTO AVGPrices ( commodity,price ) VALUES (  43, 3375 );");
        query.exec("INSERT INTO AVGPrices ( commodity,price ) VALUES (  44, 4130 );");
        query.exec("INSERT INTO AVGPrices ( commodity,price ) VALUES (  45, 100 );");
        query.exec("INSERT INTO AVGPrices ( commodity,price ) VALUES (  46, 36 );");
        query.exec("INSERT INTO AVGPrices ( commodity,price ) VALUES (  47, 6671 );");
        query.exec("INSERT INTO AVGPrices ( commodity,price ) VALUES (  48, 1917 );");
        query.exec("INSERT INTO AVGPrices ( commodity,price ) VALUES (  49, 5724 );");
        query.exec("INSERT INTO AVGPrices ( commodity,price ) VALUES (  50, 1676 );");
        query.exec("INSERT INTO AVGPrices ( commodity,price ) VALUES (  51, 200 );");
        query.exec("INSERT INTO AVGPrices ( commodity,price ) VALUES (  52, 27 );");
        query.exec("INSERT INTO AVGPrices ( commodity,price ) VALUES (  53, 3861 );");
        query.exec("INSERT INTO AVGPrices ( commodity,price ) VALUES (  54, 1357 );");
        query.exec("INSERT INTO AVGPrices ( commodity,price ) VALUES (  55, 4679 );");
        query.exec("INSERT INTO AVGPrices ( commodity,price ) VALUES (  56, 880 );");

        query.exec("INSERT INTO Systems ( id, name ) VALUES ( 1,'Eranin' );");
        query.exec("INSERT INTO Systems ( id, name ) VALUES ( 2,'Asellus Primus' );");
        query.exec("INSERT INTO Systems ( id, name ) VALUES ( 3,'Dahan' );");
        query.exec("INSERT INTO Systems ( id, name ) VALUES ( 4,'LP 98-132' );");
        query.exec("INSERT INTO Systems ( id, name ) VALUES ( 5,'LHS 3006' );");
        query.exec("INSERT INTO Systems ( id, name ) VALUES ( 6,'Aulin' );");
        query.exec("INSERT INTO Systems ( id, name ) VALUES ( 7,'I Bootis' );");
        query.exec("INSERT INTO Systems ( id, name ) VALUES ( 8,'Styx' );");
        query.exec("INSERT INTO Stations ( name,system ) VALUES ( 'Azeban City',1 );");
        query.exec("INSERT INTO Stations ( name,system ) VALUES ( 'Beagle 2 Landing',2 );");
        query.exec("INSERT INTO Stations ( name,system ) VALUES ( 'Dahan Gateway',3 );");
        query.exec("INSERT INTO Stations ( name,system ) VALUES ( 'Freeport',4 );");
        query.exec("INSERT INTO Stations ( name,system ) VALUES ( 'WCM Transfer Orbital',5 );");
        query.exec("INSERT INTO Stations ( name,system ) VALUES ( 'Aulin Enterprise',6 );");
        query.exec("INSERT INTO Stations ( name,system ) VALUES ( 'Chango Dock',7 );");
        m_db.commit();
    }
}


