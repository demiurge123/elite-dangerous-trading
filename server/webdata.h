#pragma once
#include <QtCore>
#include <QJsonDocument>
#include <QByteArray>

class WebData
{
public:
    WebData(const QStringList &list, const QString &names, const QStringList &keys);
    QByteArray data();
private:
    QJsonDocument m_doc;
};

