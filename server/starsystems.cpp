#include <QDebug>
#include <QNetworkRequest>
#include <QScopedPointer>
#include "starsystems.h"
#include "QLogger.h"
#include "database.h"

StarSystems::StarSystems(const int &port, QObject *parent) :
    QObject(parent)
{
    initMatching();

    QHttpServer *server = new QHttpServer(this);
    connect(server, SIGNAL(newRequest(QHttpRequest*, QHttpResponse*)),
            this, SLOT(handleRequest(QHttpRequest*, QHttpResponse*)));
    if(qEnvironmentVariableIsSet("PORT") && !qEnvironmentVariableIsEmpty("PORT")) {
        server->listen(QHostAddress::Any, qgetenv("PORT").toInt());
    } else {
        server->listen(QHostAddress::Any, port);
    }

}

void StarSystems::initMatching()
{
    QStringList listCommodities= Database::getInstance()->getAllNameCommodities();
    foreach(QString commodity,listCommodities) {
        QString shortCommodity = prepareToCompare(commodity);
        m_wordsOfCommodities.insertMulti(shortCommodity.size(),shortCommodity);
        m_commodities.insert(shortCommodity,commodity);
    }

    QStringList listStations= Database::getInstance()->getAllNameStations();
    foreach(QString station,listStations) {
        station = station.split("|").at(1);
        QString shortStation = prepareToCompare(station);
        m_wordsOfStations.insertMulti(shortStation.size(),shortStation);
        m_stations.insert(shortStation,station);
    }

    m_allAvgPrices  = Database::getInstance()->getlistAllAvgPrices();
}

QByteArray StarSystems::listAllCommodities()
{
    QStringList listCommodities= Database::getInstance()->getAllNameCommodities();
    QScopedPointer<WebData> webData( new WebData(listCommodities,"Commodities",QStringList()<<"commodity"));
    return webData->data();
}

QByteArray StarSystems::listAllStations()
{
    QStringList listStations= Database::getInstance()->getAllNameStations();
    QScopedPointer<WebData> webData( new WebData(listStations,"Stations",QStringList()<<"id"<<"station"<<"system"));
    return webData->data();
}

QByteArray StarSystems::listAllSystems()
{
    QStringList listSystems= Database::getInstance()->getAllNameSystems();
    QScopedPointer<WebData> webData( new WebData(listSystems,"Systems",QStringList()<<"system"));
    return webData->data();
}

QByteArray StarSystems::listCommoditiesInStation(const int &index)
{
    QStringList listCommodities= Database::getInstance()->getAllCommoditiesInStation(index);
    QScopedPointer<WebData> webData( new WebData(listCommodities,"Commodities",QStringList()<<"lastUpdate"<<"Name"<<"BuyPrice"<<"SellPrice"<<"Supply"));
    return webData->data();
}



bool StarSystems::priceToInt(const QString &priceText, int &price)
{
    if (priceText.isEmpty()) {
        price=0;
        return true;
    }
    QRegExp regexp("(\\d{1,})");
    if (priceText.indexOf(regexp)>-1 && regexp.cap(1).size()==priceText.size())
    {
        price = regexp.cap(1).toInt();
        return true;
    }
    return false;
}

bool StarSystems::checkPrice(const QString &commodity, const int &testPrice)
{
    int avg=m_allAvgPrices.value(commodity);
    double diff = double(qAbs(testPrice-avg))/avg;
    if (testPrice<100 && diff<1.0)
        return true;
    if (testPrice<300 && diff<0.5)
        return true;
    if (testPrice<1000 && diff<0.4)
        return true;
    if (testPrice<10000 && diff<0.2)
        return true;
    return false;
}

QString StarSystems::matchingCommodity(const QString &testCommodity)
{
    QString shortCommodity = prepareToCompare(testCommodity);
    for (int i=0;i<4;i++) { // 0-3 lost letters in begin word
        QString result = match(shortCommodity,m_wordsOfCommodities);
        if (!result.isEmpty()) {
            return m_commodities.value(result);
            break;
        } else {
            shortCommodity.insert(0,"?");
        }
    }
    return QString();
}

QString StarSystems::matchingStation(const QString &testStation)
{
    QString shortStation = prepareToCompare(testStation);
    for (int i=0;i<4;i++) { // 0-3 lost letters in begin word
        QString result = match(shortStation,m_wordsOfStations);
        if (!result.isEmpty()) {
            return m_stations.value(result);
            break;
        } else {
            shortStation.insert(0,"?");
        }
    }
    return QString();
}

QString StarSystems::prepareToCompare(QString text)
{
    text.remove(QRegExp("[ .-]"));
    text.replace("VV","W",Qt::CaseInsensitive);
    return text.toLower();
}

QString StarSystems::match(const QString &nameCommodity, const QMap<int, QString> &words)
{
    int length = nameCommodity.size();
    QList<QString> list = words.values(length);
    if (list.size()==0)
        return QString();

    int index;
    if ((index=list.indexOf(nameCommodity))>-1)
        return list.at(index);

    int *match = new int(list.size());
    for (int i=0;i<list.size();i++)
        match[i]=0;

    for (int i=0;i<length;i++) {
        for (int l=0;l<list.size();l++) {
            if (nameCommodity.at(i)==list.at(l).at(i))
                match[l]++;
        }
    }

    int max=0;
    QString commodity;
    for (int i=0;i<list.size();i++)
        if (max<match[i]) {
            max=match[i];
            commodity=list.at(i);
        }
    delete[] match;

    if (max>=0.5*length) // >=50% matching
        return commodity;

    return QString();
}



void StarSystems::response(QHttpResponse *resp, QByteArray body)
{
    resp->setHeader("Content-Type", QString::fromUtf8("application/json"));
    resp->setHeader("Content-Length", QString::number(body.size()));
    resp->writeHead(200);
    resp->end(body);
}

void StarSystems::menu(QHttpResponse *resp)
{
    QByteArray body;
    body.append("<h2>List of Json files</h2>");
    body.append("<a href=\"stations\">station list</a><br />");
    body.append("<a href=\"systems\">list of stellar systems</a><br />");
    body.append("<a href=\"commodities\">list of goods</a><br />");
    body.append("<h3>List of markets</h3>");

    QStringList listStations= Database::getInstance()->getAllNameStations();
    for (int i=0;i<listStations.size();i++) {
        QStringList data=listStations.at(i).split("|");
        QString id=data.at(0);
        QString nameStation=data.at(1);
        body.append("<a href=\"market/"+id+"\">"+nameStation+"</a><br />");
    }

    resp->setHeader("Content-Type", QString::fromUtf8("text/html"));
    resp->setHeader("Content-Length", QString::number(body.size()));
    resp->writeHead(200);
    resp->end(body);
}

bool StarSystems::addRecord(QHttpRequest *req)
{
    QString station=req->header("station");
    QString stat = matchingStation(station);
    if (stat.isEmpty()) {
        QLogger::QLog_Trace("Commodities", "Unidentified station: " + station);
        return false;
    }

    QString commodity=req->header("commodity");
    QString comm = matchingCommodity(commodity);
    if (comm.isEmpty()) {
        QLogger::QLog_Trace("Commodities", "Unidentified commodity: " + commodity);
        return false;
    }

    int sel,buy,sup;

    QString sellPrice=req->header("sell");
    if (!priceToInt(sellPrice.left(sellPrice.size()-2),sel)) {
        QLogger::QLog_Trace("Commodities", "Convertion Error with sell price: " + sellPrice);
        return false;
    }

    QString buyPrice=req->header("buy");
    if (!priceToInt(buyPrice.left(buyPrice.size()-2),buy)) {
        QLogger::QLog_Trace("Commodities", "Convertion Error with sell price: " + buyPrice);
        return false;
    }

    QString supply=req->header("supply");
    if (!priceToInt(supply,sup)) {
        QLogger::QLog_Trace("Commodities", "Convertion Error with supply: " + supply);
        return false;
    }

    if (buy>0 && !checkPrice(comm, buy)) {
        QLogger::QLog_Trace("Commodities", QString("Buy price: %1 does not meet the requirements of the price range.(%2) ").arg(comm).arg(buy));
        return false;
    }

    if (sel>0 && !checkPrice(comm, sel)) {
        QLogger::QLog_Trace("Commodities", QString("Sell price: %1 does not meet the requirements of the price range.(%2) ").arg(comm).arg(sel));
        return false;
    }

    if (buy>0 && buy<sel) {
        QLogger::QLog_Trace("Commodities", QString("Sell price: %1 is greater than the buy price: %2").arg(sel).arg(buy));
        return false;
    }

    bool successfully=Database::getInstance()->registration(stat,comm,sel,buy,sup);

    if (successfully)
        QLogger::QLog_Trace("Commodities",QString("Registration commodity - station: %1 commodity: %2 sell: %3 buy: %4 supply: %5")
                            .arg(stat)
                            .arg(comm)
                            .arg(sel)
                            .arg(buy)
                            .arg(sup));
    return successfully;
}

void StarSystems::handleRequest(QHttpRequest *req, QHttpResponse *resp)
{
    QHttpRequest::HttpMethod method=req->method();
    QString path=req->path();
    QRegExp regexp("/(\\w+)/?(\\d{1,})?");

    if (method==QHttpRequest::HTTP_POST) {
        QLogger::QLog_Trace("Connections", "POST: " + req->remoteAddress()+":"+QString::number(req->remotePort()));

        if (addRecord(req)==true)
            response(resp,"{\"successfully\": \"true\"}");
        else
            response(resp,"{\"successfully\": \"false\"}");
    }
    else if (method==QHttpRequest::HTTP_GET) {
        if (path.indexOf(regexp)>-1) {
            QLogger::QLog_Trace("Connections", "GET: " + req->remoteAddress()+":"+QString::number(req->remotePort()));
            QString action=regexp.cap(1);
            int number=regexp.cap(2).isEmpty()?0:regexp.cap(2).toInt();

            if (action.compare("commodities")==0) {
                QByteArray body=listAllCommodities();
                response(resp, body);
            }
            else if (action.compare("stations")==0) {
                QByteArray body=listAllStations();
                response(resp, body);
            }
            else if (action.compare("systems")==0) {
                QByteArray body=listAllSystems();
                response(resp, body);
            }
            else if (action.compare("market")==0 && number>0) {
                QByteArray body=listCommoditiesInStation(number);
                response(resp, body);
            }
            else
                menu(resp);
        } else {

            menu(resp);
        }
    } else
        response(resp, QByteArray());
}
