#pragma once
#include <QSqlDatabase>
#include <QMap>

class Database
{
public:

    static Database* getInstance() {
        if (!_instance)
            _instance = new Database();
        return _instance;
    }
    virtual ~Database();

    QStringList getAllNameCommodities();
    QStringList getAllNameStations();
    QStringList getAllNameSystems();
    QStringList getAllCommoditiesInStation(const int &index);
    QMap<QString, int> getlistAllAvgPrices();
    bool registration(const QString &station, const QString &commodity, const int &sell, const int &buy, const int &supply);
private:
    static Database *_instance;
    QSqlDatabase m_db;
    Database();

    bool createConnection();
    void createData();
};
