
#include "webdata.h"

WebData::WebData(const QStringList &list, const QString &names, const QStringList &keys)
{
    QJsonObject object;
    QJsonArray objectArray;
    foreach (QString element, list) {
        QJsonObject elementObject;
        QStringList listValues = element.split("|");
        for (int i=0;i<listValues.size();i++) {
            elementObject[keys.at(i)]=listValues.at(i);
        }
        objectArray.append(elementObject);
    }
    object[names] = objectArray;
    m_doc.setObject(object);
}

QByteArray WebData::data()
{
   return m_doc.toJson();
}
