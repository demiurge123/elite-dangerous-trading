#pragma once
#include "webdata.h"

#include <qhttpserver.h>
#include <qhttprequest.h>
#include <qhttpresponse.h>
#include <QObject>

class StarSystems : public QObject
{
    Q_OBJECT
public:
    explicit StarSystems(const int &port,QObject *parent = 0);
    bool addRecord(QHttpRequest *req);
private:
    void response(QHttpResponse *resp, QByteArray body);
    void menu(QHttpResponse *resp);
    QByteArray listAllCommodities();
    QByteArray listAllStations();
    QByteArray listAllSystems();
    QByteArray listCommoditiesInStation(const int &index);


    void initMatching();
    bool priceToInt(const QString &priceText, int &price);

    bool checkPrice(const QString &commodity, const int &testPrice);

    QString matchingCommodity(const QString &testCommodity);
    QString matchingStation(const QString &testStation);
    QString prepareToCompare(QString text);
    QString match(const QString& nameCommodity, const QMap<int,QString> &words);

    QMap<int,QString> m_wordsOfCommodities, m_wordsOfStations;
    QHash<QString, QString> m_commodities, m_stations;

     QMap<QString,int> m_allAvgPrices;
private slots: 
    void handleRequest(QHttpRequest *req, QHttpResponse *resp);
};


