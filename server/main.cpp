#include <QCoreApplication>
#include <QDir>
#include <QStandardPaths>
#include <iostream>
#include "starsystems.h"
#include "QLogger.h"

int _port = 3001;

void version()
{
    std::cout<<"Elite:Dangerous Trading Server, ";
    std::cout<<"version: "<<qApp->applicationVersion().toStdString()<<std::endl;
    std::cout<<"Author: Ireneusz Bohatyrewicz (demiurge)"<<std::endl;
}

void cmdline(const QStringList &list)
{
    if (list.size()==1)
        return;
    QString command=list.at(1);
    if (command.compare("-port")==0) {
        bool ok;
        if (list.size()==3) {
            int port=list.at(2).toInt(&ok);
            if (ok)
                _port=port;
        }

    }
}


void createLogs()
{
    QString dataPath = QStandardPaths::writableLocation(QStandardPaths::GenericDataLocation)+QDir::separator()+"EDT Group";
    QDir dirPath(dataPath+"/logs");
    if (!dirPath.exists()) {
        QDir(dataPath).mkpath("logs");
    }

    QLogger::QLoggerManager::getInstance()->addDestination(dataPath+"/logs/connections.log","Connections",QLogger::TraceLevel);
    QLogger::QLoggerManager::getInstance()->addDestination(dataPath+"/logs/database.log","Database",QLogger::WarnLevel);
    QLogger::QLoggerManager::getInstance()->addDestination(dataPath+"/logs/commodities.log","Commodities",QLogger::TraceLevel);
}

int main(int argc, char *argv[])
{
    QCoreApplication app(argc, argv);
    app.setApplicationVersion(APP_VERSION);

    QStringList cmdline_args = QCoreApplication::arguments();
    cmdline(cmdline_args);

    version();

    createLogs();

    StarSystems starSystems(_port);

    return app.exec();
}
