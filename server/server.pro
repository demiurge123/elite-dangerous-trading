QT       += core network sql
QT       -= gui

TARGET = server
CONFIG   += console
CONFIG   -= app_bundle

APP_BASE = ..
DESTDIR = $$APP_BASE/EliteDangerousTrading

VERSION = 0.7.2
TEMPLATE = app

DEFINES += APP_VERSION=\\\"$$VERSION\\\"

macx: LIBS += -L$$APP_BASE/lib -lqhttpserver
win32 {
    LIBS += -L$$APP_BASE/lib
    CONFIG(debug, debug|release) {
        DEFINES += _DEBUG

        LIBS += -lqhttpserverd
    } else {
        DEFINES += NDEBUG

        LIBS += -lqhttpserver
    }

}
INCLUDEPATH += $$PWD/../3rdparty/qhttpserver/src \
        $$PWD/../3rdparty/QLogger

DEPENDPATH += $$PWD/../3rdparty/qhttpserver/src \
        $$PWD/../3rdparty/QLogger

SOURCES += main.cpp \
    database.cpp \
    starsystems.cpp \
    ../3rdparty/QLogger/QLogger.cpp \
    webdata.cpp \
    connectiontask.cpp

HEADERS += \
    database.h \
    starsystems.h \
    ../3rdparty/QLogger/QLogger.h \
    webdata.h \
    connectiontask.h


