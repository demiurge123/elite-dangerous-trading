
QT       += testlib network

QT       -= gui

TARGET = tst_tradingtest
CONFIG   += console
CONFIG   += app_bundle

TEMPLATE = app

DEPENDPATH += . \
    ../../app

INCLUDEPATH += . \
    ../../app

SOURCES += tst_tradingtest.cpp \
    ../../app/trading.cpp \
    ../../app/galactica.cpp \
    ../../app/good.cpp \
    ../../app/station.cpp
DEFINES += SRCDIR=\\\"$$PWD/\\\"

HEADERS += \
    ../../app/trading.h \
    ../../app/galactica.h \
    ../../app/good.h \
    ../../app/station.h

RESOURCES += \
    resources.qrc
