#include <QString>
#include <QtTest>
#include "trading.h"
#include "galactica.h"

struct CommoditySlopey {
    QString system;
    QString locale;
    QString stationName;
    QString type;
    QString commodityName;
    int availStock;
    int buyPrice;
    int sellPrice;
    int fence;
    bool illegal;
    bool consumer;
    bool producer;
    QTime lastUpdate;
};

struct CommodityMarketDump {
    int buyPrice;
    int sellPrice;
    int demand;
    int demandLevel;
    int stationStock;
    int stationStockLevel;
    QString categoryName;
    QString itemName;
    QString stationName;
    QDateTime timestamp;
};

class TradingTest : public QObject
{
    Q_OBJECT

public:
    TradingTest();
    virtual ~TradingTest();


private Q_SLOTS:
    void testNameStations();
    void testNameStations_data();
    void testNameCommodities();
    void testNameCommodities_data();
    void testPrices();
    void testPrices_data();
    void testProfits();
    void testProfits_data();
    void testPricesSlopey();
    void testPricesMarketDump();
private:
    Galactica *galactica;
    Trading *trading;

    void load(const QString &filename);
    QMap<QString, CommoditySlopey> loadSlopeySystems();
    QMap<QString, CommodityMarketDump> loadMarketDump();
};

TradingTest::TradingTest()
{
    galactica = new Galactica();
    trading = new Trading(galactica);
}

TradingTest::~TradingTest()
{
    delete trading;
    delete galactica;
}

void TradingTest::testNameStations()
{
    QFETCH(QString, filename);
    QFETCH(QStringList, systems);
    QFETCH(QStringList, stations);

    load(filename);
    QList<Station *> listStations = galactica->allStations();
    for(int i=0;i<listStations.size() ;++i) {
        Station *station = listStations.at(i);
        QString nameStation = station->name();
        QVERIFY(nameStation.isEmpty()==false);

        QStringList data = nameStation.split('|');
        QVERIFY(data.count() == 2);

        QString testSystem = data.at(0).trimmed();
        QString testStation = data.at(1).trimmed();
        QCOMPARE(testSystem,systems.at(i));
        QCOMPARE(testStation,stations.at(i));
    }
    galactica->clear();
}

void TradingTest::testNameStations_data()
{
    QTest::addColumn<QString>("filename");
    QTest::addColumn<QStringList>("systems");
    QTest::addColumn<QStringList>("stations");

    QTest::newRow("2014-07-07") << ":/galactica1.json"
                                << (QStringList()<<"Aulin"<<"Eranin"<<"Asellus Primus"<<"I Bootis"<<"Dahan"<<"LP 98-132"<<"LHS 3006")
                                << (QStringList()<<"Aulin Enterprise"<<"Azeban City"<<"Beagle 2 Landing"<<"Chango Dock"<<"Dahan Gateway"<<"Freeport"<<"WCM Transfer Orbital");
    QTest::newRow("2014-07-14") << ":/galactica2.json"
                                << (QStringList()<<"Aulin"<<"Eranin"<<"Asellus Primus"<<"I Bootis"<<"Dahan"<<"LP 98-132"<<"LHS 3006")
                                << (QStringList()<<"Aulin Enterprise"<<"Azeban City"<<"Beagle 2 Landing"<<"Chango Dock"<<"Dahan Gateway"<<"Freeport"<<"WCM Transfer Orbital");
}

void TradingTest::testNameCommodities()
{
    QFETCH(QString, filename);
    QFETCH(QStringList, commodities);


    load(filename);
    QStringList testCommodities;
    QList<Station *> listStations = galactica->allStations();
    for(int i=0;i<listStations.size() ;++i) {
        Station *station = listStations.at(i);
        const QList<Good *> goods = station->goods();
        for (int j=0;j<goods.size();++j) {
            QString nameCommodity = goods.at(j)->name();
            if (testCommodities.contains(nameCommodity) == false ) {
                testCommodities.append(nameCommodity);
            }
        }
    }
    qSort(testCommodities);
    QCOMPARE(testCommodities, commodities);
    galactica->clear();
}

void TradingTest::testNameCommodities_data()
{
    QTest::addColumn<QString>("filename");
    QTest::addColumn<QStringList>("commodities");

    QTest::newRow("2014-07-07") << ":/galactica1.json"
                                << (QStringList()<<"Advanced Catalysers"<<"Agri-Medicines"<<"Algae"<<"Alloys"<<"Aluminium"<<"Animal Meat"<<"Animal Monitors"<<"Aquaponic Systems"<<"Auto-Fabricators"<<"Basic Medicines"<<"Bauxite"<<"Bertrandite"<<"Bioreducing Lichen"<<"Biowaste"<<"Clothing"<<"Cobalt"<<"Coffee"<<"Coltan"<<"Combat Stabilisers"<<"Computer Components"<<"Consumer Technology"<<"Cotton"<<"Crop Harvesters"<<"Dom. Appliances"<<"Explosives"<<"Fish"<<"Food Cartridges"<<"Gallite"<<"Gold"<<"Grain"<<"H.E. Suits"<<"Hel-Static Furnaces"<<"Hydrogen Fuels"<<"Indite"<<"Leather"<<"Lepidolite"<<"Liquor"<<"Marine Supplies"<<"Mineral Extractors"<<"Mineral Oil"<<"Narcotics"<<"Non-Lethal Wpns"<<"Performance Enhancers"<<"Personal Weapons"<<"Pesticides"<<"Plastics"<<"Progenitor Cells"<<"Reactive Armour"<<"Resonating Separators"<<"Robotics"<<"Rutile"<<"Scrap"<<"Tantalum"<<"Tea"<<"Terrain Enrichment Sys."<<"Titanium");
    QTest::newRow("2014-07-14") << ":/galactica2.json"
                                << (QStringList()<<"Advanced Catalysers"<<"Agri-Medicines"<<"Algae"<<"Alloys"<<"Aluminium"<<"Animal Meat"<<"Animal Monitors"<<"Aquaponic Systems"<<"Auto-Fabricators"<<"Basic Medicines"<<"Bauxite"<<"Bertrandite"<<"Bioreducing Lichen"<<"Biowaste"<<"Clothing"<<"Cobalt"<<"Coffee"<<"Coltan"<<"Combat Stabilisers"<<"Computer Components"<<"Consumer Technology"<<"Cotton"<<"Crop Harvesters"<<"Dom. Appliances"<<"Explosives"<<"Fish"<<"Food Cartridges"<<"Gallite"<<"Gold"<<"Grain"<<"H.E. Suits"<<"Hel-Static Furnaces"<<"Hydrogen Fuels"<<"Indite"<<"Leather"<<"Lepidolite"<<"Liquor"<<"Marine Supplies"<<"Mineral Extractors"<<"Mineral Oil"<<"Narcotics"<<"Non-Lethal Wpns"<<"Performance Enhancers"<<"Personal Weapons"<<"Pesticides"<<"Plastics"<<"Progenitor Cells"<<"Reactive Armour"<<"Resonating Separators"<<"Robotics"<<"Rutile"<<"Scrap"<<"Tantalum"<<"Tea"<<"Terrain Enrichment Sys."<<"Titanium");

}

void TradingTest::testPrices()
{
    QFETCH(QString, filename);

    load(filename);
    QList<Station *> listStations = galactica->allStations();
    for(int i=0;i<listStations.size() ;++i) {
        Station *station = listStations.at(i);
        const QList<Good *> goods = station->goods();
        for (int j=0;j<goods.size();++j) {
            int buyPrice = goods.at(j)->buyPrice();
            int sellPrice = goods.at(j)->sellPrice();
            int supply = goods.at(j)->supply();
            QVERIFY(sellPrice>0);
            if (buyPrice>0) {
                QVERIFY(sellPrice<=buyPrice);
                QVERIFY(supply>0);
            } else {
                QVERIFY(supply==0);
            }
        }
    }
    galactica->clear();
}

void TradingTest::testPrices_data()
{
    QTest::addColumn<QString>("filename");

    QTest::newRow("2014-07-07") << ":/galactica1.json";
    QTest::newRow("2014-07-14") << ":/galactica2.json";

}

void TradingTest::testProfits()
{
    QFETCH(QString, filename);

    load(filename);
    trading->initialProfits();
    QMap<QPair<int, int>,Profit*> profits = trading->getProfits();
    QVERIFY(profits.size()>0);
    QMapIterator<QPair<int, int>,Profit*> i(profits);
    while (i.hasNext()) {
        i.next();
        QVERIFY(i.key().first!=i.key().second);
        QVERIFY(i.value()->value()>0);

    }
    galactica->clear();
}

void TradingTest::testProfits_data()
{
    QTest::addColumn<QString>("filename");

    QTest::newRow("2014-07-07") << ":/galactica1.json";
    QTest::newRow("2014-07-14") << ":/galactica2.json";
}

QMap<QString, CommoditySlopey> TradingTest::loadSlopeySystems()
{
    QMap<QString, CommoditySlopey> databaseSlopey;
    QFile slopeyFile(":/systems.csv");
    if (!slopeyFile.open(QIODevice::ReadOnly|QIODevice::Text))
        return databaseSlopey;
    QTextStream in(&slopeyFile);
    while(!in.atEnd()) {
        QString line = in.readLine();
        if (line.isEmpty())
            break;
        QStringList data = line.split(';');
        if (data.at(0).compare("System")==0)
            continue;
        CommoditySlopey comm;
        comm.system=data.at(0);
        comm.locale=data.at(1);
        comm.stationName=data.at(2);
        comm.type=data.at(3);
        comm.commodityName=data.at(4);

//        if (comm.commodityName.compare("Clothing")==0)
//            comm.commodityName = "Basic Medicines";
//        else
//        if (comm.commodityName.compare("Dom. Appliances")==0)
//            comm.commodityName = "Computer Components";
//        else
//        if (comm.commodityName.compare("Artificial Habitat Modules")==0)
//            comm.commodityName = "Animal Monitors";
//        else
//        if (comm.commodityName.compare("Coffee")==0)
//            comm.commodityName = "Animal Meat";
//        else
//        if (comm.commodityName.compare("Combat Stabilisers")==0)
//            comm.commodityName = "Advanced Catalysers";
//        else
//        if (comm.commodityName.compare("Mineral Extractors")==0)
//            comm.commodityName = "Lepidolite";
//        else
        if (comm.commodityName.compare("Hazardous Environment Suits")==0)
            comm.commodityName = "H.E. Suits";
        else
        if (comm.commodityName.compare("Terrain Enrichment Systems")==0)
            comm.commodityName = "Terrain Enrichment Sys.";
        else
        if (comm.commodityName.compare("Non Lethal Weapons")==0)
            comm.commodityName = "Non-Lethal Wpns";
        else
        if (comm.commodityName.compare("Hydrogen Fuel")==0)
            comm.commodityName = "Hydrogen Fuels";
        else
        if (comm.commodityName.compare("Domestic Appliances")==0)
            comm.commodityName = "Dom. Appliances";
        else
        if (comm.commodityName.compare("Chemical Drugs")==0)
            comm.commodityName = "Narcotics";
        else
        if (comm.commodityName.compare("Agricultural Medicines")==0)
            comm.commodityName = "Agri-Medicines";
        else
        if (comm.commodityName.compare("Auto Fabricators")==0)
            comm.commodityName = "Auto-Fabricators";
        else
        if (comm.commodityName.compare("Bio Reducing Lichen")==0)
            comm.commodityName = "Bioreducing Lichen";
        else
        if (comm.commodityName.compare("Heliostatic Furnaces")==0)
            comm.commodityName = "Hel-Static Furnaces";
        else
        if (comm.commodityName.compare("Liqueurs")==0)
            comm.commodityName = "Liquor";

        comm.availStock=data.at(5).toInt();
        comm.buyPrice=data.at(6).toInt();
        comm.sellPrice=data.at(7).toInt();
        comm.fence=data.at(8).toInt();
        comm.illegal=data.at(9).toLower().compare("unchecked")==0?false:true;
        comm.consumer=data.at(10).toLower().compare("unchecked")==0?false:true;
        comm.producer=data.at(11).toLower().compare("unchecked")==0?false:true;
        comm.lastUpdate=QTime::fromString(data.at(12),"h:mm:ss AP");
        databaseSlopey.insertMulti(comm.system+" | "+comm.stationName,comm);
    }
    slopeyFile.close();

    return databaseSlopey;
}

QMap<QString, CommodityMarketDump> TradingTest::loadMarketDump()
{
    QMap<QString, CommodityMarketDump> databaseMarketDump;
    QFile marketDumpFile(":/2014-07-18-16-36-30.csv");
    if (!marketDumpFile.open(QIODevice::ReadOnly|QIODevice::Text))
        return databaseMarketDump;
    QTextStream in(&marketDumpFile);
    while(!in.atEnd()) {
        QString line = in.readLine();
        if (line.isEmpty())
            break;
        QStringList data = line.split(',');
        if (data.at(0).compare("buyPrice")==0)
            continue;
        CommodityMarketDump comm;
        comm.buyPrice = data.at(0).toInt();
        comm.sellPrice = data.at(1).toInt();
        comm.demand = data.at(2).toInt();
        comm.demandLevel = data.at(3).toInt();
        comm.stationStock = data.at(4).toInt();
        comm.stationStockLevel = data.at(5).toInt();
        comm.categoryName = data.at(6);
        comm.itemName = data.at(7);
        if (comm.itemName.compare("hazardousenvironmentsuits")==0)
            comm.itemName = "hesuits";
        else
        if (comm.itemName.compare("terrainenrichmentsystems")==0)
            comm.itemName = "terrainenrichmentsys";
        else
        if (comm.itemName.compare("nonlethalweapons")==0)
            comm.itemName = "nonlethalwpns";
        else
        if (comm.itemName.compare("hydrogenfuel")==0)
            comm.itemName = "hydrogenfuels";
        else
        if (comm.itemName.compare("domesticappliances")==0)
            comm.itemName = "domappliances";
        else
        if (comm.itemName.compare("chemicaldrugs")==0)
            comm.itemName = "narcotics";
        else
        if (comm.itemName.compare("agriculturalmedicines")==0)
            comm.itemName = "agrimedicines";
        else
        if (comm.itemName.compare("artificialhabitatmodules")==0)
            comm.itemName = "animalmonitors";
        else
        if (comm.itemName.compare("heliostaticfurnaces")==0)
            comm.itemName = "helstaticfurnaces";
        else
        if (comm.itemName.compare("liqueurs")==0)
            comm.itemName = "liquor";
        else
        if (comm.itemName.compare("oscilativesplitters")==0)
            comm.itemName = "resonatingseparators";
        comm.stationName = data.at(8);
        comm.timestamp = QDateTime::fromString(data.at(9),"yyyy-MM-ddThh:mm:ss.zzz");
        databaseMarketDump.insertMulti(comm.stationName,comm);
    }
    marketDumpFile.close();

    return databaseMarketDump;
}

void TradingTest::testPricesSlopey()
{
    QMap<QString, CommoditySlopey> databaseSlopey = loadSlopeySystems();

    load(":/galactica2.json");
    QStringList allCommodities;

    foreach(Station *station,galactica->allStations()) {
        foreach(Good* good, station->goods()) {
            QString name = station->name()+" | "+good->name();
            allCommodities.append(name);
        }

    }

    QMapIterator<QString, CommoditySlopey> i(databaseSlopey);
    while (i.hasNext()) {
        i.next();
        bool foundStation = false;
        foreach(Station *station,galactica->allStations()) {

            if (station->name().compare(i.key(),Qt::CaseInsensitive)==0) {
                foundStation = true;
                CommoditySlopey comm = i.value();
                bool found=false;
                foreach(Good* good, station->goods()) {
                    if (good->name().compare(comm.commodityName,Qt::CaseInsensitive)==0) {
                        found=true;
                        QString name = station->name()+" | "+good->name();
                        allCommodities.removeAll(name);
                        break;
                    }
                    else if (good->buyPrice()==comm.buyPrice && good->sellPrice()==comm.sellPrice){
                        found = true;
//                        qDebug()<<qPrintable(QString("* %1 | %2 - %3").arg(station->name()).arg(good->name()).arg(comm.commodityName));
                    }
                }
//                if (!found)
//                    qDebug()<<qPrintable(QString("- %1 | %2").arg(station->name()).arg(comm.commodityName));
                break;
            }
        }
//        if (!foundStation)
//            qDebug()<<i.key();
    }
//    qDebug()<<allCommodities;
}

void TradingTest::testPricesMarketDump()
{
    QMap<QString, CommodityMarketDump> databaseMarketDump = loadMarketDump();

    load(":/galactica3.json");
    QStringList allCommodities;

    foreach(Station *station,galactica->allStations()) {
        foreach(Good* good, station->goods()) {
            QString name = station->name()+" | "+good->name();
            allCommodities.append(name);
        }

    }

    QMapIterator<QString, CommodityMarketDump> i(databaseMarketDump);
    while (i.hasNext()) {
        i.next();
        bool foundStation = false;
        foreach(Station *station,galactica->allStations()) {
            QString stationName = station->name();
            stationName = stationName.split(" | ").at(1).trimmed();
            if (stationName.compare(i.key(),Qt::CaseInsensitive)==0) {
                foundStation = true;
                CommodityMarketDump comm = i.value();
                bool found=false;
                foreach(Good* good, station->goods()) {
                    QString goodName = good->name();
                    goodName.remove(QRegExp("[ -\\.]"));
                    if (goodName.compare(comm.itemName,Qt::CaseInsensitive)==0) {
                        found=true;
                        QString name = station->name()+" | "+good->name();
                        allCommodities.removeAll(name);
                        QCOMPARE(comm.buyPrice, good->buyPrice());
                        QCOMPARE(comm.sellPrice, good->sellPrice());
                        QCOMPARE(comm.stationStock, good->supply());
                        break;
                    }
                }
                QVERIFY(found);
//                if (!found)
//                    qDebug()<<qPrintable(QString("- %1 | %2").arg(station->name()).arg(comm.itemName));
                break;
            }
        }

    }
    QVERIFY(allCommodities.size()==0);
//    qDebug()<<allCommodities;
}

void TradingTest::load(const QString &filename)
{
    QFile loadFile(filename);

    if (!loadFile.open(QIODevice::ReadOnly)) {
        return;
    }

    QByteArray saveData = loadFile.readAll();

    QJsonDocument loadDoc(QJsonDocument::fromJson(saveData));
    galactica->read(loadDoc.object());
}

QTEST_MAIN(TradingTest)

#include "tst_tradingtest.moc"
