#pragma once
#include <QObject>
#include <QDir>
#include "recognitionInterface.h"
#include "preperationimages.h"
#include "segmentationimages.h"
#include "recognitionimages.h"
#include "fields.h"

enum COMMAND_TYPE {
    CAPTION,
    PREPARATION,
    SEGMENTATION,
    RECOGNITION
};

//const int POINTS_SOURCE_STATION_1920[] = {0,0, 905,70, 905,135, 0,77};
//const int POINTS_DEST_STATION_1920[] = {0,0, 905,0, 905,74, 0,73};
//const int POINTS_SOURCE_GOODS_1920[] = {0,53, 476,0, 476,391, 0,391};
//const int POINTS_DEST_GOODS_1920[] = {0,0, 476,0, 476,388, 0,391};

//const QRect CROP_AREA_STATION_1920 = QRect(141,49,905,135);
//const QRect CROP_AREA_GOODS_1920 = QRect(1291,161,476,391);

//const QSize SIZE_DEST_STATION_1920 = QSize(905,77);
//const QSize SIZE_DEST_GOODS_1920 = QSize(476,391);

//const QPolygon ANCHORS_SOURCE_STATION_1920 = QPolygon(4,POINTS_SOURCE_STATION_1920);
//const QPolygon ANCHORS_DEST_STATION_1920 = QPolygon(4,POINTS_DEST_STATION_1920);
//const QPolygon ANCHORS_SOURCE_GOODS_1920 = QPolygon(4,POINTS_SOURCE_GOODS_1920);
//const QPolygon ANCHORS_DEST_GOODS_1920 = QPolygon(4,POINTS_DEST_GOODS_1920);

class Tools : public QObject
{
    Q_OBJECT
public:
    Tools(QObject *parent = 0);

    QString dataFolder() const {return dataPath;}
    void setFolderPath(const QString &folderPath, COMMAND_TYPE type);

    QDir screenshots;
    QDir preparations;
    QDir segmentations;
    QDir recognitions;

//    void createField();
    void createCropStation();
    void createCropGoods();
    void createSegmentation();
    void createRecognition();
    void isolationCharacters();
    void saveRecognitionText();
    void learn(const double &efficiency);
    QFile readRecognitionCorrect();
    void readCorrectText();
private:
    QString dataPath;
    QHash<QString,QString> m_recognitionImages;



    void createProcessingImage(QDir &folderSource, QDir &folderDest, Recognition::Command* command);

    QStringList getListIncorrectLetters();

    void clearDir( const QString &path );
    void addLetters(QStringList &listLetters, const double &diff);
    void deleteLetters(QStringList &listLetters, const double &diff);


};

