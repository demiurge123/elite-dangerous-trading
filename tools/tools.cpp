#include <QCoreApplication>
#include <QDir>
#include <QStandardPaths>
#include <QTextStream>
#include <QTime>
#include <QEventLoop>
#include "QLogger.h"
#include <QDebug>
#include "tools.h"
#include "trainer.h"


Tools::Tools(QObject *parent) :
    QObject(parent)
{
    dataPath = QStandardPaths::writableLocation(QStandardPaths::GenericDataLocation)+QDir::separator()+"EDT Group";
    QDir(dataPath).mkpath("recognition/allChars");
    QDir(dataPath).mkpath("recognition/incorrectChars");
    QLogger::QLoggerManager::getInstance()->addDestination(dataPath+"/logs/recognition.log","Recognition",QLogger::TraceLevel);
}

void Tools::setFolderPath(const QString &folderPath, COMMAND_TYPE type)
{
    switch(type) {
    case CAPTION:
        screenshots.setPath(folderPath);
        break;
    case PREPARATION:
        preparations.setPath(folderPath);
        break;
    case SEGMENTATION:
        segmentations.setPath(folderPath);
        break;
    case RECOGNITION:
        recognitions.setPath(folderPath);
        break;
    default:
        screenshots.setPath(folderPath);
    }
}

void Tools::createProcessingImage(QDir &folderSource, QDir &folderDest, Recognition::Command *command)
{

    QStringList filters;

    filters << "*.png";
    folderSource.setNameFilters(filters);
    QStringList filenames=folderSource.entryList(QDir::Files);

    int countFiles=filenames.count();
    int next5procent=countFiles>=20?countFiles/20:1;

    for (int currentFile=0;currentFile<countFiles;++currentFile) {
        QString fileName=filenames.at(currentFile);
        QString filePathSource = folderSource.absolutePath()+QDir::separator()+fileName;
        QImage image(filePathSource);

        QString area;
        QString resolution;
        if (!command->getNameAREA().isEmpty()) {
            resolution = QString("%1x%2").arg(image.width()).arg(image.height());
            area = command->getNameAREA();
            Fields::getInstance()->setCurrent(area+"-"+resolution);
        } else {
            QRegExp regexp("(\\w+)\\_(\\d{1,}x\\d{1,})\\_");
            if (fileName.indexOf(regexp)>-1) {
                resolution = regexp.cap(2);
                area = regexp.cap(1).toLower();
                area[0]=area.at(0).toUpper();
                Fields::getInstance()->setCurrent(area+"-"+resolution);
            }
        }

        command->setImageObject(image);
        command->execute();

        if (currentFile>=next5procent) {
            QTextStream out(stdout, QIODevice::WriteOnly);
            out <<QString("\r%1:%2%").arg(area.leftJustified(7)).arg(QString::number(1+(100*next5procent/countFiles)).rightJustified(5));
            next5procent+=countFiles>=20?countFiles/20:1;
        }

        Recognition::RecognitionInterface *obj=command->getStaticImage();
        QImage img=obj->getImage();

        QString pre=command->getNameAREA().toUpper();
        if (!pre.isEmpty()) {
            pre.append("_");
            pre.append(resolution);
            pre.append("_");
        }

        QString filePathDest = folderDest.absolutePath()+QDir::separator()+pre+fileName;
        img.save(filePathDest);

        //        QString recognitionText=obj->getDataText();
        //        if (!recognitionText.isEmpty())
        //        {
        //            m_recognitionImages.insert(fileName,recognitionText);
        //        }
    }
    QTextStream out(stdout, QIODevice::WriteOnly);
    out << endl;
    //    }
}

void Tools::readCorrectText()
{
    m_recognitionImages.clear();
    QFile file(dataPath+"/recognition/recognitions.txt");
    if (file.open(QIODevice::Text|QIODevice::ReadOnly)) {
        QTextStream in(&file);
        while(!in.atEnd()) {
            QString line= in.readLine();
            QStringList data = line.split("|");
            if (data.count()==2)
                m_recognitionImages.insert(data.at(0).trimmed(),data.at(1).trimmed());
        }
        file.close();
    }
}

void Tools::saveRecognitionText()
{
    readCorrectText();
    QFile file(dataPath+"/recognition/recognitions.txt");
    if (file.open(QIODevice::Text|QIODevice::Append)) {
        QTextStream out(&file);
        QStringList filters;
        filters << "*.png";
        segmentations.setNameFilters(filters);
        QStringList filenames=segmentations.entryList(QDir::Files);

        for (int numberOfFile=0;numberOfFile<filenames.size();++numberOfFile) {
            QString fileName=filenames.at(numberOfFile);
            QString filePathSource = segmentations.absolutePath()+QDir::separator()+fileName;
            if (!m_recognitionImages.contains(filePathSource)) {
                out << filePathSource << "| " << endl;
            }
        }

        file.close();
    }
}

void Tools::learn(const double &efficiency)
{
    int counterAttempts=0;
    const int MAX_ATTEMPTS=1000;
    double max=0.0;
    double goodRecognition=99.5;
    bool isAddLetters = false;
    QStringList listLetters;
    qsrand(QTime::currentTime().msec());


    QString allChars=dataPath+"/recognition/allChars/";
    QString pathIncorrect=dataPath+"/recognition/incorrectChars/";

    m_recognitionImages.clear();
    QFile file(dataPath+"/recognition/recognitions.txt");
    if (file.open(QIODevice::Text|QIODevice::ReadOnly)) {
        QTextStream in(&file);
        while(!in.atEnd()) {
            QString line= in.readLine();
            QStringList data = line.split("|");
            if (data.count()==2)
                m_recognitionImages.insert(data.at(0).trimmed(),data.at(1).trimmed());
        }
        file.close();
    }

    double diff=0.0;
    while (goodRecognition<efficiency) {
        Trainer trainer;
        double procentRecognition = 0.0;
        trainer.trainLetters(allChars);

        QHashIterator<QString, QString> i(m_recognitionImages);
        while (i.hasNext()) {
            i.next();
            QString correctText = i.value();
            correctText=correctText.remove(" ");
            double procent=trainer.recognitionImage(i.key(),correctText);
            procentRecognition+=procent;
            if (procent<90.0)
                QLogger::QLog_Trace("Recognition", QString("%1 : [%2] = %3%").arg(i.key()).arg(i.value()).arg(procent));
        }
        procentRecognition/=m_recognitionImages.size();
        if (max<procentRecognition) {
            max=procentRecognition;
            if (QFile(dataPath+"/data/mlp.yml").exists())
                QFile::rename(dataPath+"/data/mlp.yml", dataPath+QString("/data/mlp_%1.yml").arg(QDateTime::currentDateTime().toString("yyyy-MM-dd HH-mm-ss")));

            QFile::rename(dataPath+"/data/mlp_last.yml", dataPath+"/data/mlp.yml");
        }

        diff = procentRecognition-goodRecognition;

        qDebug()<< qPrintable(QString(" attempt: %1 - %2% (%3%)").arg(counterAttempts+1).arg(procentRecognition).arg(diff));
        if (procentRecognition==100.0 || procentRecognition>efficiency) {
            clearDir(pathIncorrect);
            break;
        }
        if (goodRecognition<procentRecognition) {
            isAddLetters = true;
        } else {
            isAddLetters = false;
        }
        goodRecognition = procentRecognition;
        diff = qAbs(diff)/100.0;
        if (isAddLetters) {
            addLetters(listLetters,diff);
        } else {
            deleteLetters(listLetters,diff);
            addLetters(listLetters,diff);
        }

        clearDir(pathIncorrect);
        counterAttempts++;
        if (counterAttempts==MAX_ATTEMPTS)
            break;
    }

    qDebug()<<qPrintable(QString("max: %1").arg(max));
}

QStringList Tools::getListIncorrectLetters()
{
    QStringList chars;
    QString path=dataPath+"/recognition/incorrectChars/";
    QStringList filters;
    filters << "*.png";
    QDir allDir(path);
    QStringList allList = allDir.entryList(QDir::Dirs|QDir::NoDotAndDotDot);
    int numCharacters = allList.count();
    for (int i=0;i<numCharacters;++i) {
        QDir letterDir(path+allList.at(i));
        letterDir.setNameFilters(filters);
        QStringList letterList = letterDir.entryList(QDir::Files);
        for (int j=0;j<letterList.count();++j) {
            QString pathChar=allList.at(i)+QDir::separator()+letterList.at(j);
            chars.push_back(pathChar);
        }
    }

    return chars;
}

void Tools::clearDir( const QString &path )
{
    QDir dir( path );

    dir.setFilter( QDir::NoDotAndDotDot | QDir::Files );
    foreach( QString dirItem, dir.entryList() )
        dir.remove( dirItem );

    dir.setFilter( QDir::NoDotAndDotDot | QDir::Dirs );
    foreach( QString dirItem, dir.entryList() )
    {
        QDir subDir( dir.absoluteFilePath( dirItem ) );
        subDir.removeRecursively();
    }
}

void Tools::addLetters(QStringList &listLetters, const double &diff)
{
    Q_ASSERT(diff>=0.0);
    QString pathALL=dataPath+"/recognition/allChars/";
    QString pathIncorrect=dataPath+"/recognition/incorrectChars/";
    QStringList chars = getListIncorrectLetters();
    int sizeLetters = chars.count();
    QStringList newListLetters;
    if (sizeLetters>=12)
        while(newListLetters.size() < sizeLetters * diff) {
            int drawnNumber=qrand()%sizeLetters;
            QString drawnLetter = chars.at(drawnNumber);
            if (!newListLetters.contains(drawnLetter))
                newListLetters.append(drawnLetter);
        }

    for (int i = 0; i < newListLetters.size(); ++i) {
        QString newLetter = newListLetters.at(i);
        if (!listLetters.contains(newLetter))  {

            if (!QDir(pathALL+QDir::separator()+newLetter.at(0)).exists())
                QDir(pathALL).mkpath(newLetter.at(0));
            while(!QFile::copy(pathIncorrect+newLetter, pathALL+newLetter)) {
                QRegExp exp("\\w[\\\\/]\\w_(\\d{1,})\\.png");
                if (newLetter.indexOf(exp)>-1) {
                    int number = exp.cap(1).toInt();
                    int next=number+1;
                    newLetter = newLetter.replace(QString::number(number),QString::number(next));
                } else
                    break;
            }
            listLetters.append(newLetter);
        }
    }
}

void Tools::deleteLetters(QStringList &listLetters, const double &diff)
{
    Q_ASSERT(diff>=0.0);
    QString pathALL=dataPath+"/recognition/allChars/";
    int sizeLetters = listLetters.size();
    QStringList deleteLetters;
    while(deleteLetters.size()<sizeLetters * diff) {
        int drawnNumber=qrand()%sizeLetters;
        QString drawnLetter = listLetters.at(drawnNumber);
        if (!deleteLetters.contains(drawnLetter))
            deleteLetters.append(drawnLetter);
    }
    for(int i=0; i<deleteLetters.size(); ++i) {
        QString letter = deleteLetters.at(i);
        if (QFile::remove(pathALL+letter))
            listLetters.removeAll(letter);

    }
}

void Tools::createCropStation()
{
    Recognition::PreperationImages *prepareStation = new Recognition::PreperationImages("Station");
    createProcessingImage(screenshots,preparations,prepareStation);
    delete prepareStation;
}

void Tools::createCropGoods()
{
    Recognition::PreperationImages *prepareGoods = new Recognition::PreperationImages("Goods");
    createProcessingImage(screenshots,preparations,prepareGoods);
    delete prepareGoods;
}

void Tools::createSegmentation()
{
    Recognition::SegmentationImages *segmentationImages = new Recognition::SegmentationImages();
    createProcessingImage(preparations,segmentations,segmentationImages);
    delete segmentationImages;
}

void Tools::createRecognition()
{
    Recognition::RecognitionImages *recognitionImages = new Recognition::RecognitionImages();
    createProcessingImage(segmentations,recognitions,recognitionImages);

    delete recognitionImages;
}

void Tools::isolationCharacters()
{
    static int number=1;
    QString pathALL=dataPath+"/recognition/allChars/";
    QStringList filters;

    filters << "*.png";
    segmentations.setNameFilters(filters);
    QStringList filenames=segmentations.entryList(QDir::Files);

    for (int numberOfFile=0;numberOfFile<filenames.size();++numberOfFile) {
        QString fileName=filenames.at(numberOfFile);
        QString filePathSource = segmentations.absolutePath()+QDir::separator()+fileName;
        QImage image(filePathSource);
        int counterLetter = image.width()/21;

        for (int numberOfLetter=0;numberOfLetter<counterLetter;++numberOfLetter) {
            QImage temp = image.copy(numberOfLetter*21,0,20,20);
            temp.save(pathALL+QString("char_%1.png").arg(number++));
        }
    }
}
