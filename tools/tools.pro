include(../library.pri)
QT       += core

CONFIG   += console
CONFIG   -= app_bundle

TARGET = tools
TOOL_VERSION = 0.7.2
TEMPLATE = app

APP_BASE = ..
DESTDIR = $$APP_BASE/EliteDangerousTrading

DEFINES += APP_VERSION=\\\"$$TOOL_VERSION\\\"

DEPENDPATH += . \
    ../app \
    ../3rdparty/QLogger

INCLUDEPATH += . \
    ../app \
    ../3rdparty/QLogger

SOURCES += main.cpp \
    ../app/preparationstaticimage.cpp \
    ../app/preperationimages.cpp \
    ../app/fields.cpp \
    ../app/segmentationstaticimage.cpp \
    ../app/segmentationimages.cpp \
    ../app/recognitionimages.cpp \
    ../app/recognitionstaticimage.cpp \
    ../app/trainer.cpp \
    tools.cpp \
    ../3rdparty/QLogger/QLogger.cpp

HEADERS += \
    ../app/preparationstaticimage.h \
    ../app/preperationimages.h \
    ../app/fields.h \
    ../app/commandInterface.h \
    ../app/segmentationstaticimage.h \
    ../app/segmentationimages.h \
    ../app/recognitionimages.h \
    ../app/recognitionInterface.h \
    ../app/recognitionstaticimage.h \
    ../app/trainer.h \
    tools.h \
    ../3rdparty/QLogger/QLogger.h
DEFINES += SRCDIR=\\\"$$PWD/\\\"

OTHER_FILES += \
    command_tools.txt

RESOURCES += \
    resource.qrc


