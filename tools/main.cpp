#include <QGuiApplication>

#include <QDebug>
#include "tools.h"

void help()
{
    QFile helpFile(":/command_tools.txt");
    if (helpFile.open(QIODevice::Text|QIODevice::ReadOnly)) {
        QTextStream in(&helpFile);
        std::cout<<in.readAll().toStdString()<<std::endl;
        helpFile.close();
    }
}

void version()
{
    std::cout<<"Elite:Dangerous Trading Tools, ";
    std::cout<<"version: "<<qApp->applicationVersion().toStdString()<<std::endl;
    std::cout<<"Author: Ireneusz Bohatyrewicz (demiurge)"<<std::endl;
}

void folder()
{
    Tools tools;
    std::cout<<"Path to data folder: "<<tools.dataFolder().toStdString()<<std::endl;
}

void listFields(const QString &nameField = QString(""))
{
    if (nameField.isEmpty()) {
        QStringList listNames=Fields::getInstance()->getAllFieldName();
        foreach (QString name, listNames) {
            std::cout<<qPrintable(name)<<std::endl;
        }
    } else {
        Field field=Fields::getInstance()->getField(nameField);
        if (field.exists()) {
            std::cout<<"field [name]: "<<qPrintable(field.getName())<<std::endl;
            QRect rect=field.getCropArea();
            std::cout<<"[crop] area: "<<qPrintable(QString("%1.%2-%3x%4").arg(rect.x()).arg(rect.y()).arg(rect.width()).arg(rect.height()))<<std::endl;
            QSize size=field.getSizeDest();
            std::cout<<"[size] destination image: "<<qPrintable(QString("%1x%2").arg(size.width()).arg(size.height()))<<std::endl;
            QPolygon source=field.getAnchorsSource();
            std::cout<<"[source] anchors:"<<std::endl;
            for (int i=0;i<source.size();i++) {
                QPoint point=source.at(i);
                std::cout<<qPrintable(QString("\t%1. %2.%3").arg(i+1).arg(point.x()).arg(point.y()))<<std::endl;
            }
            QPolygon dest=field.getAnchorsDest();
            std::cout<<"[destination] anchors:"<<std::endl;
            for (int i=0;i<dest.size();i++) {
                QPoint point=dest.at(i);
                std::cout<<qPrintable(QString("\t%1. %2.%3").arg(i+1).arg(point.x()).arg(point.y()))<<std::endl;
            }
            QList<QRect> listAreas = field.getSegmentationAreas();
            std::cout<<"[segmentation] areas:"<<std::endl;
            for (int i=0;i<listAreas.size();i++) {
                QRect rect=listAreas.at(i);
                std::cout<<qPrintable(QString("\t%1. %2.%3-%4x%5").arg(i+1).arg(rect.x()).arg(rect.y()).arg(rect.width()).arg(rect.height()))<<std::endl;
            }
        } else {
            std::cout<<"No exists field of name: "<<qPrintable(nameField)<<std::endl;
        }
    }
}

void createField(const QString &nameField) {
    Fields::getInstance()->createField(nameField);
}

void deleteField(const QString &nameField) {
    Fields::getInstance()->deleteField(nameField);
}

void modifyField(const QString &nameField, const QString &parametr, const QString &value) {
    Fields::getInstance()->modifyField(nameField,parametr,value);
}

void prep(const QString &nameFolderWithScreenshots,const QString &nameFolderWithPreparations) {
    Tools tools;
    tools.setFolderPath(nameFolderWithScreenshots,CAPTION);
    tools.setFolderPath(nameFolderWithPreparations,PREPARATION);

    tools.createCropStation();
    tools.createCropGoods();
}

void segm(const QString &nameFolderWithPreparations,const QString &nameFolderWithSegmentations) {
    Tools tools;
    tools.setFolderPath(nameFolderWithPreparations,PREPARATION);
    tools.setFolderPath(nameFolderWithSegmentations,SEGMENTATION);

    tools.createSegmentation();
}

void recg(const QString &nameFolderWithSegmentations,const QString &nameFolderWithRecognitions) {
    Tools tools;

    tools.setFolderPath(nameFolderWithSegmentations,SEGMENTATION);
    tools.setFolderPath(nameFolderWithRecognitions,RECOGNITION);

    tools.createRecognition();
}

void collect(const QString &nameFolderWithSegmentations) {
    Tools tools;
    tools.setFolderPath(nameFolderWithSegmentations,SEGMENTATION);
    tools.isolationCharacters();
}

void learn(const QString &nameFolderWithSegmentations,const double &efficiency = 0.0) {
    Tools tools;
    tools.setFolderPath(nameFolderWithSegmentations,SEGMENTATION);

    if (efficiency<=0.0) {
        tools.saveRecognitionText();
    } else {
        tools.learn(efficiency);
    }
}

void cmdline(const QStringList &list)
{
    if (list.size()==1) {
        help();
        return;
    } else {
        QString command=list.at(1);
        if (command.compare("-help")==0) {
            help();
        }
        else if (command.compare("-version")==0) {
            version();
        }
        else if (command.compare("-folder")==0) {
            folder();
        }
        else if (command.compare("-list")==0) {
            if (list.size()==4 && !list.at(2).isEmpty() && !list.at(3).isEmpty()) {
                QString nameField=list.at(2).toLower();
                nameField[0]=nameField.at(0).toUpper();
                QString resolution=list.at(3).toLower();

                listFields(QString(nameField+"-"+resolution));
            }
            else if (list.size()==3 && !list.at(2).isEmpty()) {
                listFields(list.at(2));
            }
            else
                listFields();
        }
        else if (command.compare("-create")==0) {
            if (list.size()==4 && !list.at(2).isEmpty() && !list.at(3).isEmpty()) {
                QString nameField=list.at(2).toLower();
                nameField[0]=nameField.at(0).toUpper();
                QString resolution=list.at(3).toLower();

                createField(QString(nameField+"-"+resolution));
            }
            else if (list.size()==3 && !list.at(2).isEmpty()) {
                QString resolution=list.at(2).toLower();
                QRegExp regexp("(\\d{1,})x(\\d{1,})");
                if (regexp.indexIn(resolution)>-1) {
                    createField(QString("Station-%1x%2").arg(regexp.cap(1)).arg(regexp.cap(2)));
                    createField(QString("Goods-%1x%2").arg(regexp.cap(1)).arg(regexp.cap(2)));
                }
            }
        }
        else if (command.compare("-delete")==0) {
            if (list.size()==4 && !list.at(2).isEmpty() && !list.at(3).isEmpty()) {
                QString nameField=list.at(2).toLower();
                nameField[0]=nameField.at(0).toUpper();
                QString resolution=list.at(3).toLower();

                deleteField(QString(nameField+"-"+resolution));
            }
            else if (list.size()==3 && !list.at(2).isEmpty()) {
                QString resolution=list.at(2).toLower();
                QRegExp regexp("(\\d{1,})x(\\d{1,})");
                if (regexp.indexIn(resolution)>-1) {
                    deleteField(QString("Station-%1x%2").arg(regexp.cap(1)).arg(regexp.cap(2)));
                    deleteField(QString("Goods-%1x%2").arg(regexp.cap(1)).arg(regexp.cap(2)));
                }
            }
        }
        else if (command.compare("-modify")==0) {
            if (list.size()==6 && !list.at(2).isEmpty() && !list.at(3).isEmpty() && !list.at(4).isEmpty() && !list.at(5).isEmpty()) {
                QString nameField=list.at(2).toLower();
                nameField[0]=nameField.at(0).toUpper();
                QString resolution=list.at(3).toLower();
                QString parametr=list.at(4);
                QString value=list.at(5);
                modifyField(QString(nameField+"-"+resolution),parametr,value);
            } else if (list.size()==5 && !list.at(2).isEmpty() && !list.at(3).isEmpty() && !list.at(4).isEmpty()) {
                QStringList nameAndResolution=list.at(2).split("-");
                QString nameField=nameAndResolution.at(0).toLower();
                nameField[0]=nameField.at(0).toUpper();
                QString resolution=nameAndResolution.at(1).toLower();
                QString parametr=list.at(3);
                QString value=list.at(4);
                modifyField(QString(nameField+"-"+resolution),parametr,value);
            }
        }
        else if (command.compare("-prep")==0) {
            if (list.size()==3 && !list.at(2).isEmpty()) {
                QString nameFolderWithScreenshots=list.at(2);

                QDir folder(nameFolderWithScreenshots);
                folder.makeAbsolute();
                nameFolderWithScreenshots=folder.absolutePath();
                if (folder.exists()) {
                    folder.cdUp();
                    folder.mkpath("preparations");
                    folder.cd("preparations");
                    QString nameFolderWithPreparations = folder.absolutePath();
                    prep(nameFolderWithScreenshots,nameFolderWithPreparations);
                }
            }
        }
        else if (command.compare("-segm")==0) {
            if (list.size()==3 && !list.at(2).isEmpty()) {
                QString nameFolderWithPreparations=list.at(2);

                QDir folder(nameFolderWithPreparations);
                folder.makeAbsolute();
                nameFolderWithPreparations=folder.absolutePath();
                if (folder.exists()) {
                    folder.cdUp();
                    folder.mkpath("segmentations");
                    folder.cd("segmentations");
                    QString nameFolderWithSegmentations = folder.absolutePath();
                    segm(nameFolderWithPreparations,nameFolderWithSegmentations);
                }
            }
        }
        else if (command.compare("-recg")==0) {
            if (list.size()==3 && !list.at(2).isEmpty()) {
                QString nameFolderWithSegmentations=list.at(2);

                QDir folder(nameFolderWithSegmentations);
                folder.makeAbsolute();
                nameFolderWithSegmentations=folder.absolutePath();
                if (folder.exists()) {
                    folder.cdUp();
                    folder.mkpath("recognitions");
                    folder.cd("recognitions");
                    QString nameFolderWithRecognitions = folder.absolutePath();
                    recg(nameFolderWithSegmentations,nameFolderWithRecognitions);
                }
            }
        }
        else if (command.compare("-collect")==0) {
            if (list.size()==3 && !list.at(2).isEmpty()) {
                QString nameFolderWithSegmentations=list.at(2);

                QDir folder(nameFolderWithSegmentations);
                folder.makeAbsolute();
                nameFolderWithSegmentations=folder.absolutePath();

                collect(nameFolderWithSegmentations);
            }

        }
        else if (command.compare("-learn")==0) {
            if (list.size()>=3 && list.size()<=4 && !list.at(2).isEmpty()) {
                QString nameFolderWithSegmentations=list.at(2);

                QDir folder(nameFolderWithSegmentations);
                folder.makeAbsolute();
                nameFolderWithSegmentations=folder.absolutePath();

                if (list.size()==4) {
                    double efficiency=list.at(3).toDouble();
                    learn(nameFolderWithSegmentations,efficiency);
                } else
                    learn(nameFolderWithSegmentations);
            }

        }
    }

}

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);
    app.setApplicationVersion(APP_VERSION);
    QStringList cmdline_args = QCoreApplication::arguments();
    cmdline(cmdline_args);

    return 0;
}
