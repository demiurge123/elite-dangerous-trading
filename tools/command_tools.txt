Command Elite:Dangerous Trading Tools

1. INFORMATION ABOUT THE PROGRAM

    1.1 Version

        tools -version

    1.2 Help

        tools -help

    1.3 Folder include data (recognition, data, logs)

        tools -folder

2. EDIT TEXT RECOGNITION FIELDS

    2.1 List fields

        tools -list

    2.2 Create field

        tools -create nameField resolution

    2.3 Delete field

        tools -delete nameField resolution

    2.4 List parameters field

        tools -list nameField resolution

    2.5 Modify parameters field

        tools -modify nameField resolution parametr value

3. PREPARATION, SEGMENTATION AND RECOGNITION IMAGES

    3.1 Preparation images

        tools -prep path_to_folder_with_screenshots

    3.2 Segmentation images

        tools -segm path_to_folder_with_preparations

    3.3 Recognition images

        tools -recg path_to_folder_with_segmentations

4. LEARN RECOGNITION CHARS

    4.1 The collection of characters

        tools -collect path_to_folder_with_segmentations

    4.2 Learn recognition chars

        tools -learn path_to_folder_with_segmentations [threshold]

threshold - number (in procent) that specifies the threshold of successful recognized characters.
            When you omit this parameter creates a file called recognition.txt.
