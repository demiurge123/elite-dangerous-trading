set QTDIR=c:\Qt\Qt5.3.1\5.3\msvc2013_opengl
set PATH=%QTDIR%\bin;%PATH%

copy "c:\opencv\build\x86\vc12\bin\opencv_core249.dll" . /Y
copy "c:\opencv\build\x86\vc12\bin\opencv_ml249.dll" . /Y
copy "c:\opencv\build\x86\vc12\bin\opencv_imgproc249.dll" . /Y
copy "c:\Program Files (x86)\Microsoft Visual Studio 12.0\VC\redist\x86\Microsoft.VC120.CRT\msvcp120.dll" . /Y
copy "c:\Program Files (x86)\Microsoft Visual Studio 12.0\VC\redist\x86\Microsoft.VC120.CRT\msvcr120.dll" . /Y
copy ..\lib\qhttpserver.dll . /Y

windeployqt.exe server.exe
windeployqt.exe tools.exe
windeployqt.exe EliteDangerousTrading.exe
