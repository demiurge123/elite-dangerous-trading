<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="pl_PL">
<context>
    <name>Dialog</name>
    <message>
        <location filename="../dialog.ui" line="32"/>
        <source>Elite:Dangerous Trading</source>
        <translation>Handlowanie w Elite:Dangerous</translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="54"/>
        <source>Scan</source>
        <translation>Skan</translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="76"/>
        <source>Capture image</source>
        <translation>Przechwyć obraz</translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="182"/>
        <source>Nowy wiersz</source>
        <translation>Nowy wiersz</translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="187"/>
        <source>Station name</source>
        <translation>Nazwa stacji</translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="192"/>
        <source>Commodity</source>
        <translation>Towar</translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="197"/>
        <source>Sell Price</source>
        <translation>Sprzedaż</translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="202"/>
        <source>Buy Price</source>
        <translation>Kupno</translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="207"/>
        <source>Supply</source>
        <translation>Dostawa</translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="234"/>
        <source>Stations</source>
        <translation>Stacje</translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="298"/>
        <source>Trading</source>
        <translation>Handel</translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="306"/>
        <source>Starship</source>
        <translation>Statek</translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="318"/>
        <source>capital</source>
        <translation>fundusze</translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="339"/>
        <source>Siderwinder Mk.I</source>
        <translation>Siderwinder Mk.I</translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="344"/>
        <source>Eagle Mk.II</source>
        <translation>Eagle Mk.II</translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="349"/>
        <source>Zorgon Peterson Hauler</source>
        <translation>Zorgon Peterson Hauler</translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="354"/>
        <source>Cobra Mk.III</source>
        <translation>Cobra Mk.III</translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="359"/>
        <source>Anaconda</source>
        <translation>Anaconda</translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="364"/>
        <source>Lakon Type 9</source>
        <translation>Lakon Type 9</translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="375"/>
        <source>Calculate</source>
        <translation>Oblicz trase</translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="397"/>
        <source>Systems</source>
        <translation>Systemy</translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="409"/>
        <source>start Station</source>
        <translation>początkowa stacja</translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="425"/>
        <source>end Station</source>
        <translation>końcowa stacja</translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="442"/>
        <source>Space</source>
        <translation>Kosmos</translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="448"/>
        <source>Max hiperspace jumps: 1</source>
        <translation>Maksymalna liczba skoków hiperprzestrzennych: 1</translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="474"/>
        <source>Export to PDF</source>
        <translation>Eksportuj do PDF</translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="484"/>
        <source>Settings</source>
        <translation>Ustawienia</translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="496"/>
        <source>Server</source>
        <translation>Serwer</translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="502"/>
        <source>Use EDT server and share other players common data collection</source>
        <translation>Użyj serwera i udostępniaj innym graczom wspólne dane</translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="509"/>
        <source>port</source>
        <translation>port</translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="516"/>
        <source>address</source>
        <translation>adres</translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="539"/>
        <source>http://mar-eu-1-snbp4i3n.qtcloudapp.com</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="555"/>
        <source>Scan resolution</source>
        <translation>Rozdzielczość skanowania</translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="561"/>
        <source>Use EDT tools to add another (see manual)</source>
        <translation>Użyj narzędzia do tworzenia nowych rozdzielczości skanowania</translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="568"/>
        <source>available</source>
        <translation>dostępne</translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="594"/>
        <source>Screenshot</source>
        <translation>Zrzut z gry</translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="600"/>
        <source>make screenshot when quality recognition  is below 75%</source>
        <translation>twórz zrzuty z gry, kiedy jakość rozpoznawania tekstu spadnie poniżej 75%</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="505"/>
        <source>make screenshot when quality recognition  is below %1%</source>
        <translation>twórz zrzuty z gry, kiedy jakość rozpoznawania tekstu spadnie poniżej %1%</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="510"/>
        <source>Max hiperspace jumps: %1</source>
        <translation>Maksymalna liczba skoków hiperprzestrzennych: %1</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="614"/>
        <source>Starship:%1</source>
        <translation>Statek:%1</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="637"/>
        <source>Start system:</source>
        <translation>Początkowy system:</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="651"/>
        <source>End system:</source>
        <translation>Końcowy system:</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="673"/>
        <source>Capital: %1, profit: +%2</source>
        <translation>Fundusze: %1, zysk: +%2</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="689"/>
        <source>Total profit: +%1</source>
        <translation>Całość zysków: +%1</translation>
    </message>
</context>
<context>
    <name>Station</name>
    <message>
        <location filename="../station.cpp" line="75"/>
        <source>Name</source>
        <translation>Nazwa</translation>
    </message>
    <message>
        <location filename="../station.cpp" line="77"/>
        <source>Sell</source>
        <translation>Sprzedaż</translation>
    </message>
    <message>
        <location filename="../station.cpp" line="79"/>
        <source>Buy</source>
        <translation>Kupno</translation>
    </message>
    <message>
        <location filename="../station.cpp" line="81"/>
        <source>Supply</source>
        <translation>Dostawa</translation>
    </message>
    <message>
        <location filename="../station.cpp" line="83"/>
        <source>Update</source>
        <translation>Dodane</translation>
    </message>
</context>
</TS>
