<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
<context>
    <name>Dialog</name>
    <message>
        <location filename="../dialog.ui" line="32"/>
        <source>Elite:Dangerous Trading</source>
        <translation>Elite:Dangerous Trading</translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="54"/>
        <source>Scan</source>
        <translation>Scan</translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="76"/>
        <source>Capture image</source>
        <translation>Capture image</translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="182"/>
        <source>Nowy wiersz</source>
        <translation>New row</translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="187"/>
        <source>Station name</source>
        <translation>Station name</translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="192"/>
        <source>Commodity</source>
        <translation>Commodity</translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="197"/>
        <source>Sell Price</source>
        <translation>Sell Price</translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="202"/>
        <source>Buy Price</source>
        <translation>Buy Price</translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="207"/>
        <source>Supply</source>
        <translation>Supply</translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="234"/>
        <source>Stations</source>
        <translation>Stations</translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="298"/>
        <source>Trading</source>
        <translation>Trading</translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="306"/>
        <source>Starship</source>
        <translation>Starship</translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="318"/>
        <source>capital</source>
        <translation>capital</translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="339"/>
        <source>Siderwinder Mk.I</source>
        <translation>Siderwinder Mk.I</translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="344"/>
        <source>Eagle Mk.II</source>
        <translation>Eagle Mk.II</translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="349"/>
        <source>Zorgon Peterson Hauler</source>
        <translation>Zorgon Peterson Hauler</translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="354"/>
        <source>Cobra Mk.III</source>
        <translation>Cobra Mk.III</translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="359"/>
        <source>Anaconda</source>
        <translation>Anaconda</translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="364"/>
        <source>Lakon Type 9</source>
        <translation>Lakon Type 9</translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="375"/>
        <source>Calculate</source>
        <translation>Calculate route</translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="397"/>
        <source>Systems</source>
        <translation>Systems</translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="409"/>
        <source>start Station</source>
        <translation>start station</translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="425"/>
        <source>end Station</source>
        <translation>end station</translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="442"/>
        <source>Space</source>
        <translation>Space</translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="448"/>
        <source>Max hiperspace jumps: 1</source>
        <translation>Max hiperspace jumps: 1</translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="474"/>
        <source>Export to PDF</source>
        <translation>Export to PDF</translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="484"/>
        <source>Settings</source>
        <translation>Settings</translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="496"/>
        <source>Server</source>
        <translation>Server</translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="502"/>
        <source>Use EDT server and share other players common data collection</source>
        <translation>Use EDT server and share other players common data collection</translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="509"/>
        <source>port</source>
        <translation>port</translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="516"/>
        <source>address</source>
        <translation>address</translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="539"/>
        <source>http://mar-eu-1-snbp4i3n.qtcloudapp.com</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="555"/>
        <source>Scan resolution</source>
        <translation>Scan resolution</translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="561"/>
        <source>Use EDT tools to add another (see manual)</source>
        <translation>Use EDT tools to add another (see manual)</translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="568"/>
        <source>available</source>
        <translation>available</translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="594"/>
        <source>Screenshot</source>
        <translation>Screenshot</translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="600"/>
        <source>make screenshot when quality recognition  is below 75%</source>
        <translation>make screenshot when quality recognition  is below 75%</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="505"/>
        <source>make screenshot when quality recognition  is below %1%</source>
        <translation>make screenshot when quality recognition  is below %1%</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="510"/>
        <source>Max hiperspace jumps: %1</source>
        <translation>Max hiperspace jumps: %1</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="614"/>
        <source>Starship:%1</source>
        <translation>Starship:%1</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="637"/>
        <source>Start system:</source>
        <translation>Start system:</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="651"/>
        <source>End system:</source>
        <translation>End system:</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="673"/>
        <source>Capital: %1, profit: +%2</source>
        <translation>Capital: %1, profit: +%2</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="689"/>
        <source>Total profit: +%1</source>
        <translation>Total profit: +%1</translation>
    </message>
</context>
<context>
    <name>Station</name>
    <message>
        <location filename="../station.cpp" line="75"/>
        <source>Name</source>
        <translation>Name</translation>
    </message>
    <message>
        <location filename="../station.cpp" line="77"/>
        <source>Sell</source>
        <translation>Sell</translation>
    </message>
    <message>
        <location filename="../station.cpp" line="79"/>
        <source>Buy</source>
        <translation>Buy</translation>
    </message>
    <message>
        <location filename="../station.cpp" line="81"/>
        <source>Supply</source>
        <translation>Supply</translation>
    </message>
    <message>
        <location filename="../station.cpp" line="83"/>
        <source>Update</source>
        <translation>Update</translation>
    </message>
</context>
</TS>
