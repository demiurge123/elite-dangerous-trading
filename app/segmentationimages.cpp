#include <QDebug>
#include "segmentationimages.h"

using namespace Recognition;

SegmentationImages::SegmentationImages(QObject *parent)
    : QObject(parent)
{
    m_staticImage = new SegmentationStaticImage(this);
}

SegmentationImages::~SegmentationImages()
{
    delete m_staticImage;
}

void SegmentationImages::execute()
{
    m_staticImage->threshold();
    m_staticImage->countour();
    m_staticImage->rectangle();
}

void SegmentationImages::setFileNameObject(const QString &filename)
{
    m_staticImage->setImage(filename);
}

void SegmentationImages::setImageObject(const QImage & image)
{
    m_staticImage->setImage(image);
}
