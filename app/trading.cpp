#include <QDebug>
#include <iostream>
#include "trading.h"


int Profit::getBuyPrice() const
{
    return buyPrice;
}

void Profit::setBuyPrice(int value)
{
    buyPrice = value;
}

int Profit::getSellPrice() const
{
    return sellPrice;
}

void Profit::setSellPrice(int value)
{
    sellPrice = value;
}

int Profit::getSupply() const
{
    return supply;
}

void Profit::setSupply(int value)
{
    supply = value;
}

Profit::Profit(const QString &commodity, const int &buyPrice, const int &sellPrice, const int &supply)
    : commodity(commodity), buyPrice(buyPrice), sellPrice(sellPrice), supply(supply)
{

}


Trading::Trading(Galactica *galactica)
    : galactica(galactica)
{

}

void Trading::initialProfits()
{
    foreach(Station* startStation,galactica->allStations()) {
        int idStartStation = startStation->id();
        foreach (Station* endStation, galactica->allStations()) {
            int idEndStation = endStation->id();
            if (idStartStation != idEndStation) {
                foreach (Good* startGoods, startStation->goods()) {
                    QString nameCommodity = startGoods->name();
                    int buyPrice = startGoods->buyPrice();
                    int supply = startGoods->supply();
                    if (buyPrice == 0 || supply == 0)
                        continue;
                    foreach (Good* endGoods, endStation->goods()) {
                        QString compareCommodity = endGoods->name();
                        int sellPrice = endGoods->sellPrice();
                        if (nameCommodity.compare(compareCommodity)==0 && buyPrice < sellPrice) {
                            Profit* profit = new Profit(nameCommodity,buyPrice, sellPrice,supply);
                            m_profits.insertMulti(QPair<int,int>(idStartStation,idEndStation),profit);
                        }
                    }
                }
            }
        }
    }
}

void Trading::setCargo(const int &cargo)
{
    m_connections.clear();
    for (int s=0;s<galactica->allStations().size();++s)
        for (int e=0;e<galactica->allStations().size();++e)
            if (s!=e) {
                int start = galactica->station(s)->id();
                int end = galactica->station(e)->id();
                newKnapsackTrading(start,end,cargo);
            }
}

void Trading::newKnapsackTrading(const int &start, const int &end, const int &cargo)
{
    QPair<int, int> link(start,end);

    int maxCapital = 0;
    QList<Profit *> profits=possibleProfits(start,end);
    foreach(Profit *profit,profits) {
        quint16 valueProfit = profit->getBuyPrice();
        if (maxCapital<valueProfit*(cargo+1)-1)
            maxCapital=valueProfit*(cargo+1)-1;
    }

    if (profits.size()>0) {
        quint32 **values;
        quint16 **quantity;
        quint16 **containers;

        
        QVector<int> profitValues;
        QVector<int> profitBuyPrices;
        QVector<int> profitSupplies;
        
        for (int i=0;i<profits.size();++i) {
            profitValues.push_back(profits.at(i)->value());
            profitBuyPrices.push_back(profits.at(i)->getBuyPrice());
            profitSupplies.push_back(profits.at(i)->getSupply());
        }
        
//        const int maxCapital = qMin( (cargo+1) *  profitBuyPrices.last()-1, maxCapital);

//        profitValues<<1<<2<<3;
//        profitBuyPrices<<2<<3<<4;
//        const int maxCapital = 19;

        values=new quint32*[profits.size()+1];
        for (int i=0;i<=profits.size();i++)
            values[i] = new quint32[maxCapital+1];
        quantity=new quint16*[profits.size()+1];
        for (int i=0;i<=profits.size();i++)
            quantity[i] = new quint16[maxCapital+1];
        containers=new quint16*[profits.size()+1];
        for (int i=0;i<=profits.size();i++)
            containers[i] = new quint16[maxCapital+1];

        for (int i=0;i<=profits.size();i++)
            for (int j=0;j<=maxCapital;j++) {
                values[i][j]=0;
                quantity[i][j]=0;
                containers[i][j]=0;
            }




        for (int j=1;j<=maxCapital;j++) {
            for (int i=1;i<=profits.size();i++) {

                int limited = qMin(cargo,profitSupplies.at(i-1));
                int buy=profitBuyPrices.at(i-1);
                if (j>=buy && j<(limited+1)*buy) {
                    quantity[i][j] = quantity[i][j-buy]+1;
                    int value = quantity[i][j]*profitValues.at(i-1);
                    int step = quantity[i][j]*profitBuyPrices.at(i-1);

                    quint32 maxValueOptimal=0;
                    quint16 maxContainersOptimal=0;
                    if (quantity[i][j]<cargo)
                    for (int k=1;k<=profits.size();++k) {
                        if (maxValueOptimal<values[k][j-step] && quantity[i][j]+containers[k][j-step]<=cargo) {
                            maxValueOptimal = values[k][j-step];
                            maxContainersOptimal = containers[k][j-step];
                        } else
                            if (values[k][j-step]>0)
                                maxContainersOptimal = cargo+1;
                    }
                    values[i][j] = value+maxValueOptimal;
                    if (maxContainersOptimal>0)
                        containers[i][j] = quantity[i][j]+maxContainersOptimal;
                    else
                        containers[i][j] = quantity[i][j];
//                    Q_ASSERT(containers[i][j]<=cargo);
//                    qDebug()<<qPrintable(QString("(%1,%2) [%3] [%4] [%5]").arg(i).arg(j).arg(values[i][j]).arg(quantity[i][j]).arg(containers[i][j]));
                }
            }
        }


        for (int i=1;i<=profits.size();i++) {
            quint32 maxProfit = 0;
            for (int j=1;j<=maxCapital;j++) {

                int buy=0;
                if (maxProfit<values[i][j] && containers[i][j]<=cargo) {
                    maxProfit = values[i][j];
                    int container = containers[i][j];
                    QList<int> selectedCommodities;
                    for (int k=1;k<=quantity[i][j];++k) {
                        selectedCommodities.append(i-1);
                        buy+=profitBuyPrices.at(i-1);
                        container--;
                    }
                    int step = quantity[i][j]*profitBuyPrices.at(i-1);
                    while(container>0 && step<j) {

                        quint32 maxValue=0;
                        int l=0;
                        for (int k=1;k<=profits.size();++k)
                            if (maxValue<values[k][j-step] && quantity[i][j]+containers[k][j-step]<=cargo) {
                                maxValue=values[k][j-step];
                                l=k;
                            }

                        if (l>0) {
                            for (int k=1;k<=quantity[l][j-step];++k) {
                                selectedCommodities.append(l-1);
                                buy+=profitBuyPrices.at(l-1);
                                container--;
                            }
                            step += quantity[l][j-step]*profitBuyPrices.at(l-1);
                        }
                        else
                            step = j;
                    }
                    QPair<int,QList<int> > computedData(maxProfit,selectedCommodities);
                    int limited = qMax(buy,j);
                    QPair<int, QPair<int,QList<int> > > bestProfitForCapital (limited,computedData);
                    m_connections.insertMulti(link,bestProfitForCapital);
                }

            }
        }

//        qDebug()<<maxProfit<<selectedCommodities;


        for (int i=0;i<=profits.size();i++)
            delete[] containers[i];
        delete[] containers;

        for (int i=0;i<=profits.size();i++)
            delete[] quantity[i];
        delete[] quantity;

        for (int i=0;i<=profits.size();i++)
            delete[] values[i];
        delete[] values;
    }

}

bool valueProfit(Profit *p1, Profit *p2) {
    return p1->getBuyPrice()<p2->getBuyPrice();
}

QList<Profit *> Trading::possibleProfits(const int &start, const int &end)
{
    QList<Profit *> profits=m_profits.values(QPair<int,int>(start,end));
//    QMutableListIterator<Profit *> it(profits);
//    while (it.hasNext()) {
//        Profit *profit=it.next();
//        if (profit->getBuyPrice() >capital || profit->getSupply()==0)
//            it.remove();
//    }
    qSort(profits.begin(),profits.end(),valueProfit);
    return profits;
}

int Trading::minMaxProfit(int level, int &startStation, int capital)
{
    if (level==0)
        return 0;
    QHash<int,int> profitsInStations;
    int start = startStation;

    for (int s=0;s<galactica->allStations().size();++s) {
        int endStation=galactica->station(s)->id();
        if (startStation!=endStation) {
            int oldStation=endStation;
            int profit=getProfit(startStation, endStation, capital);
            int maxProfit=profit+minMaxProfit(level-1,endStation,capital+profit);
            profitsInStations.insert(oldStation,maxProfit);
        }
    }
    if (level==3)
        level=3;
    int minMax=-1;
    QHashIterator<int, int> i(profitsInStations);
    while (i.hasNext()) {
        i.next();
        if (start!=i.key()) {
            if (level%2==0) { // min
                if (minMax==-1) {
                    minMax=i.value();
                    startStation = i.key();
                } else
                    if (minMax>i.value() ) {
                    minMax=i.value();
                    startStation = i.key();
                }
            }  else { // max
                    if (minMax==-1) {
                        minMax=i.value();
                        startStation = i.key();
                    }
                    else if (minMax<i.value() ) {
                        minMax=i.value();
                        startStation = i.key();
                    }
            }
        }

    }
    return minMax;
}

int Trading::getProfit(const int &start, const int &end, const int &capital)
{
    int profit=0;
    QPair<int, int> link(start,end);
    QList<QPair<int, QPair<int, QList<int> > > > capitalProfits = m_connections.values(link); // WTF!

    for(int i=0;i<capitalProfits.size();++i) {
        QPair<int, QPair<int, QList<int> > > capitalProfit = capitalProfits.at(i);

        QPair<int, QList<int> > computedData = capitalProfit.second;
        if (capitalProfit.first<=capital) {
            if (computedData.first>profit)
                profit = computedData.first;
        }
    }

    return profit;
}

QList<int> Trading::getCommodities(const int &start, const int &end, const int &capital)
{
    QList<int> listCommodities;
    int profit=0;
    QPair<int, int> link(start,end);
    QList<QPair<int, QPair<int, QList<int> > > > capitalProfits = m_connections.values(link); // WTF!

    for(int i=0;i<capitalProfits.size();++i) {
        QPair<int, QPair<int, QList<int> > > capitalProfit = capitalProfits.at(i);

        QPair<int, QList<int> > computedData = capitalProfit.second;
        if (capitalProfit.first<=capital) {
            if (computedData.first>profit) {
                profit = computedData.first;
                listCommodities = computedData.second;
            }
        }
    }

    return listCommodities;
}



QStringList Trading::bestTradingFor3More(int &capital, quint32 &maxProfit, int start, const int &jumps)
{
    QStringList allSelectedCommodities;
    int oldStart=-1;
    for (int j=0;j<jumps;++j) {
        int funds=capital;
        int end=start;
        minMaxProfit(3,end,funds);
        Q_ASSERT(start!=end);

        maxProfit = getProfit(start, end, funds);
        QList<int> selected=getCommodities(start, end, funds);
        Q_ASSERT(selected.size()>0);
        QList<int> uniqueSelected(selected);
        qSort(uniqueSelected.begin(), uniqueSelected.end(), qLess<int>() );
        QList<int>::iterator iter = std::unique (uniqueSelected.begin(), uniqueSelected.end());
        uniqueSelected.erase(iter, uniqueSelected.end());

        QStringList selectedCommodities;
        QList<Profit *> profits=possibleProfits(start,end);
        if (uniqueSelected.size()>0) {
            selectedCommodities.append(QString("%1 -> %2|%3+%4")
                                       .arg(start).arg(end)
                                       .arg(capital).arg(maxProfit));
            foreach (int number, uniqueSelected) {
                int count = selected.count(number);
                selectedCommodities.append(QString ("%1 x %2").arg(count).arg(profits.at(number)->nameCommodity()));
                galactica->decrementSupply(start,profits.at(number)->nameCommodity(),count);
            }
        }

        allSelectedCommodities.append(selectedCommodities);
        Q_ASSERT(oldStart!=start);
        oldStart=start;
        start=end;
        capital+=maxProfit;
    }
    return allSelectedCommodities;
}

QStringList Trading::bestTradingFor1(int &capital, quint32 &maxProfit, int start, const int &jumps)
{
    QStringList allSelectedCommodities;
    int selectedEndStation;
    for (int j=0;j<jumps;++j) {
        quint32 max=0;
        QStringList tempSelectedCommodities;
        for (int s=0;s<galactica->allStations().size();++s) {
            int end=galactica->station(s)->id();

            if (start!=end) {

                QList<Profit *> profits=possibleProfits(start,end);
                maxProfit = getProfit(start, end, capital);
                QList<int> selected=getCommodities(start, end, capital);
                //            Q_ASSERT(selected.size()==cargo);

                QList<int> uniqueSelected(selected);
                qSort(uniqueSelected.begin(), uniqueSelected.end(), qLess<int>() );
                QList<int>::iterator iter = std::unique (uniqueSelected.begin(), uniqueSelected.end());
                uniqueSelected.erase(iter, uniqueSelected.end());

                QStringList selectedCommodities;
                if (uniqueSelected.size()>0) {
                    selectedCommodities.append(QString("%1 -> %2|%3+%4")
                                               .arg(start).arg(end)
                                               .arg(capital).arg(maxProfit));
                    foreach (int number, uniqueSelected) {
                        int count = selected.count(number);
                        selectedCommodities.append(QString ("%1 x %2").arg(count).arg(profits.at(number)->nameCommodity()));
                    }
                }
                if (max<maxProfit) {
                    max=maxProfit;
                    tempSelectedCommodities.clear();
                    tempSelectedCommodities.append(selectedCommodities);
                    selectedEndStation=end;
                }
            }
        }


        allSelectedCommodities.append(tempSelectedCommodities);
        start=selectedEndStation;
        capital+=max;
    }
    return allSelectedCommodities;
}

QStringList Trading::computeBestTrades(const int &idStartStation,int &capital, quint32 &maxProfit, const int &jumps)
{
    QStringList allSelectedCommodities;

    int start=idStartStation;

    if (jumps>3)
        allSelectedCommodities = bestTradingFor3More(capital, maxProfit, start, jumps);
    else
        allSelectedCommodities = bestTradingFor1(capital, maxProfit, start, jumps);
//    qDebug()<<allSelectedCommodities;
    return allSelectedCommodities;
}
