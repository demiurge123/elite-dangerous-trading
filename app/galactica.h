#pragma once
#include "station.h"
#include <QAbstractListModel>
#include <QtNetwork/QNetworkAccessManager>
#include <QtNetwork/QNetworkReply>

class Galactica: public QAbstractListModel
{
    Q_OBJECT
public:
    Galactica(QObject *parent = 0);
    virtual ~Galactica();


    int rowCount(const QModelIndex &parent=QModelIndex()) const;
    QVariant data(const QModelIndex &index, int role=Qt::DisplayRole) const;



    int size() const {return _stations.size();}
    Station* station(const int& index) const {return _stations.at(index); }
    QString stationName(int id);
    const QList<Station*> allStations() const {return _stations; }

    void read(const QJsonObject &json);
    void write(QJsonObject &json) const;

    void download(const QString &address);
    void decrementSupply(const int &idStation, const QString &commodity, const int &number);
    void clear();

private:
    QEventLoop loop;
    QList<Station*> _stations;
    QNetworkAccessManager m_manager;
private slots:
    void replyFinished(QNetworkReply *reply);
};
