#pragma once

#include <QPixmap>
#ifdef Q_OS_WIN32
#include <Windows.h>
#include <psapi.h>


DWORD getProcess();

class Screenshot
{
public:
    Screenshot();
    QImage getScreenshot();
    static HWND wHandle;
    bool online();
private:
    void getPixmap();
    HBITMAP getWindow();

    QImage m_image;

};

#endif

