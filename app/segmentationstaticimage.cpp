#include <opencv2/opencv.hpp>
#include <QDebug>
#include <QDir>
#include <QPainter>
#include <QStandardPaths>
#include "segmentationstaticimage.h"

using namespace Recognition;

SegmentationStaticImage::SegmentationStaticImage(QObject *parent)
    : QObject(parent)
{
    dataPath = QStandardPaths::writableLocation(QStandardPaths::GenericDataLocation)+QDir::separator()+"EDT Group";

    segmentationOK=true;
}

void SegmentationStaticImage::threshold()
{
//    static int number=1;
    cv::threshold(m_image, m_image, 90, 255, CV_THRESH_BINARY_INV);
//    QImage dest = QImage((uchar*) m_image.data, m_image.cols, m_image.rows, (int)m_image.step, QImage::Format_RGB888);
//    dest.save(QString("threshold_%1.png").arg(number++));
}

void SegmentationStaticImage::countour()
{
    Q_ASSERT(contours.size()==0);
    cv::findContours(m_image,
                     contours,            // a vector of contours
                     CV_RETR_EXTERNAL,    // retrieve the external contours
                     CV_CHAIN_APPROX_NONE); // all pixels of each contour

    cv::cvtColor(m_image, m_image, CV_GRAY2RGB);
    cv::drawContours(m_image,contours,
                     -1, // draw all contours
                     cv::Scalar(255,0,0), // in blue
                     1); // with a thickness of 1

    //    QImage dest = QImage((uchar*) m_image.data, m_image.cols, m_image.rows, (int)m_image.step, QImage::Format_RGB888);
    //    dest.save("countour.png");

}

inline uint qHash(const QRectF & r)
{
    return qHash(r.top()) ^ qHash(r.left()) ^
            qHash(r.bottom()) ^ qHash(r.right());
}

bool sortRect(const QRect& rec1,const QRect& rec2) {
    if (qAbs(rec1.center().y()-rec2.center().y())<=25) {
        if (rec1.center().x()< rec2.center().x())
            return true;
        else
            return false;
    } else {
        if (rec1.center().y()< rec2.center().y())
            return true;
        else
            return false;
    }
    return false;
}

void SegmentationStaticImage::addRectAndImage(cv::Rect mr)
{
    //    static int numer=1;
    Q_ASSERT(mr.width>0 && mr.height>0);
    cv::Mat auxRoi(m_origin, mr);

    auxRoi=preprocessChar(auxRoi);
    cv::Mat temp;
    cv::cvtColor(auxRoi,temp,CV_GRAY2RGB);
    QImage dest = QImage((uchar*) temp.data, temp.cols, temp.rows, (int)temp.step, QImage::Format_RGB888);
    //    QString filePath=dataPath+QDir::separator()+"allChars"+QDir::separator()+QString("char_%1.png").arg(numer++);
    //    dest.save(filePath);
    QRect r=QRect(mr.x,mr.y,mr.width,mr.height);
    rects.append(r);
    images.insert(r,dest.copy());

}

void SegmentationStaticImage::rectangle()
{
    if (contours.size()==0) {
        segmentationOK=false;
        return;
    }
    std::vector<std::vector<cv::Point> >::iterator itc= contours.begin();

    while (itc!=contours.end()) {
        cv::Rect mr= cv::boundingRect(cv::Mat(*itc));
        cv::rectangle(m_image, mr, cv::Scalar(0,255,0));


        if( verifyPosition(mr) && verifySize(mr)) {
            if (mr.width>=1.37*mr.height) {
                // split 2 images
                cv::Rect mr1=mr;
                mr1.width/=2;
                addRectAndImage(mr1);
                cv::rectangle(m_image, mr1, cv::Scalar(0,125,255));

                cv::Rect mr2=mr;
                mr2.x+=mr2.width/2;
                mr2.width/=2;
                addRectAndImage(mr2);
                cv::rectangle(m_image, mr2, cv::Scalar(0,125,255));
            } else {
                addRectAndImage(mr);
                cv::rectangle(m_image, mr, cv::Scalar(0,125,255));
            }
        }
        ++itc;
    }
    //        QImage dest = QImage((uchar*) m_image.data, m_image.cols, m_image.rows, m_image.step, QImage::Format_RGB888);
    //        dest.save("rectangle.png");

    if (rects.size()==0) {
        segmentationOK=false;
        return;
    }

    qSort(rects.begin(),rects.end(),sortRect);
    result=QImage(QSize(rects.size()*21,20),QImage::Format_RGB32);
    result.fill(QColor("black"));
    QPainter p(&result);
    QRect lastRect=QRect();
    QPen pen1(QColor(255, 0, 0));
    QPen pen2(QColor(0, 255, 0));
    //    pen1.setWidth(1);
    for(int i = 0; i<rects.size(); ++i) {
        QRect r=rects.at(i);
        QImage img=images[r];
        p.drawImage(i*21,0,img);

        if (lastRect == QRect()) {
            lastRect=r;
        } else {
            int lenX=qAbs(lastRect.x()+lastRect.width()-r.x());
            int lenY = qAbs(lastRect.y()-r.y());
            int height=r.height();
            if (r.y()<50) // only goods name, above line y=50
                height/=2;

            if ( lenX >  height && lenX<4*height && lenY < height) { // spacing
                p.setPen(pen1);
                p.drawLine(QPoint((i)*21-1,0),QPoint((i)*21-1,20));
            } else
                if (lenY >  height || lenX> 1.5* height) { //next line
                    p.setPen(pen2);
                    p.drawLine(QPoint((i)*21-1,0),QPoint((i)*21-1,20));
                }

            lastRect=r;
        }
    }
    p.end();
    //        img.save(QString("scalone_%1.png").arg(numer++));
    //    result.save(QString("scalone_%1.png").arg(numer++));
}

const QImage & SegmentationStaticImage::getImage()
{
    if (segmentationOK==false)
        result=QImage();
    return result;
}

void SegmentationStaticImage::setImage(const QImage & image)
{
    segmentationOK = true;

    cv::Mat res(image.height(),image.width(),CV_8UC4,(uchar*)image.bits(),image.bytesPerLine());
    cv::cvtColor(res, res,CV_RGB2GRAY);
    res.copyTo(m_image);

    QImage temp=image.copy();
    temp.invertPixels(QImage::InvertRgba);
    cv::Mat res2(temp.height(),temp.width(),CV_8UC4,(uchar*)temp.bits(),temp.bytesPerLine());
    cv::cvtColor(res2, res2,CV_RGB2GRAY);
    res2.copyTo(m_origin);

    //    qDebug()<<Layouts::getInstance()->current().getName();
    segmentationAreas = Fields::getInstance()->current().getSegmentationAreas();

    contours.clear();
    rects.clear();
}

void SegmentationStaticImage::setImage(const QString &filename)
{
    QImage image(filename);
    setImage(image);
}

bool SegmentationStaticImage::verifyPosition(const cv::Rect &rect) const
{
    Q_ASSERT(segmentationAreas.size()>0);
    QRect rr(rect.tl().x,rect.tl().y,rect.width, rect.height);
    for (int i=0;i<segmentationAreas.count();++i)
        if (segmentationAreas.at(i).contains(rr))
            return true;
    return false;
}

bool SegmentationStaticImage::verifySize(const cv::Rect &rect) const
{
    float minHeight=12;
    float maxHeight=28;
    if(rect.height >= minHeight && rect.height < maxHeight)
        return true;
    else
        return false;
}

cv::Mat SegmentationStaticImage::preprocessChar(const cv::Mat &in)
{
    //Remap image
    int h=in.rows;
    int w=in.cols;
    cv::Mat transformMat=cv::Mat::eye(2,3,CV_32F);
    int m=cv::max(w,h);
    transformMat.at<float>(0,2)=m/2 - w/2;
    transformMat.at<float>(1,2)=m/2 - h/2;

    cv::Mat warpImage(m,m, in.type());
    cv::warpAffine(in, warpImage, transformMat, warpImage.size(), cv::INTER_LINEAR, cv::BORDER_CONSTANT, cv::Scalar(0) );

    cv::Mat out;
    cv::resize(warpImage, out, cv::Size(20, 20) );

    return out;
}
