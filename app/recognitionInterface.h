#pragma once
#include <QObject>
#include <QSharedPointer>
#include <QImage>
#include <QHash>

namespace Recognition {

class RecognitionInterface {
public:
// preparation
    virtual void crop() = 0;
    virtual void distort() = 0;
    virtual void invert() = 0;
    virtual void curve() = 0;
    virtual void clear() = 0;
// segmentation
    virtual void threshold() = 0;
    virtual void countour() = 0;
    virtual void rectangle() = 0;
//recognition
    virtual void extract() = 0;

    virtual const QImage &getImage() = 0;
    virtual QHash<QString,QString> getData() const = 0;
    virtual QString getDataText() = 0;
};

}
Q_DECLARE_INTERFACE(Recognition::RecognitionInterface, "pl.ibohatyrewicz.EliteDangerousTrading.RecognitionInterface")

