#pragma once
#include <QObject>
#include "commandInterface.h"
#include "recognitionstaticimage.h"

namespace Recognition {

class RecognitionImages : public QObject,
        public Command
{
    Q_OBJECT
    Q_INTERFACES(Recognition::Command)
public:
    explicit RecognitionImages(QObject *parent = 0);
    void execute();

    void setFileNameObject(const QString &filename);
    void setImageObject(const QImage & image);
    RecognitionStaticImage* getStaticImage() const {return m_staticImage;}
    double getProcentRecognition() const;
private:
    RecognitionStaticImage *m_staticImage;
signals:
    void qualityRecognition(const double &procent);
};

}
