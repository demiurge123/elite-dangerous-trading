#pragma once
#include "good.h"

#include <QAbstractTableModel>
#include <QEventLoop>
#include <QtNetwork/QNetworkAccessManager>

class Station : public QAbstractTableModel
{
    Q_OBJECT
public:
    Station(QObject* parent = 0);
    virtual ~Station();
    const QList<Good *> goods() const;
    void setGoods(QList<Good *> goods);

    //    QModelIndex index(int row, int column,
    //                              const QModelIndex &parent = QModelIndex()) const;
    //    QModelIndex parent(const QModelIndex &child) const;
    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    int columnCount(const QModelIndex &parent = QModelIndex()) const;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const;

    void read(const QJsonObject &json);
    void write(QJsonObject &json) const;

    QString name() const;
    void download(const QString &address);
    void readGoods(const QJsonObject &json);

    int id() const { return _id;}
    void decrementSupply(const QString &commodity, const int &number);
    void clear();
private:
    QEventLoop loop;
    QString _stationName;
    QString _systemName;
    int _id;
    QList<Good *> _goods;
    QNetworkAccessManager m_manager;
private slots:
    void replyFinished(QNetworkReply *reply);
};


