#pragma once
#include <QJsonObject>
#include <QObject>
#include <QDateTime>

class Good
{
public:

    Good(const QString& name="", const int& sellPrice=0, const int& buyPrice=0, const int &supply=0, const QDateTime &lastUpdate=QDateTime());

    const QString& name() const { return _name; }
    const int& buyPrice() const { return _buyPrice; }
    const int& sellPrice() const { return _sellPrice; }
    const int& supply() const { return _supply; }
    const QDateTime& lastUpdate() const { return _lastUpdate; }


    void read(const QJsonObject &json);
    void write(QJsonObject &json) const;
    void decrementSupply(const int &number);
private:
    QString _name;
    int _sellPrice;
    int _buyPrice;
    int _supply;
    QDateTime _lastUpdate;
};


