#include <QGuiApplication>
#include <QFile>
#include <QJsonDocument>
#include <QDebug>
#include <QDateTime>
#include <QStandardPaths>
#include <QDir>
#include <QPalette>
#include <QColor>
#include <QSettings>
#include <QPrinter>

#include "comboboxdelegate.h"

#include "dialog.h"
#include "ui_dialog.h"

#define SERVERDATA
const double QUALITY_RECOGNITION=85.0;

void Dialog::setWidthColumn()
{
    ui->tableWidget->horizontalHeader()->resizeSection(0, 140); // station
    ui->tableWidget->horizontalHeader()->resizeSection(1, 170); // commodity
    ui->tableWidget->horizontalHeader()->resizeSection(2, 54); // sell price
    ui->tableWidget->horizontalHeader()->resizeSection(3, 54); // buy price
    ui->tableWidget->horizontalHeader()->resizeSection(4, 54); // supply


}

void Dialog::settingInternet()
{
    internetAddress = ui->address->text();
    internetPort = ui->port->value();
}

void Dialog::createData()
{
    dataPath = QStandardPaths::writableLocation(QStandardPaths::GenericDataLocation)+QDir::separator()+"EDT Group";
    QDir dirPath(dataPath+"/data");
    if (!dirPath.exists()) {
        QDir(dataPath).mkpath("data");
    }

    if (!QFile(dataPath+"/data/fields.json").exists()) {
        QFile::copy(":/data/fields.json", dataPath+"/data/fields.json");
    } else if (QFileInfo(dataPath+"/data/fields.json").lastModified().daysTo(QDateTime::fromString("2014-07-09 22:00:00","yyyy-MM-dd hh:mm:ss"))>0) {
        QFile::rename(dataPath+"/data/fields.json", dataPath+QString("/data/fields_%1.json").arg(QDateTime::currentDateTime().toString("yyyy-MM-dd HH-mm-ss")));
        QFile::copy(":/data/fields.json", dataPath+"/data/fields.json");
    }

    if (!QFile(dataPath+"/data/mlp.yml").exists()) {
        QFile::copy(":/data/mlp.yml", dataPath+"/data/mlp.yml");
    } else if (QFileInfo(dataPath+"/data/mlp.yml").lastModified().daysTo(QDateTime::fromString("2014-07-09 22:00:00","yyyy-MM-dd hh:mm:ss"))>0) {
        QFile::rename(dataPath+"/data/mlp.yml", dataPath+QString("/data/mlp_%1.yml").arg(QDateTime::currentDateTime().toString("yyyy-MM-dd HH-mm-ss")));
        QFile::copy(":/data/mlp.yml", dataPath+"/data/mlp.yml");
    }


    QStringList listResolution=Fields::getInstance()->getAllResolution();
    ui->resolution->addItems(listResolution);
}

void Dialog::setupRecognition()
{
    prepare = new Recognition::PreperationImages();
    segmentation = new Recognition::SegmentationImages();
    recognition = new Recognition::RecognitionImages();
    connect(recognition,SIGNAL(qualityRecognition(double)),this,SLOT(qualityRecognition(double)));

    itemStation = new QTableWidgetItem();
    itemGoods = new QTableWidgetItem();
    itemBuy = new QTableWidgetItem();
    itemSell = new QTableWidgetItem();
    itemSupply = new QTableWidgetItem();

    ui->tableWidget->setItem(0,0,itemStation);
    ui->tableWidget->setItem(0,1,itemGoods);
    ui->tableWidget->setItem(0,2,itemSell);
    ui->tableWidget->setItem(0,3,itemBuy);
    ui->tableWidget->setItem(0,4,itemSupply);
}

void Dialog::setupSceneGraphics()
{
    scene = new QGraphicsScene(QRectF(-182,-200,364,400),this);
    ui->graphicsView->setScene(scene);
    m_screenImage = new ScreenImage();
    scene->addItem(m_screenImage);
    ui->graphicsView->viewport()->installEventFilter(this);
}

Dialog::Dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog)
{
    ui->setupUi(this);
    setWindowTitle(QString("Elite Dangerous Trading - %1 by demiurge").arg(qApp->applicationVersion()));
    ui->progressBar->setVisible(false);
    readSettings();

    setWidthColumn();

    connect(ui->capture,SIGNAL(toggled(bool)),this,SLOT(captureImage(bool)));
    connect(ui->address,SIGNAL(textChanged(QString)),this,SLOT(addressChanged(QString)));
    connect(ui->port,SIGNAL(valueChanged(int)),this,SLOT(portChanged(int)));
    connect(ui->selectStation,SIGNAL(currentIndexChanged(int)),this,SLOT(currentIndexComboBox(int)));
    connect(ui->resolution,SIGNAL(currentIndexChanged(int)),this,SLOT(resolutionChanged(int)));
    connect(&manager,SIGNAL(finished(QNetworkReply*)),this,SLOT(respond(QNetworkReply*)));
    connect(ui->qualityScreenshot,SIGNAL(valueChanged(int)),this,SLOT(qualityScreenshotChanged(int)));
    connect(ui->jumps,SIGNAL(sliderMoved(int)),this,SLOT(hiperspaceJumpsChanged(int)));
    connect(ui->tradingButton,SIGNAL(clicked()),this,SLOT(computeTrading()));
    connect(ui->starship,SIGNAL(currentIndexChanged(QString)),this,SLOT(starshipChanged(QString)));
    connect(ui->pdf,SIGNAL(clicked()),this,SLOT(exportToPDF()));

    galactica = new Galactica(this);

    trading = new Trading(galactica);
    currentCargo=0;

    ui->selectStation->setModel(galactica);
    ui->startStation->setModel(galactica);
    ui->endStation->setModel(galactica);

    ComboBoxDelegate* delegate = new ComboBoxDelegate(this);
    ui->selectStation->setItemDelegate(delegate);
    ui->startStation->setItemDelegate(delegate);
    ui->endStation->setItemDelegate(delegate);

    settingInternet();

#ifdef SERVERDATA

    galactica->download(QString("%1:%2/%3").arg(internetAddress).arg(internetPort).arg("stations"));
    downloadAllMarkets();
#endif

    setupSceneGraphics();

#ifdef Q_OS_WIN32
    m_screenshot = NULL;
#endif


    createData();

    setupRecognition();

    starshipChanged("Siderwinder Mk.I");
    ui->pdf->setEnabled(false);
}

Dialog::~Dialog()
{
    delete prepare;
    delete segmentation;
    delete recognition;

    delete itemStation;
    delete itemGoods;
    delete itemBuy;
    delete itemSell;
    delete itemSupply;

    delete m_screenImage;
#ifdef Q_OS_WIN32
    if (m_screenshot!=NULL)
        delete m_screenshot;
#endif
    delete trading;
    delete galactica;
    delete scene;
    delete ui;
}

bool Dialog::loadGalactica(const QString &filename)
{
    QFile loadFile(filename);

    if (!loadFile.open(QIODevice::ReadOnly)) {
        return false;
    }

    QByteArray saveData = loadFile.readAll();

    QJsonDocument loadDoc(QJsonDocument::fromJson(saveData));
    galactica->read(loadDoc.object());
    return true;
}

bool Dialog::saveGalactica(const QString &filename) const
{
    QFile saveFile(filename);

    if (!saveFile.open(QIODevice::WriteOnly)) {
        return false;
    }

    QJsonObject galacticaObject;
    galactica->write(galacticaObject);
    QJsonDocument saveDoc(galacticaObject);
    saveFile.write(saveDoc.toJson());

    return true;
}

QImage Dialog::getImage(Recognition::Command *command, const QImage & image, const QString &nameField)
{
    Fields::getInstance()->setCurrent(nameField);

    command->setImageObject(image);
    command->execute();

    QImage img = command->getStaticImage()->getImage();
    tempImage=img.copy();
    tempData = command->getStaticImage()->getData();
    return tempImage;
}

void Dialog::setBackgroundColor(QString nameColor)
{
    itemStation->setBackgroundColor(nameColor);
    itemGoods->setBackgroundColor(nameColor);
    itemBuy->setBackgroundColor(nameColor);
    itemSell->setBackgroundColor(nameColor);
    itemSupply->setBackgroundColor(nameColor);
}



void Dialog::sendData(const QHash<QString,QString> &data)
{
    // Build your JSON string as usual
    QByteArray jsonString;
    jsonString.append("{\"station\":"+station+"}");

    // For your "Content-Length" header
    QByteArray postDataSize = QByteArray::number(jsonString.size());

    // Time for building your request
    QUrl serverURL(QString("%1:%2").arg(internetAddress).arg(internetPort));
    request.setUrl(serverURL);

    // Add the headers specifying their names and their values with the following method : void QNetworkRequest::setRawHeader(const QByteArray & headerName, const QByteArray & headerValue);
    request.setRawHeader("User-Agent", "My app name v0.1");
    request.setRawHeader("X-Custom-User-Agent", "My app name v0.1");
    request.setRawHeader("Content-Type", "application/json");
    request.setRawHeader("Content-Length", postDataSize);
    request.setRawHeader("station",data.value("station").toLocal8Bit());
    request.setRawHeader("commodity",data.value("commodity").toLocal8Bit());
    request.setRawHeader("sell",data.value("sell").toLocal8Bit());
    request.setRawHeader("buy",data.value("buy").toLocal8Bit());
    request.setRawHeader("supply",data.value("supply").toLocal8Bit());

    // Use QNetworkReply * QNetworkAccessManager::post(const QNetworkRequest & request, const QByteArray & data); to send your request. Qt will rearrange everything correctly.
    manager.post(request, jsonString);
}

void Dialog::readSettings()
{
    QSettings settings("Metropolis", "EDTrading");
    ui->address->setText(settings.value("server/address", "http://localhost").toString());
    ui->port->setValue(settings.value("server/port", 3001).toInt());
    ui->resolution->setCurrentText(settings.value("resolution","1920x1080").toString());
}

void Dialog::writeSettings()
{
    QSettings settings("Metropolis", "EDTrading");
    settings.setValue("server/address", ui->address->text());
    settings.setValue("server/port", ui->port->value());
    settings.setValue("resolution", ui->resolution->currentText());
}

void Dialog::timerEvent(QTimerEvent *event)
{
    if (event->timerId()==timerChangeSettings.timerId()) {
        QPalette palette=QGuiApplication::palette();
        QColor color=palette.color(QPalette::Base);
        ui->address->setStyleSheet(QString("background-color: %1;").arg(color.name()));
        timerChangeSettings.stop();
    }




#ifdef Q_OS_WIN32
    bool changeData =false;
    if (m_screenshot!=NULL && m_screenshot->online()) {
        QImage image = m_screenshot->getScreenshot();
        QSize size = image.size();
        if (m_size!=size) {
            QRegExp regexp("(\\d{1,})x(\\d{1,})");
            int index=0;
            foreach(QString resolution,Fields::getInstance()->getAllResolution()) {
                if (resolution.indexOf(regexp)>-1) {
                    if (regexp.cap(1).toInt()==size.width() && regexp.cap(2).toInt()==size.height()) {
                        m_size=size;
                        resolutionChanged(index);
                        break;
                    }
                index++;
                }
            }
        }

        if (ui->qualityCheckBox->isChecked() && ui->progressBar->value() < ui->qualityScreenshot->value())
        {
            QDateTime now = QDateTime::currentDateTime();
            qDebug()<<qApp->applicationDirPath();
            if (!QDir(qApp->applicationDirPath()+"/screenshots").exists())
                QDir(qApp->applicationDirPath()).mkpath("screenshots");

            image.save(qApp->applicationDirPath()+"/screenshots/"+QString("EliteDangerous %1.png").arg(now.toString("yyyy-MM-dd hh-mm-ss-zzz")));
        }


        QImage imgStation = getImage(prepare, image, QString("Station-%1").arg(_resolution));
        QImage imgGoods = getImage(prepare, image, QString("Goods-%1").arg(_resolution));

        QImage segStation, segGoods;
        if (!imgStation.isNull() && !imgGoods.isNull()) {
            segStation = getImage(segmentation, imgStation, QString("Station-%1").arg(_resolution));
            segGoods = getImage(segmentation, imgGoods, QString("Goods-%1").arg(_resolution));
        }
        QImage recStation,recGoods;
        QHash<QString,QString> dataSend;
        if (!segStation.isNull() && !segGoods.isNull()) {

            recStation = getImage(recognition, segStation, QString("Station-%1").arg(_resolution));
            if (tempData.contains("station")) {
                dataSend.insert("station",tempData.value("station"));
                if (station!=tempData.value("station") && !tempData.value("station").isEmpty()) {
                    station=tempData.value("station");
                    itemStation->setText(station);

                    changeData=true;
                }
            }
            recGoods = getImage(recognition, segGoods, QString("Goods-%1").arg(_resolution));
            if (tempData.contains("commodity")) {
                dataSend.insert("commodity",tempData.value("commodity"));
                if (goods!=tempData.value("commodity") && !tempData.value("commodity").isEmpty()) {
                    goods=tempData.value("commodity");
                    itemGoods->setText(goods);

                    changeData=true;
                }
            }
            if (tempData.contains("sell")) {
                dataSend.insert("sell",tempData.value("sell"));
                if (sell!=tempData.value("sell") && !tempData.value("sell").isEmpty()) {
                    sell=tempData.value("sell");
                    itemSell->setText(sell);

                    changeData=true;
                }
            }
            if (tempData.contains("buy")) {
                dataSend.insert("buy",tempData.value("buy"));
                if (buy!=tempData.value("buy") && !tempData.value("buy").isEmpty() ) {
                    buy=tempData.value("buy");
                    itemBuy->setText(buy);

                    changeData=true;
                }
            } else {
                dataSend.insert("buy","");
                if (!buy.contains("0")||!buy.isEmpty()) {
                    buy="";
                    itemBuy->setText(buy);

                    changeData=true;
                }
            }
            if (tempData.contains("supply")) {
                dataSend.insert("supply",tempData.value("supply"));
                if (supply!=tempData.value("supply") && !tempData.value("supply").isEmpty()) {
                    supply=tempData.value("supply");
                    itemSupply->setText(supply);

                    changeData=true;
                }
            }


        }
        if (changeData && dataSend.size()==5 && ui->progressBar->value()>QUALITY_RECOGNITION) {
            setBackgroundColor("white");
            sendData(dataSend);
        }

        m_screenImage->setImage(image);
        m_screenImage->setSegmentationImages(imgStation, imgGoods);
    }

    ui->graphicsView->viewport()->repaint();
#endif
}

void Dialog::downloadMarket(const int &index)
{
    if (index<0 || index>=galactica->size())
        return;

    Station *currentStation=galactica->station(index);
    int id =currentStation->id();
    currentStation->download(QString("%1:%2/%3/%4").arg(internetAddress).arg(internetPort).arg("market").arg(id));

    ui->tableView->setModel(currentStation);
}

void Dialog::downloadAllMarkets()
{
    for (int i=0; i<galactica->size(); ++i) {
        Station *station=galactica->station(i);
        int id = station->id();
        station->download(QString("%1:%2/%3/%4").arg(internetAddress).arg(internetPort).arg("market").arg(id));
    }
    trading->initialProfits();
    saveGalactica("galactica3.json");
}

void Dialog::currentIndexComboBox(const int &index)
{
    downloadMarket(index);
    ui->tableView->horizontalHeader()->resizeSection(0, 188); // commodity name
    ui->tableView->horizontalHeader()->resizeSection(1, 50); // sell price
    ui->tableView->horizontalHeader()->resizeSection(2, 50); // buy price
    ui->tableView->horizontalHeader()->resizeSection(3, 50); // supply
    ui->tableView->horizontalHeader()->resizeSection(4, 129); // last update
}

void Dialog::captureImage(const bool &isCapture)
{
#ifdef Q_OS_WIN32
    if (isCapture) {
        timer.start(500,this);
        m_screenshot = new Screenshot();
        ui->progressBar->setVisible(true);
    } else {
        if (m_screenshot!=NULL) {
            delete m_screenshot;
            m_screenshot = NULL;
            timer.stop();
            ui->progressBar->setVisible(false);
        }
    }
#endif
}

void Dialog::addressChanged(const QString &address)
{
    QUrl url(address);
    if (url.isValid() && url.scheme().compare("http")==0 && url.port()==-1) {
        ui->address->setStyleSheet("background-color: GreenYellow;");
        timerChangeSettings.start(1000,this);
        internetAddress=address;
        writeSettings();
    } else
        ui->address->setStyleSheet("background-color: DeepPink;");

}

void Dialog::portChanged(const int &port)
{
    internetPort = port;
    writeSettings();
}

void Dialog::resolutionChanged(int index)
{
    _resolution=ui->resolution->itemText(index);
    writeSettings();
}

void Dialog::respond(QNetworkReply *reply)
{
    QByteArray all=reply->readAll();
    QJsonDocument loadDoc(QJsonDocument::fromJson(all));
    QJsonObject json=loadDoc.object();
    QString successfully = json["successfully"].toString();
    if (successfully.contains("true")) {
        downloadMarket(ui->selectStation->currentIndex());
        setBackgroundColor("Orange");
    } else {
        setBackgroundColor("Moccasin");
    }
}

void Dialog::qualityRecognition(const double &procent) {
    QString danger = "QProgressBar::chunk {background: QLinearGradient( x1: 0, y1: 0, x2: 1, y2: 0,stop: 0 DeepPink,stop: 1 DeepPink );border-bottom-right-radius: 5px;border-bottom-left-radius: 5px;border: .px solid black;}";
    QString safe= "QProgressBar::chunk {background: QLinearGradient( x1: 0, y1: 0, x2: 1, y2: 0,stop: 0 GreenYellow,stop: 1 GreenYellow );border-bottom-right-radius: 5px;border-bottom-left-radius: 5px;border: .px solid black;}";

    if(procent<QUALITY_RECOGNITION)
        ui->progressBar->setStyleSheet(danger);
    else
        ui->progressBar->setStyleSheet(safe);
    ui->progressBar->setValue((int)procent);

}

void Dialog::qualityScreenshotChanged(const int &value)
{
    ui->qualityCheckBox->setText(tr("make screenshot when quality recognition  is below %1%").arg(value));
}

void Dialog::hiperspaceJumpsChanged(const int &value)
{
    ui->jumpsLabel->setText(tr("Max hiperspace jumps: %1").arg(value));
}

void Dialog::clearTrading()
{
    QMutableListIterator<QTreeWidgetItem *> it(items);
    while (it.hasNext()) {
        it.next();
        it.remove();
    }
    items.clear();
    ui->bestTrading->clear();
}

void Dialog::computeTrading()
{
    int index = ui->startStation->currentIndex();
    if (index<0 || index>=galactica->size())
        return;
    Station *startStation=galactica->station(index);
    int idStartStation =startStation->id();
    QTreeWidgetItem* topItem;
    
    clearTrading();

    int capital = ui->capital->value();
    int jumps = ui->jumps->value();
    quint32 maxProfit = 0;


    

    QStringList listBestTrading = trading->computeBestTrades(idStartStation,capital,maxProfit, jumps);
    int ind=0;
    for (int i=0;i<listBestTrading.size();++i)
    {

        QStringList data=listBestTrading.at(i).split('|');
        if (data.size()>1) {
            QRegExp regexp("(\\d{1,}) -> (\\d{1,})");
            data.at(0).indexOf(regexp);
            int start=regexp.cap(1).toInt();
            int end=regexp.cap(2).toInt();
            QString shortText = QString("%1 -> %2 - %3").arg(galactica->stationName(start).split("|").at(1)).arg(galactica->stationName(end).split("|").at(1)).arg(data.at(1));
            QString longText = QString("%1 -> %2 - %3").arg(galactica->stationName(start)).arg(galactica->stationName(end)).arg(data.at(1));
            topItem = new QTreeWidgetItem((QTreeWidget*)0);
            items.append(topItem);
            ui->bestTrading->insertTopLevelItem(ind++,topItem);
            topItem->setText(0,shortText);
            topItem->setToolTip(0, longText);
        } else {
            QTreeWidgetItem *item = new QTreeWidgetItem(topItem,QStringList(data.at(0)));
            items.append(item);

        }
    }
    ui->pdf->setEnabled(true);
}

void Dialog::starshipChanged(const QString &name)
{
    int cargo=0;
    if (name.compare("Siderwinder Mk.I")==0)
        cargo=4;
    if (name.compare("Eagle Mk.II")==0)
        cargo=4;
    if (name.compare("Zorgon Peterson Hauler")==0)
        cargo=16;
    if (name.compare("Cobra Mk.III")==0)
        cargo=36;
    if (name.compare("Anaconda")==0)
        cargo=228;
    if (name.compare("Lakon Type 9")==0)
        cargo=440;
    if (cargo!=currentCargo) {
        currentCargo = cargo;
        trading->setCargo(currentCargo);
        clearTrading();
    }
}

void Dialog::exportToPDF()
{
    if (ui->bestTrading->topLevelItemCount()>0) {
        QDir path(qApp->applicationDirPath());
        path.mkpath("export");
        QString fileName=QString("%1/export/trading_%2.pdf").arg(qApp->applicationDirPath()).arg(QDateTime::currentDateTime().toString("yyyy-MM-dd hh-mm-ss"));
        QPrinter printer;
        printer.setOutputFormat(QPrinter::PdfFormat);
        printer.setOutputFileName(fileName);
        printer.setPageSize(QPageSize(QPageSize::A5));


        int width = printer.width();
        QPainter painter;

        if (!painter.begin(&printer))
            return;


        int x = 20;
        int y = 0;
        int row_height = 20;
        int profit=0;

        QRect r=QRect(x,y,width/4,row_height);
        painter.drawText(r,Qt::AlignLeft, tr("Starship:%1").arg(ui->starship->currentText()));
        y+=row_height;

        QFont font=painter.font();
        for (int i=0; i<ui->bestTrading->topLevelItemCount(); i++)
        {
            int yy=y;
            QTreeWidgetItem * item = ui->bestTrading->topLevelItem(i);
            QString text = item->toolTip(0);
            QRegExp regexp("([\\w\\s]+)\\|([\\w\\s]+) -> ([\\w\\s]+)\\|([\\w\\s]+) - (\\d{1,})\\+(\\d{1,})");
            if (text.indexOf(regexp)>-1) {
                profit+=regexp.cap(6).toInt();
                QRect r=QRect(x,y,width/4,row_height);
//                font.setItalic(true);
//                painter.setFont(font);
//                painter.drawText(r,Qt::AlignLeft, QString("Hiperspace jump:%1").arg(i+1));
//                font.setItalic(false);
//                painter.setFont(font);
//                y+=row_height;

                r=QRect(x+3,y,width/2,row_height);
                font.setBold(true);
                painter.setFont(font);
                painter.drawText(r,Qt::AlignLeft, tr("Start system:"));
                font.setBold(false);
                painter.setFont(font);
                r=QRect(x+13,y,width/2,row_height);
                painter.drawText(r,Qt::AlignRight, regexp.cap(1));
                r=QRect(x+13+width/2,y,10,row_height);
                painter.drawText(r,Qt::AlignLeft, "|");
                r=QRect(x+16+width/2,y,width/2,row_height);
                painter.drawText(r,Qt::AlignLeft, regexp.cap(2));
                y+=row_height;

                r=QRect(x+3,y,width/2,row_height);
                font.setBold(true);
                painter.setFont(font);
                painter.drawText(r,Qt::AlignLeft, tr("End system:"));
                font.setBold(false);
                painter.setFont(font);
                r=QRect(x+13,y,width/2,row_height);
                painter.drawText(r,Qt::AlignRight, regexp.cap(3));
                r=QRect(x+13+width/2,y,10,row_height);
                painter.drawText(r,Qt::AlignLeft, "|");
                r=QRect(x+16+width/2,y,width/2,row_height);
                painter.drawText(r,Qt::AlignLeft, regexp.cap(4));

                for (int j=0;j<item->childCount();j++) {
                    y+=row_height;
                    QTreeWidgetItem * child = item->child(j);
                    QString commodities = child->text(0);
                    r=QRect(x+3,y,width/2,row_height);
                    painter.drawText(r,Qt::AlignLeft, commodities);
                }
                y+=row_height;

                r=QRect(x+3,y,width/2,row_height);
                font.setItalic(true);
                painter.setFont(font);
                painter.drawText(r,Qt::AlignLeft, tr("Capital: %1, profit: +%2").arg(regexp.cap(5)).arg(regexp.cap(6)));
                font.setItalic(false);
                painter.setFont(font);
                y+=row_height;

                painter.drawRect(QRect(QPoint(0,yy),QPoint(x,y)));
                painter.drawText(QRect(QPoint(0,yy),QPoint(x,y)),Qt::AlignCenter,QString::number(i+1));
                painter.drawRect(QRect(QPoint(x,yy),QPoint(printer.width()-x,y)));
            }
            if (y>printer.height()-6*row_height) {
                printer.newPage();
                y=0;
            }

        }
        font.setBold(true);
        painter.setFont(font);
        r=QRect(x,y,width/2,row_height);
        painter.drawText(r,Qt::AlignLeft, tr("Total profit: +%1").arg(profit));

        painter.end();
    }
    ui->pdf->setEnabled(false);
}



