#include <QJsonArray>
#include <QJsonDocument>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QSize>
#include "station.h"

Station::Station(QObject *parent)
    : QAbstractTableModel(parent)
{
    connect(&m_manager, SIGNAL(finished(QNetworkReply*)),
            this, SLOT(replyFinished(QNetworkReply*)));
}

Station::~Station()
{

}

const QList<Good *> Station::goods() const
{
    return _goods;
}

void Station::setGoods(QList<Good *> goods)
{
    _goods.swap(goods);
}

int Station::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    return _goods.size();
}

int Station::columnCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    return 5;
}

QVariant Station::data(const QModelIndex &index, int role) const
{
    if (!index.isValid() || index.row()<0 || index.row()>=_goods.size() || index.column()<0 || index.column() >5)
        return QVariant();
    if (role == Qt::DisplayRole)
        switch(index.column()) {
        case 0:
            return _goods.at(index.row())->name();
            break;
        case 1:
            return _goods.at(index.row())->sellPrice();
            break;
        case 2:
            return _goods.at(index.row())->buyPrice();
            break;
        case 3:
            return _goods.at(index.row())->supply();
            break;
        case 4:
            return _goods.at(index.row())->lastUpdate();
            break;
        default:
            return QVariant();
        }

    return QVariant();
}

QVariant Station::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role == Qt::DisplayRole && orientation == Qt::Horizontal)
        switch(section) {
        case 0:
            return tr("Name");
        case 1:
            return tr("Sell");
        case 2:
            return tr("Buy");
        case 3:
            return tr("Supply");
        case 4:
            return tr("Update");
        default:
            return QVariant();
        }
    return QVariant();
}

void Station::readGoods(const QJsonObject &json)
{
    _goods.clear();
    beginResetModel();
    QJsonArray goodsArray = json["Commodities"].toArray();
    for (int goodIndex = 0; goodIndex < goodsArray.size(); ++goodIndex) {
        QJsonObject goodObject = goodsArray[goodIndex].toObject();
        Good *good=new Good();
        good->read(goodObject);
        _goods.append(good);
    }
    endResetModel();
}

void Station::decrementSupply(const QString &commodity, const int &number)
{
    foreach (Good* good, _goods) {
        if (good->name().compare(commodity,Qt::CaseInsensitive)==0) {
            good->decrementSupply(number);
            break;
        }
    }
}

void Station::clear()
{
    foreach(Good *good,_goods) {
        delete good;
    }
    _goods.clear();
}

void Station::read(const QJsonObject &json)
{
    beginResetModel();

    _stationName = json["station"].toString();
    _systemName =json["system"].toString();
    _id =json["id"].toString().toInt();

    readGoods(json);
    endResetModel();
}

void Station::write(QJsonObject &json) const
{

    json["station"] = _stationName;
    json["system"] = _systemName;
    json["id"] = QString::number(_id);
    QJsonArray goodsArray;
    foreach (const Good *commodity, _goods) {
        QJsonObject goodObject;
        commodity->write(goodObject);
        goodsArray.append(goodObject);
    }
    json["Commodities"] = goodsArray;
}

QString Station::name() const
{
    return QString("%1 | %2").arg(_systemName).arg(_stationName);
}

void Station::download(const QString &address)
{
    QUrl url(address);

    QNetworkRequest request(url);

    m_manager.get(request);
    loop.exec();
}

void Station::replyFinished(QNetworkReply *reply)
{
    QByteArray all=reply->readAll();
    QJsonDocument loadDoc(QJsonDocument::fromJson(all));
    readGoods(loadDoc.object());
    loop.exit();
}
