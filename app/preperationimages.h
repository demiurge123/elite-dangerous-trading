#pragma once
#include <QObject>
#include "commandInterface.h"
#include "preparationstaticimage.h"
#include "fields.h"

namespace Recognition {


class PreperationImages : public QObject,
        public Command
{
    Q_OBJECT
    Q_INTERFACES(Recognition::Command)
public:
    explicit PreperationImages(const QString &nameArea = QString(), QObject *parent = 0);
    void execute();

    void setFileNameObject(const QString &filename);
    void setImageObject(const QImage & image);

    PreparationStaticImage* getStaticImage() const {return m_staticImage;}

private:
    PreparationStaticImage *m_staticImage;

};

}

