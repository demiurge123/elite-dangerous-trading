#include <QFile>
#include <QJsonDocument>
#include <QJsonArray>
#include <QSharedPointer>
#include <QStandardPaths>
#include <QDir>
#include "fields.h"

Fields *Fields::m_Instance;

Field::Field(const QString &nameField)
    : name(nameField)
{
    cropArea=QRect();
    sizeDest=QSize();
    anchorsSource=QPolygon();
    anchorsDest=QPolygon();
    segmentationAreas=QList<QRect>();
}

void Field::read(const QJsonObject &json)
{
    name = json["field"].toString();

    QJsonObject cropObject = json["cropArea"].toObject();
    cropArea = QRect(
                cropObject["x"].toInt(),
            cropObject["y"].toInt(),
            cropObject["width"].toInt(),
            cropObject["height"].toInt());

    QJsonObject sizeObject = json["sizeDest"].toObject();
    sizeDest = QSize(
                sizeObject["width"].toInt(),
            sizeObject["height"].toInt());

    anchorsSource.clear();
    QJsonArray polygonSourceArray = json["anchorsSource"].toArray();
    for (int polygonIndex = 0; polygonIndex < polygonSourceArray.size(); ++polygonIndex) {
        QJsonObject pointObject = polygonSourceArray[polygonIndex].toObject();
        QPoint p(pointObject["x"].toInt(),pointObject["y"].toInt());
        anchorsSource.append(p);
    }

    anchorsDest.clear();
    QJsonArray polygonDestArray = json["anchorsDest"].toArray();
    for (int polygonIndex = 0; polygonIndex < polygonDestArray.size(); ++polygonIndex) {
        QJsonObject pointObject = polygonDestArray[polygonIndex].toObject();
        QPoint p(pointObject["x"].toInt(),pointObject["y"].toInt());
        anchorsDest.append(p);
    }

    segmentationAreas.clear();
    QJsonArray segmentationArray = json["segmentationAreas"].toArray();
    for (int segmentationIndex = 0; segmentationIndex < segmentationArray.size(); ++segmentationIndex) {
        QJsonObject segmentationObject = segmentationArray[segmentationIndex].toObject();
        QRect r(segmentationObject["x"].toInt(),
                segmentationObject["y"].toInt(),
                segmentationObject["width"].toInt(),
                segmentationObject["height"].toInt());
        segmentationAreas.append(r);
    }
}

void Field::write(QJsonObject &json) const
{
    json["field"] = name;

    QJsonObject cropObject;
    cropObject["x"]=cropArea.x();
    cropObject["y"]=cropArea.y();
    cropObject["width"]=cropArea.width();
    cropObject["height"]=cropArea.height();
    json["cropArea"] = cropObject;

    QJsonObject sizeObject;
    sizeObject["width"] = sizeDest.width();
    sizeObject["height"] = sizeDest.height();
    json["sizeDest"] = sizeObject;

    QJsonArray polygonSourceArray;
    for (int i=0;i<anchorsSource.size();i++) {
        QJsonObject pointObject;
        pointObject["x"] = anchorsSource.at(i).x();
        pointObject["y"] = anchorsSource.at(i).y();
        polygonSourceArray.append(pointObject);
    }
    json["anchorsSource"] = polygonSourceArray;

    QJsonArray polygonDestArray;
    for (int i=0;i<anchorsDest.size();i++) {
        QJsonObject pointObject;
        pointObject["x"] = anchorsDest.at(i).x();
        pointObject["y"] = anchorsDest.at(i).y();
        polygonDestArray.append(pointObject);
    }
    json["anchorsDest"] = polygonDestArray;

    QJsonArray segmentationArray;
    for (int i=0;i<segmentationAreas.size();i++) {
        QJsonObject segmentationObject;
        segmentationObject["x"] = segmentationAreas.at(i).x();
        segmentationObject["y"] = segmentationAreas.at(i).y();
        segmentationObject["width"] = segmentationAreas.at(i).width();
        segmentationObject["height"] = segmentationAreas.at(i).height();
        segmentationArray.append(segmentationObject);
    }
    json["segmentationAreas"] = segmentationArray;
}

void Field::addSegmentationArea(const QRect &rect)
{
    if (!segmentationAreas.contains(rect))
        segmentationAreas.append(rect);
}

void Field::delSegmentationArea(const QRect &rect)
{
    if (segmentationAreas.contains(rect))
        segmentationAreas.removeAll(rect);
}

Field Fields::getField(const QString &name) const {
    Field f=fields.value(name,Field(name));
    return f;
}

void Fields::addField(const Field &field)
{
    fields.insert(field.getName(),field);
}

void Fields::delField(const Field &field)
{
    fields.remove(field.getName());
}

QStringList Fields::getAllFieldName() const
{
    return fields.keys();
}

void Fields::createField(const QString &name)
{
    Field f=getField(name);
    addField(f);
    saveFields(dataPath+"/data/fields.json");
}

void Fields::deleteField(const QString &name)
{
    Field f=getField(name);
    delField(f);
    saveFields(dataPath+"/data/fields.json");
}

void Fields::modifyField(const QString &name, const QString &parametr, const QString &value)
{
    Field f=getField(name);
    delField(f);
    if (parametr.compare("name")==0)
        f.setName(value);
    if (parametr.compare("crop")==0) {
        QRegExp regexp("(\\d{1,})\\.(\\d{1,})-(\\d{1,})x(\\d{1,})");
        if (value.indexOf(regexp)>-1) {
            f.setCropArea(QRect(regexp.cap(1).toInt(),regexp.cap(2).toInt(),regexp.cap(3).toInt(),regexp.cap(4).toInt()));
        }
    }
    if (parametr.compare("size")==0) {
        QRegExp regexp("(\\d{1,})x(\\d{1,})");
        if (value.indexOf(regexp)>-1) {
            f.setSizeDest(QSize(regexp.cap(1).toInt(),regexp.cap(2).toInt()));
        }
    }
    if (parametr.compare("source")==0) {
        QRegExp regexp("(\\d{1,})\\.(\\d{1,})-(\\d{1,})\\.(\\d{1,})-(\\d{1,})\\.(\\d{1,})-(\\d{1,})\\.(\\d{1,})");
        if (value.indexOf(regexp)>-1) {
            f.setAnchorsSource(QPolygon()<<QPoint(regexp.cap(1).toInt(),regexp.cap(2).toInt())
                               <<QPoint(regexp.cap(3).toInt(),regexp.cap(4).toInt())
                               <<QPoint(regexp.cap(5).toInt(),regexp.cap(6).toInt())
                               <<QPoint(regexp.cap(7).toInt(),regexp.cap(8).toInt()));
        }
    }
    if (parametr.compare("destination")==0) {
        QRegExp regexp("(\\d{1,})\\.(\\d{1,})-(\\d{1,})\\.(\\d{1,})-(\\d{1,})\\.(\\d{1,})-(\\d{1,})\\.(\\d{1,})");
        if (value.indexOf(regexp)>-1) {
            f.setAnchorsDest(QPolygon()<<QPoint(regexp.cap(1).toInt(),regexp.cap(2).toInt())
                               <<QPoint(regexp.cap(3).toInt(),regexp.cap(4).toInt())
                               <<QPoint(regexp.cap(5).toInt(),regexp.cap(6).toInt())
                               <<QPoint(regexp.cap(7).toInt(),regexp.cap(8).toInt()));
        }
    }
    if (parametr.compare("segmentation")==0) {
        QRegExp regexp("([+-])(\\d{1,})\\.(\\d{1,})-(\\d{1,})x(\\d{1,})");
        if (value.indexOf(regexp)>-1) {
            QRect rect(regexp.cap(2).toInt(),regexp.cap(3).toInt(),regexp.cap(4).toInt(),regexp.cap(5).toInt());
            if (regexp.cap(1).compare("+")==0) {
                f.addSegmentationArea(rect);
            }
            else if (regexp.cap(1).compare("-")==0) {
                f.delSegmentationArea(rect);
            }
        }
    }

    addField(f);
    saveFields(dataPath+"/data/fields.json");
}

QStringList Fields::getAllResolution()
{
    QStringList list;
    QStringList listFields=getAllFieldName();
    for(int i=0;i<listFields.size();i++) {
        QRegExp regexp("\\w+-(\\d{1,}x\\d{1,})");
        QString fieldName=listFields.at(i);
        if (fieldName.indexOf(regexp)>-1 && !list.contains(regexp.cap(1)))
            list.append(regexp.cap(1));
    }
    return list;
}

Fields::Fields()
{
    dataPath = QStandardPaths::writableLocation(QStandardPaths::GenericDataLocation)+QDir::separator()+"EDT Group";
    QDir dirPath(dataPath);
    if (!dirPath.exists()) {
        dirPath.mkpath("data");
        saveFields(dataPath+"/data/fields.json");
    } else {
        loadFields(dataPath+"/data/fields.json");
    }
}

void Fields::read(const QJsonObject &json)
{
    fields.clear();
    QJsonArray fieldsArray = json["fields"].toArray();
    for (int fieldIndex = 0; fieldIndex < fieldsArray.size(); ++fieldIndex) {
        QJsonObject fieldObject = fieldsArray[fieldIndex].toObject();
        Field field;
        field.read(fieldObject);
        addField(field);
    }
}

void Fields::write(QJsonObject &json) const
{
    QJsonArray fieldsArray;
    foreach (Field field, fields) {
        QJsonObject fieldObject;
        field.write(fieldObject);
        fieldsArray.append(fieldObject);
    }
    json["fields"] = fieldsArray;
}

Field Fields::current() const
{
    return getField(nameField);
}

void Fields::setCurrent(const QString &name)
{
    nameField = name;
}

bool Fields::loadFields(const QString &filename)
{
    QFile loadFile(filename);

    if (!loadFile.open(QIODevice::ReadOnly)) {
        qWarning("Couldn't open Fields.json file.");
        return false;
    }

    QByteArray saveData = loadFile.readAll();

    QJsonDocument loadDoc(QJsonDocument::fromJson(saveData));
    read(loadDoc.object());
    return true;
}

bool Fields::saveFields(const QString &filename) const
{
    QFile saveFile(filename);

    if (!saveFile.open(QIODevice::WriteOnly)) {
        qWarning("Couldn't open Fields.json file.");
        return false;
    }

    QJsonObject fieldsObject;
    write(fieldsObject);
    QJsonDocument saveDoc(fieldsObject);
    saveFile.write(saveDoc.toJson());

    return true;
}
