#include "good.h"

Good::Good(const QString &name, const int &sellPrice, const int &buyPrice, const int &supply, const QDateTime &lastUpdate)
    : _name(name),  _sellPrice(sellPrice), _buyPrice(buyPrice), _supply(supply), _lastUpdate(lastUpdate)
{

}

void Good::read(const QJsonObject &json)
{
    _name = json["Name"].toString();
    _sellPrice = json["SellPrice"].toString().toInt();
    _buyPrice = json["BuyPrice"].toString().toInt();
    _supply = json["Supply"].toString().toInt();
    QString lastUpdate =json["lastUpdate"].toString();
    QDateTime dateTime=QDateTime::fromString(lastUpdate,"yyyy-MM-dd hh:mm:ss");
    QDateTime UTC(dateTime);
    UTC.setTimeSpec(Qt::UTC);
    QDateTime local(UTC.toLocalTime());
    _lastUpdate=local;
}

void Good::write(QJsonObject &json) const
{
    json["Name"] = _name;
    json["SellPrice"] = QString::number(_sellPrice);
    json["BuyPrice"] = QString::number(_buyPrice);
    json["Supply"] = QString::number(_supply);
    json["lastUpdate"] = _lastUpdate.toString("yyyy-MM-dd hh:mm:ss");
}

void Good::decrementSupply(const int &number)
{
    if (_supply>=number)
        _supply-=number;
}
