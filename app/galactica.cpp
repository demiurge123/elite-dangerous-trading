#include <QJsonArray>
#include <QDebug>
#include <QNetworkRequest>
#include <QJsonDocument>
#include <QMutableListIterator>
#include "galactica.h"

Galactica::Galactica(QObject *parent)
    : QAbstractListModel(parent)
{
    connect(&m_manager, SIGNAL(finished(QNetworkReply*)),
            this, SLOT(replyFinished(QNetworkReply*)));
}

Galactica::~Galactica()
{

}

int Galactica::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    return _stations.size();
}

QVariant Galactica::data(const QModelIndex &index, int role) const
{
    if (!index.isValid() || index.row()>=_stations.size() || index.row()<0)
        return QVariant();
    if (role == Qt::DisplayRole)
        return _stations[index.row()]->name();
    return QVariant();
}

QString Galactica::stationName(int id)
{
    for (int i=0;i<_stations.size();++i)
        if (_stations.at(i)->id()==id)
            return _stations.at(i)->name();
    return QString();
}



void Galactica::read(const QJsonObject &json)
{
    beginResetModel();

    _stations.clear();
    QJsonArray stationsArray = json["Stations"].toArray();
    for (int stationIndex = 0; stationIndex < stationsArray.size(); ++stationIndex) {
        QJsonObject stationObject = stationsArray[stationIndex].toObject();
        Station *station = new Station(this);
        station->read(stationObject);
        _stations.append(station);
    }

    endResetModel();

}

void Galactica::write(QJsonObject &json) const
{
    QJsonArray stationsArray;
    foreach (Station* station, _stations) {
        QJsonObject stationObject;
        station->write(stationObject);
        stationsArray.append(stationObject);
    }
    json["Stations"] = stationsArray;
}

void Galactica::download(const QString &address)
{
    QUrl url(address);

    QNetworkRequest request(url);

    m_manager.get(request);
    loop.exec();
}

void Galactica::decrementSupply(const int &idStation, const QString &commodity, const int &number)
{
    foreach (Station* station, _stations) {
        if (station->id()==idStation) {
            station->decrementSupply(commodity, number);
            break;
        }
    }
}

void Galactica::clear()
{
    foreach (Station *station, _stations) {
        station->clear();
        delete station;
    }
    _stations.clear();
}

void Galactica::replyFinished(QNetworkReply *reply)
{
    QByteArray all=reply->readAll();
    QJsonDocument loadDoc(QJsonDocument::fromJson(all));
    read(loadDoc.object());
    loop.exit();
}
