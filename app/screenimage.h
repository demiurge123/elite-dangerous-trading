#pragma once
#include <QtWidgets/QGraphicsObject>

class ScreenImage : public QGraphicsObject
{
    Q_OBJECT
private:
    QImage m_image,st_image, gd_image;

public:
    ScreenImage();
    ~ScreenImage();
    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
               QWidget *widget);
    void setImage(const QImage & image);
    void setSegmentationImages(const QImage & stationImage, const QImage & goodsImage);

    void load();
    void save() const;
};
