#include <QDir>
#include <QDebug>
#include <QPainter>
#include <QRgb>
#include <QDesktopServices>
#include <QDateTime>
#include "recognitionstaticimage.h"


using namespace Recognition;



double RecognitionStaticImage::getProcentRecognition() const
{
    return m_trainer->getQuality();
}



RecognitionStaticImage::RecognitionStaticImage(QObject *parent)
    : QObject(parent)
{
    dataPath =  QStandardPaths::writableLocation(QStandardPaths::GenericDataLocation)+QDir::separator()+"EDT Group";
    m_trainer=new Trainer(true);
}

void RecognitionStaticImage::extract()
{
    QStringList list;
    m_data.clear();

    int countLetterRecognized = m_image.width()/21;
    QString text;
    for (int i=0;i<countLetterRecognized;i++) {
        QImage temp = m_image.copy(i*21,0,20,20);
        cv::Mat img(temp.height(),temp.width(),CV_8UC4,(uchar*)temp.bits(),temp.bytesPerLine());
        cvtColor(img, img,CV_RGB2GRAY);

        cv::Mat f=m_trainer->features(img,20);
        char znak = m_trainer->classify(f);

        text.append(znak);
        QRgb rgb=m_image.pixel((i+1)*21-1,10);
        if (rgb==QColor(255, 0, 0).rgb())
            text.append(" ");
        else if (rgb==QColor(0,255,0).rgb()) {
            list.append(text);
            text.clear();
        }
    }
    if (!text.isEmpty()) {
        list.append(text);
        text.clear();
    }

    switch (list.size()) {
    case 1:
        m_data.insert("station",list.at(0));
        break;
    case 3:
        m_data.insert("commodity",list.at(0));
        m_data.insert("sell",list.at(1));
        m_data.insert("supply",list.at(2));
        break;
    case 4:
        m_data.insert("commodity",list.at(0));
        m_data.insert("sell",list.at(1));
        m_data.insert("buy",list.at(2));
        m_data.insert("supply",list.at(3));
        break;
    }
    emit qualityRecognition(m_trainer->getQuality());
}

const QImage & RecognitionStaticImage::getImage()
{
    Q_ASSERT(m_image.width()>0 && m_image.height()>0);
    QImage result(QSize(m_image.width(),40),QImage::Format_RGB32);
    result.fill(Qt::white);
    QPainter p(&result);
    p.setRenderHint(QPainter::Antialiasing);
    p.drawImage(0,0,m_image);
    QFont f=p.font();
    f.setLetterSpacing(QFont::PercentageSpacing,125);
    p.setFont(f);
    p.setPen(QColor("navy"));

    p.drawText(QRectF(QPoint(0,20),QSizeF(m_image.width(),20)), Qt::AlignCenter, getDataText());

    p.end();
    tempImage = result.copy();
    return tempImage;
}

QString RecognitionStaticImage::getDataText()
{
    QString text;
    if (m_data.contains("station"))
        text=m_data.value("station");
    else {
        if (m_data.contains("commodity"))
            text=m_data.value("commodity");
        if (m_data.contains("sell")) {
            text.append(" ");
            text.append(m_data.value("sell"));
        }
        if (m_data.contains("buy")) {
            text.append(" ");
            text.append(m_data.value("buy"));
        }
        else
            text.append(" -");
        if (m_data.contains("supply")) {
            text.append(" ");
            text.append(m_data.value("supply"));
        }
    }
    return text;
}

void RecognitionStaticImage::setFilenameImage(const QString &filenameImage)
{
    m_image = QImage(filenameImage);
    int lastSeparator = filenameImage.length()-filenameImage.lastIndexOf(QDir::separator());
    m_filename = filenameImage.right(lastSeparator-1);

    m_data.clear();
}

void RecognitionStaticImage::setImage(const QImage & image)
{
    m_image=image.copy();
    m_data.clear();
}


