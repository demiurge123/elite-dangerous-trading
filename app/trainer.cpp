#include <QDir>
#include <QImage>
#include <QStandardPaths>
#include <QStringList>
#include "trainer.h"



Trainer::Trainer(bool trained)
    : trained(trained)
{
    dataPath = QStandardPaths::writableLocation(QStandardPaths::GenericDataLocation)+QDir::separator()+"EDT Group";
    QFile trainFile(dataPath+"/data/mlp.yml");
    if (trainFile.exists() && trained) {
        loadTrain();
        loadChars();
    }

}

void Trainer::loadTrain()
{
    QString path = dataPath+"/data/mlp.yml";
    ann.load(path.toStdString().c_str());
}

void Trainer::loadChars()
{
    QString path=dataPath+"/recognition/allChars";
    chars = QVector<QString>()<<"0"<< "1"<< "2"<< "3"<< "4"<< "5"<< "6"<< "7"<< "8"<< "9"<<
                                    "A"<< "B"<< "C"<< "D"<< "E"<< "F"<< "G"<< "H"<< "I"<< "K"<<
                                    "L"<< "M"<< "N"<< "O"<< "P"<< "Q"<< "R"<< "S"<< "T"<< "U"<<
                                    "V"<< "W"<< "X"<< "Y"<< "Z";
}

void Trainer::trainLetters(const QString &path)
{
    cv::Mat classes;
    cv::Mat trainingDataf20;
    std::vector<int> trainingLabels;

    QStringList filters;
    filters << "*.png";
    QDir allDir(path);
    QStringList allList = allDir.entryList(QDir::Dirs|QDir::NoDotAndDotDot);
    int numCharacters = allList.count();
    for (int i=0;i<numCharacters;++i) {
        QDir letterDir(path+QDir::separator()+allList.at(i));
        letterDir.setNameFilters(filters);
        chars.push_back(allList.at(i));
        QStringList letterList = letterDir.entryList(QDir::Files);
        for (int j=0;j<letterList.count();++j) {
            QString pathChar=path+QDir::separator()+allList.at(i)+QDir::separator()+letterList.at(j);
            QImage temp(pathChar);
            cv::Mat img(temp.height(),temp.width(),CV_8UC4,(uchar*)temp.bits(),temp.bytesPerLine());
            cvtColor(img, img,CV_RGB2GRAY);
            cv::Mat f20=features(img, 20);
            trainingDataf20.push_back(f20);
            trainingLabels.push_back(i);
        }
    }

    trainingDataf20.convertTo(trainingDataf20, CV_32FC1);
    cv::Mat(trainingLabels).copyTo(classes);

    cv::Mat layers(1,3,CV_32SC1);
    layers.at<int>(0)= trainingDataf20.cols;
    layers.at<int>(1)= 20;
    layers.at<int>(2)= numCharacters;
    ann.create(layers, CvANN_MLP::SIGMOID_SYM, 1, 1);

    //Prepare trainClases
    //Create a mat with n trained data by m classes
    cv::Mat trainClasses;
    trainClasses.create( trainingDataf20.rows, numCharacters, CV_32FC1 );
    for( int i = 0; i <  trainClasses.rows; i++ )
    {
        for( int k = 0; k < trainClasses.cols; k++ )
        {
            //If class of data i is same than a k class
            if( k == classes.at<int>(i) )
                trainClasses.at<float>(i,k) = 1;
            else
                trainClasses.at<float>(i,k) = 0;
        }
    }
    cv::Mat weights( 1, trainingDataf20.rows, CV_32FC1, cv::Scalar::all(1) );

    //Learn classifier
    ann.train( trainingDataf20, trainClasses, weights );
    ann.save(QString(dataPath+"/data/mlp_last.yml").toStdString().c_str());
    trained=true;
}

cv::Mat Trainer::features(cv::Mat in, int sizeData)
{
    //Histogram features
    cv::Mat vhist=ProjectedHistogram(in,VERTICAL);
    cv::Mat hhist=ProjectedHistogram(in,HORIZONTAL);

    //Low data feature
    cv::Mat lowData;
    cv::resize(in, lowData, cv::Size(sizeData, sizeData) );

    //    drawVisualFeatures(in, hhist, vhist, lowData);



    //Last 10 is the number of moments components
    int numCols=vhist.cols+hhist.cols+lowData.cols*lowData.cols;

    cv::Mat out=cv::Mat::zeros(1,numCols,CV_32F);
    //Asign values to feature
    int j=0;
    for(int i=0; i<vhist.cols; i++)
    {
        out.at<float>(j)=vhist.at<float>(i);
        j++;
    }
    for(int i=0; i<hhist.cols; i++)
    {
        out.at<float>(j)=hhist.at<float>(i);
        j++;
    }
    for(int x=0; x<lowData.cols; x++)
    {
        for(int y=0; y<lowData.rows; y++){
            out.at<float>(j)=(float)lowData.at<unsigned char>(x,y);
            j++;
        }
    }

    return out;
}

char Trainer::classify(cv::Mat f)
{
    cv::Mat output(1, 35, CV_32FC1);
    ann.predict(f, output);
    cv::Point maxLoc;
    double maxVal;
    minMaxLoc(output, 0, &maxVal, 0, &maxLoc);
    char znak=chars.at(maxLoc.x).toStdString().c_str()[0];
    allQuality+=maxVal;
    counterQuality++;
    return znak;
}

double Trainer::recognitionImage(const QString &pathImage, const QString &correctText)
{
    static int number=1;
    int countCorrectChars=0;
    QImage image(pathImage);
    int countLetterRecognized = image.width()/21;
    for (int i=0;i<countLetterRecognized;i++) {
        QImage temp = image.copy(i*21,0,20,20);
        cv::Mat img(temp.height(),temp.width(),CV_8UC4,(uchar*)temp.bits(),temp.bytesPerLine());
        cvtColor(img, img,CV_RGB2GRAY);

        cv::Mat f=features(img,20);
        char znak = classify(f);


        if (correctText.size()==countLetterRecognized) {
            QChar correctChar=correctText.at(i);
            if (QChar(znak) == correctText.at(i) || correctText.at(i)==QChar('*')) {
                countCorrectChars++;
            } else {
                QString path = dataPath+"/recognition/incorrectChars";
                QDir dir(path);
                dir.mkpath(correctChar);
                temp.save(path+QDir::separator()+QString(correctChar)+QDir::separator()+QString(correctChar)+QString("_%1.png").arg(number++));
            }
        }
    }
    if (countLetterRecognized==0)
        return 0.0;
    return 100.0*double(countCorrectChars)/countLetterRecognized;
}

double Trainer::getQuality()
{
    if (counterQuality==0)
        return 0.0;
    double quality=100.0*allQuality/counterQuality;
    allQuality=0.0;
    counterQuality=0;
    return quality;
}

cv::Mat Trainer::ProjectedHistogram(cv::Mat img, int t)
{
    int sz=(t)?img.rows:img.cols;
    cv::Mat mhist=cv::Mat::zeros(1,sz,CV_32F);

    for(int j=0; j<sz; j++){
        cv::Mat data=(t)?img.row(j):img.col(j);
        mhist.at<float>(j)=countNonZero(data);
    }

    //Normalize histogram
    double min, max;
    minMaxLoc(mhist, &min, &max);

    if(max>0)
        mhist.convertTo(mhist,-1 , 1.0f/max, 0);

    return mhist;
}
