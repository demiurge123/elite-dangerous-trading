QT       += core gui network printsupport widgets
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = EliteDangerousTrading
TEMPLATE = app
VERSION_APP = 0.7.2

DEFINES += APP_VERSION=\\\"$$VERSION_APP\\\"

APP_BASE = ..
DESTDIR = $$APP_BASE/EliteDangerousTrading

SOURCES += \
    main.cpp \
    preperationimages.cpp \
    preparationstaticimage.cpp \
    segmentationstaticimage.cpp \
    segmentationimages.cpp \
    recognitionimages.cpp \
    recognitionstaticimage.cpp \
    galactica.cpp \
    station.cpp \
    good.cpp \
    comboboxdelegate.cpp \
    screenshot.cpp \
    dialog.cpp \
    screenimage.cpp \
    fields.cpp \
    trainer.cpp \
    trading.cpp

HEADERS += \
    preperationimages.h \
    commandInterface.h \
    preparationstaticimage.h \
    segmentationstaticimage.h \
    segmentationimages.h \
    recognitionimages.h \
    recognitionstaticimage.h \
    recognitionInterface.h \
    galactica.h \
    station.h \
    good.h \
    comboboxdelegate.h \
    screenshot.h \
    dialog.h \
    screenimage.h \
    fields.h \
    trainer.h \
    trading.h

FORMS += \
    dialog.ui

RESOURCES += \
    resources.qrc

TRANSLATIONS = languages/EliteDangerousTrading_en.ts \
               languages/EliteDangerousTrading_pl.ts

include(../library.pri)

