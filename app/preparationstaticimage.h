#pragma once
#include <QObject>
#include <QSharedPointer>
#include <QImage>
#include <QRect>
#include <opencv2/opencv.hpp>
#include "recognitionInterface.h"


namespace Recognition {


class PreparationStaticImage : public QObject,
        public RecognitionInterface
{
    Q_OBJECT
    Q_INTERFACES(Recognition::RecognitionInterface)
public:
    explicit PreparationStaticImage(QObject *parent = 0);

    void crop();
    void distort();
    void invert();
    void curve();
    void clear();

    void threshold() {}
    void countour() {}
    void rectangle() {}

    void extract() {}

    QImage &getImage();
    QHash<QString,QString> getData() const { return QHash<QString,QString>(); }
    QString getDataText() { return QString(); }

    void setImage(const QString &filename);
    void setImage(const QImage & image);

private:
    cv::Mat m_image;
    QImage tempImage;
};

}

