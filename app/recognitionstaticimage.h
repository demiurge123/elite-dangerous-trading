#pragma once
#include <QObject>
#include <QImage>
#include <QMap>
#include <QStringList>
#include <opencv2/opencv.hpp>
#include "recognitionInterface.h"
#include "trainer.h"

namespace Recognition {


class RecognitionStaticImage : public QObject,
        public RecognitionInterface
{
    Q_OBJECT
    Q_INTERFACES(Recognition::RecognitionInterface)
public:
    explicit RecognitionStaticImage(QObject *parent = 0);
    void crop() {}
    void distort() {}
    void invert() {}
    void curve() {}
    void clear() {}

    void threshold() {}
    void countour() {}
    void rectangle() {}

    void extract();

    const QImage & getImage();
    QHash<QString,QString> getData() const { return m_data; }
    QString getDataText();

    void setFilenameImage(const QString &filenameImage);
    void setImage(const QImage & image);


    double getProcentRecognition() const;
private:
    QString dataPath;

    Trainer *m_trainer;

    QImage m_image;
    QString m_filename;

    QHash<QString,QString> m_data;
    QImage tempImage;


signals:
    void qualityRecognition(const double &procent);
};

}

