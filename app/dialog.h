#pragma once

#include <QtWidgets/QDialog>
#include <QFile>
#include <QtWidgets/QGraphicsScene>
#include <QtWidgets/QTableWidgetItem>
#include <QtWidgets/QTreeWidgetItem>
#include <QBasicTimer>
#include "galactica.h"
#include "trading.h"
#include "screenshot.h"
#include "screenimage.h"
#include "preperationimages.h"
#include "segmentationimages.h"
#include "recognitionimages.h"


namespace Ui {
class Dialog;
}

class Dialog : public QDialog
{
    Q_OBJECT

public:
    explicit Dialog(QWidget *parent = 0);
    bool loadGalactica(const QString& filename);
    bool saveGalactica(const QString& filename) const;
    ~Dialog();

    QImage getImage(Recognition::Command *command, const QImage & image, const QString &nameField);
    void setBackgroundColor(QString nameColor);
    void setWidthColumn();
    void settingInternet();
    void createData();
    void setupRecognition();
    void setupSceneGraphics();
    void sendData(const QHash<QString, QString> &data);
    void readSettings();
    void writeSettings();

    void downloadMarket(const int &index);
    void downloadAllMarkets();
    void clearTrading();
private:
    Ui::Dialog *ui;
    Galactica *galactica;
    Trading *trading;
    QList<QTreeWidgetItem *> items;

    QGraphicsScene *scene;
    QBasicTimer timer,timerChangeSettings;
#ifdef Q_OS_WIN32
    Screenshot *m_screenshot;
#endif
    ScreenImage *m_screenImage;
    QSize m_size;
    QImage tempImage;
    QHash<QString,QString> tempData;
    QString station, goods, buy, sell, supply;

    QString dataPath;

    Recognition::PreperationImages *prepare;
    Recognition::SegmentationImages *segmentation;
    Recognition::RecognitionImages *recognition;

    QTableWidgetItem *itemStation;
    QTableWidgetItem *itemGoods;
    QTableWidgetItem *itemBuy;
    QTableWidgetItem *itemSell;
    QTableWidgetItem *itemSupply;

    int currentCargo;

    QString internetAddress;
    quint16 internetPort;
    QNetworkAccessManager manager;
    QNetworkRequest request;

    QString _resolution;

protected:
    void timerEvent(QTimerEvent * event);
private slots:
    void currentIndexComboBox(const int &index);
    void captureImage(const bool &isCapture);
    void addressChanged(const QString &address);
    void portChanged(const int &port);
    void resolutionChanged(int index);
    void respond(QNetworkReply* reply);
    void qualityRecognition(const double &procent);
    void qualityScreenshotChanged(const int &value);
    void hiperspaceJumpsChanged(const int &value);
    void computeTrading();
    void starshipChanged(const QString& name);
    void exportToPDF();
};

