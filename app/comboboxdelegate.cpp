#include <QtWidgets/QApplication>
#include "comboboxdelegate.h"
#include <QPainter>
#include <QDebug>
ComboBoxDelegate::ComboBoxDelegate(QObject *parent)
    : QItemDelegate(parent)
{
}

void ComboBoxDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    painter->save();

    if (index.isValid()) {
        QStringList data = index.data( Qt::DisplayRole ).toString().split("|");
        QString system = data.at(0).trimmed();
        QString station = data.at(1).trimmed();


        if (option.state & QStyle::State_Selected)
        {
            painter->fillRect(option.rect, option.palette.highlight());
            painter->setPen(option.palette.highlightedText().color());
        }

        QFont czcionka= painter->font();
        czcionka.setBold(false);
        painter->setFont(czcionka);
        painter->drawText(option.rect,Qt::AlignLeft, station);
        czcionka.setBold(true);
        painter->setFont(czcionka);
        painter->drawText(option.rect,Qt::AlignRight, system);

    } else
        QItemDelegate::paint( painter, option, index );
    painter->restore();
}

