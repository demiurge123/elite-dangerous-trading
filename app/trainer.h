#pragma once
#include <QObject>
#include <QVector>
#include <opencv2/opencv.hpp>

class Trainer
{
public:
    Trainer(bool trained=false);
    void loadTrain();
    void loadChars();
    void trainLetters(const QString &path);
    cv::Mat ProjectedHistogram(cv::Mat img, int t);
    cv::Mat features(cv::Mat in, int sizeData);
    char classify(cv::Mat f);
    double recognitionImage(const QString &pathImage, const QString &correctText);
    double getQuality();
private:
    QString dataPath;
    QVector<QString> chars;
    CvANN_MLP  ann;
    CvKNearest knnClassifier;
    bool trained;
    enum DIRECTION {VERTICAL, HORIZONTAL};
    double allQuality;
    int counterQuality;
};

