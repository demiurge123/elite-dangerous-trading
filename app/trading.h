#pragma once
#include <QObject>
#include <QStringList>
#include <QVector>
#include "galactica.h"

class Profit {
private:
    QString commodity;
    int buyPrice;
    int sellPrice;
    int supply;
public:
    Profit(const QString &commodity,const int &buyPrice, const int &sellPrice, const int &supply);
    QString nameCommodity() const { return commodity; }
    int value() const { return sellPrice-buyPrice; }
    int getBuyPrice() const;
    void setBuyPrice(int value);
    int getSellPrice() const;
    void setSellPrice(int value);
    int getSupply() const;
    void setSupply(int value);
};

class Trading : public QObject
{
    Q_OBJECT
public:
    Trading(Galactica *galactica);
    QStringList computeBestTrades(const int &idStartStation, int &capital, quint32 &maxProfit, const int &jumps);

    void setCargo(const int &cargo);

    void initialProfits();
    const QMap<QPair<int, int>,Profit*> getProfits() const { return m_profits; }
    
private:

    QList<Profit *> possibleProfits(const int &start, const int &end);

    Galactica *galactica;
    quint16 maxCargo;
    QMap<QPair<int, int>,Profit*> m_profits;
    QList<int> m_listPrices;

    QMap<QPair<int, int>, QPair<int, QPair<int,QList<int> > > > m_connections;

    void newKnapsackTrading(const int &start, const int &end, const int &cargo);

    int minMaxProfit(int level, int &idStation, int capital);
    int getProfit(const int &start, const int &end, const int &capital);
    QList<int> getCommodities(const int &start, const int &end, const int &capital);
    QStringList bestTradingFor3More(int &capital, quint32 &maxProfit, int start, const int &jumps);
    QStringList bestTradingFor1(int &capital, quint32 &maxProfit, int start, const int &jumps);
};


