#pragma once
#include <QObject>
#include <QJsonObject>
#include <QHash>
#include <QPolygon>
#include <QRect>
#include <QVector>

enum AREA {STATION,GOODS};

class Field {
public:
    QRect cropArea;
    QSize sizeDest;
    QPolygon anchorsSource;
    QPolygon anchorsDest;

    QList<QRect> segmentationAreas;

    QString name;
public:
    Field(const QString &nameField=QString());


    bool exists() const { return !name.isEmpty();}
    void read(const QJsonObject &json);
    void write(QJsonObject &json) const;

    QString getName() const {return name;}
    void setName(const QString &name) {this->name=name;}
    QRect getCropArea() const {return cropArea;}
    void setCropArea(const QRect &rect) {this->cropArea=rect;}
    QSize getSizeDest() const {return sizeDest;}
    void setSizeDest(const QSize &size) {this->sizeDest=size;}
    QPolygon getAnchorsSource() const {return anchorsSource;}
    void setAnchorsSource(const QPolygon &polygon) {this->anchorsSource=polygon;}
    QPolygon getAnchorsDest() const {return anchorsDest;}
    void setAnchorsDest(const QPolygon &polygon) {this->anchorsDest=polygon;}

    QList<QRect> getSegmentationAreas() const {return segmentationAreas; }
    void addSegmentationArea(const QRect &rect);
    void delSegmentationArea(const QRect &rect);
};

class Fields  : public QObject
{
    Q_OBJECT
private:
    Q_DISABLE_COPY(Fields)
    Fields();

    static Fields* m_Instance;
    QHash<QString,Field> fields;
    QString nameField;

    void read(const QJsonObject &json);
    void write(QJsonObject &json) const;

    QString dataPath;
public:
    static Fields* getInstance()
    {
        static QMutex mutex;
        if (!m_Instance)
        {
            mutex.lock();

            if (!m_Instance)
                m_Instance = new Fields;

            mutex.unlock();
        }

        return m_Instance;
    }

    Field current() const;
    void setCurrent(const QString &name);
    bool loadFields(const QString& filename);
    bool saveFields(const QString& filename) const;

    Field getField(const QString &name) const;
    void addField(const Field &field);
    void delField(const Field &field);
    QStringList getAllFieldName() const;
    void createField(const QString &name);
    void deleteField(const QString &name);
    void modifyField(const QString &name, const QString &parametr, const QString &value);
    QStringList getAllResolution();
};



