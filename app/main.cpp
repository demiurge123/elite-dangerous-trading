#include "dialog.h"
#include <QtWidgets/QApplication>
#include <QTranslator>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    a.setApplicationVersion(APP_VERSION);


    QTranslator appTranslator;
    QString locale = QLocale::system().name();
    locale.truncate(locale.lastIndexOf('_'));
    appTranslator.load("languages/EliteDangerousTrading_"+locale);
    a.installTranslator(&appTranslator);

    Dialog dialog;
    dialog.setWindowFlags(Qt::WindowStaysOnTopHint);
    dialog.setAttribute(Qt::WA_ShowWithoutActivating);
    dialog.show();
    return a.exec();
}
