#pragma once
#include <QObject>
#include "commandInterface.h"
#include "segmentationstaticimage.h"


namespace Recognition {


class SegmentationImages : public QObject,
        public Command
{
    Q_OBJECT
    Q_INTERFACES(Recognition::Command)
public:
    explicit SegmentationImages(QObject *parent = 0);
    virtual ~SegmentationImages();
    void execute();
    void setFileNameObject(const QString &filename);
    void setImageObject(const QImage & image);
    SegmentationStaticImage* getStaticImage() const {return m_staticImage;}
private:
    SegmentationStaticImage *m_staticImage;
};

}
