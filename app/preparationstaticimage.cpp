#include <QPainter>
#include <QDebug>

#include "preparationstaticimage.h"
#include "fields.h"

using namespace Recognition;

PreparationStaticImage::PreparationStaticImage(QObject *parent) :
    QObject(parent)
{

}

void PreparationStaticImage::crop()
{
    QRect rect=Fields::getInstance()->current().getCropArea();
    cv::Rect myROI(rect.x(),rect.y(),rect.width(),rect.height());
    m_image=m_image(myROI);

//    QImage test((uchar*) m_image.data, m_image.cols, m_image.rows, (int)m_image.step, QImage::Format_RGB32);
//    test.save("test_crop.png");
}

void PreparationStaticImage::distort()
{
    QSize size=Fields::getInstance()->current().getSizeDest();

    cv::Mat target(size.height(), size.width(), m_image.type());

    std::vector<cv::Point2f> target_points;
    QPolygon dest=Fields::getInstance()->current().getAnchorsDest();
    for(int i=0;i<dest.size();++i) {
        QPoint point=dest.at(i);
        target_points.push_back(cv::Point2f(point.x(),point.y()));
    }

    std::vector<cv::Point2f> source_points;
    QPolygon source=Fields::getInstance()->current().getAnchorsSource();
    for(int i=0;i<source.size();++i) {
        QPoint point=source.at(i);
        source_points.push_back(cv::Point2f(point.x(),point.y()));
    }

    cv::Mat const trans_mat = cv::getPerspectiveTransform(source_points,
                                                          target_points);
    cv::warpPerspective(m_image, target, trans_mat, target.size());

    target.copyTo(m_image);

//    QImage test((uchar*) m_image.data, m_image.cols, m_image.rows, (int)m_image.step, QImage::Format_RGB32);
//    test.save("test_distort.png");
}

void PreparationStaticImage::invert()
{
    int height,width,step,channels;
    uchar *data;

    height=m_image.rows;
    width=m_image.cols;
    step=(int)m_image.step;
    channels=m_image.channels();
    data=m_image.data;
    for(int i=0;i<height;i++){
        for(int j=0;j<width;j++){
            for(int k=0;k<channels;k++){
                if (k<3)
                    data[i*step + j*channels + k]=255-data[i*step + j*channels + k];
            }
        }
    }

//    QImage test((uchar*) m_image.data, m_image.cols, m_image.rows, (int)m_image.step, QImage::Format_RGB32);
//    test.save("test_invert.png");
}

void PreparationStaticImage::curve()
{
    cv::Mat target(m_image.cols,m_image.rows,m_image.type());

    double alpha=4.0; // 256.0/(indentRight - indentLeft)
    int beta = -768; // -(indentLeft* alpha)
    m_image.convertTo(target, -1, alpha, beta);

    target.copyTo(m_image);

//    QImage test((uchar*) m_image.data, m_image.cols, m_image.rows, (int)m_image.step, QImage::Format_RGB32);
//    test.save("test_curve.png");
}

void PreparationStaticImage::clear()
{
    int height,width,step,channels;
    uchar *data;

    height=m_image.rows;
    width=m_image.cols;
    step=(int)m_image.step;
    channels=m_image.channels();
    data=m_image.data;
    for(int i=0;i<height;i++){
        for(int j=0;j<width;j++){
            int b=data[i*step + j*channels + 0];
            int g=data[i*step + j*channels + 1];
            int r=data[i*step + j*channels + 2];
            int gray=(b+g+r)/3;

            int max=std::max(std::max(std::abs(b-g),std::abs(b-r)),std::abs(g-r));

            if (max>25 ) {
                data[i*step + j*channels + 0]=255;
                data[i*step + j*channels + 1]=255;
                data[i*step + j*channels + 2]=255;
            } else {
                if (gray<160) {
                    data[i*step + j*channels + 0]=gray;
                    data[i*step + j*channels + 1]=gray;
                    data[i*step + j*channels + 2]=gray;
                } else {
                    data[i*step + j*channels + 0]=255;
                    data[i*step + j*channels + 1]=255;
                    data[i*step + j*channels + 2]=255;
                }
            }
        }
    }

//    QImage test((uchar*) m_image.data, m_image.cols, m_image.rows, (int)m_image.step, QImage::Format_RGB32);
//    test.save("test_clear.png");
}

QImage & PreparationStaticImage::getImage()
{
    QImage img((uchar*) m_image.data, m_image.cols, m_image.rows, (int)m_image.step, QImage::Format_RGB32);
    tempImage = img.copy();
    return tempImage;
}

void PreparationStaticImage::setImage(const QString &filename)
{
    QImage tmp(filename);
    cv::Mat src(tmp.height(),tmp.width(),CV_8UC4,(uchar*)tmp.bits(),tmp.bytesPerLine());
    src.copyTo(m_image);
}

void PreparationStaticImage::setImage(const QImage & image)
{
    cv::Mat src(image.height(),image.width(),CV_8UC4,(uchar*)image.bits(),image.bytesPerLine());
    src.copyTo(m_image);
}


