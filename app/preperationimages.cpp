#include <QSharedPointer>
#include "preperationimages.h"

using namespace Recognition;

Recognition::PreperationImages::PreperationImages(const QString & nameArea, QObject *parent)
    : QObject(parent)
{
    m_staticImage = new PreparationStaticImage();
    this->nameArea = nameArea;
}

void Recognition::PreperationImages::execute()
{
    m_staticImage->crop();
    m_staticImage->distort();
    m_staticImage->invert();
    m_staticImage->curve();
    m_staticImage->clear();
}

void PreperationImages::setFileNameObject(const QString &filename)
{
    m_staticImage->setImage(filename);
}

void PreperationImages::setImageObject(const QImage & image)
{
    m_staticImage->setImage(image);
}
