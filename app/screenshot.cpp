#include "screenshot.h"
#ifdef Q_OS_WIN32
HWND Screenshot::wHandle;

DWORD getProcess() {

    DWORD aProcesses[1024], cbNeeded, cProcesses;
    unsigned int i;
    DWORD pid=0;

    if ( EnumProcesses( aProcesses, sizeof(aProcesses), &cbNeeded ) )
    {
        cProcesses = cbNeeded / sizeof(DWORD);
        for ( i = 0; i < cProcesses; i++ )
        {
            if( aProcesses[i] != 0 )
            {
                TCHAR szProcessName[MAX_PATH] = TEXT("<unknown>");
                HANDLE hProcess = OpenProcess( PROCESS_QUERY_INFORMATION |
                                               PROCESS_VM_READ,
                                               FALSE, aProcesses[i] );
                if (NULL != hProcess )
                {
                    HMODULE hMod;
                    DWORD cbNeeded;

                    if ( EnumProcessModules( hProcess, &hMod, sizeof(hMod),
                                             &cbNeeded) )
                    {
                        GetModuleBaseName( hProcess, hMod, szProcessName,
                                           sizeof(szProcessName)/sizeof(TCHAR) );
                    }
                }
                QString nazwa=QString::fromWCharArray(szProcessName);
                if (nazwa.compare("EliteDangerous.exe",Qt::CaseInsensitive)==0) {
                    pid=aProcesses[i];
                }

                CloseHandle( hProcess );
            }
        }
    }
    return pid;
}



BOOL CALLBACK MyEnumProc(HWND hWnd, LPARAM lParam)
{
    TCHAR title[500];
    ZeroMemory(title, sizeof(title));


    GetWindowText(hWnd, title, sizeof(title)/sizeof(title[0]));


    QString tytul = QString::fromWCharArray(title);
    if(tytul.compare("Elite - Dangerous (CLIENT)",Qt::CaseInsensitive)==0)
    {
        Screenshot::wHandle = hWnd;
        return FALSE;
    }
    return TRUE;
}

void Screenshot::getPixmap()
{
    HBITMAP bitmap = getWindow();

    BITMAP bitmap_info;
    memset(&bitmap_info, 0, sizeof(BITMAP));

    const int res = GetObject(bitmap, sizeof(BITMAP), &bitmap_info);
    if (!res) {
        qErrnoWarning("QPixmap::fromWinHBITMAP(), failed to get bitmap info");
        return ;
    }
    const int w = bitmap_info.bmWidth;
    const int h = bitmap_info.bmHeight;

    BITMAPINFO bmi;
    memset(&bmi, 0, sizeof(bmi));
    bmi.bmiHeader.biSize        = sizeof(BITMAPINFOHEADER);
    bmi.bmiHeader.biWidth       = w;
    bmi.bmiHeader.biHeight      = -h;
    bmi.bmiHeader.biPlanes      = 1;
    bmi.bmiHeader.biBitCount    = 32;
    bmi.bmiHeader.biCompression = BI_RGB;
    bmi.bmiHeader.biSizeImage   = w * h * 4;

    // Get bitmap bits
    QScopedArrayPointer<uchar> data(new uchar[bmi.bmiHeader.biSizeImage]);
    HDC display_dc = GetDC(0);
    if (!GetDIBits(display_dc, bitmap, 0, h, data.data(), &bmi, DIB_RGB_COLORS)) {
        ReleaseDC(0, display_dc);
        qWarning("%s, failed to get bitmap bits", __FUNCTION__);
        return;
    }

    QImage::Format imageFormat = QImage::Format_ARGB32_Premultiplied;
    uint mask = 0;
    //    if (hbitmapFormat == HBitmapNoAlpha) {
    imageFormat = QImage::Format_RGB32;
    mask = 0xff000000;
    //    }

    // Create image and copy data into image.
    QImage image(w, h, imageFormat);
    if (image.isNull()) { // failed to alloc?
        ReleaseDC(0, display_dc);
        qWarning("%s, failed create image of %dx%d", __FUNCTION__, w, h);
        return ;
    }
    const int bytes_per_line = w * sizeof(QRgb);
    for (int y = 0; y < h; ++y) {
        QRgb *dest = (QRgb *) image.scanLine(y);
        const QRgb *src = (const QRgb *) (data.data() + y * bytes_per_line);
        for (int x = 0; x < w; ++x) {
            const uint pixel = src[x];
            if ((pixel & 0xff000000) == 0 && (pixel & 0x00ffffff) != 0)
                dest[x] = pixel | 0xff000000;
            else
                dest[x] = pixel | mask;
        }
    }
    ReleaseDC(0, display_dc);
    m_image=image.copy();

    DeleteObject(bitmap);
    ReleaseDC(0, display_dc);

}

HBITMAP Screenshot::getWindow()
{
    RECT r;
    GetClientRect(wHandle, &r);

    int width = r.right - r.left;
    int height = r.bottom - r.top;

    // Create and setup bitmap
    HDC display_dc = GetDC(0);
    HDC bitmap_dc = CreateCompatibleDC(display_dc);
    HBITMAP bitmap = CreateCompatibleBitmap(display_dc, width, height);
    HGDIOBJ null_bitmap = SelectObject(bitmap_dc, bitmap);

    // copy data
    HDC window_dc = GetDC(wHandle);
    BitBlt(bitmap_dc, 0, 0, width, height, window_dc, 0, 0, SRCCOPY | CAPTUREBLT);

    // clean up all but bitmap
    ReleaseDC(wHandle, window_dc);
    SelectObject(bitmap_dc, null_bitmap);
    DeleteDC(bitmap_dc);

    return bitmap;
}


Screenshot::Screenshot()
{
    EnumWindows(MyEnumProc, 0);
}

QImage Screenshot::getScreenshot()
{
    getPixmap();
    return m_image;
}

bool Screenshot::online()
{
    return getProcess()>0;
}

#endif
