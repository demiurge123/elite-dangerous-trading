#include "recognitionimages.h"
#include "recognitionstaticimage.h"

using namespace Recognition;

RecognitionImages::RecognitionImages(QObject *parent)
    : QObject(parent)
{
    m_staticImage = new RecognitionStaticImage(parent);
    connect(m_staticImage,SIGNAL(qualityRecognition(double)),this,SIGNAL(qualityRecognition(double)));
}

void RecognitionImages::execute()
{
    m_staticImage->extract();
}

void RecognitionImages::setFileNameObject(const QString &filename)
{
    m_staticImage->setFilenameImage(filename);
}

void RecognitionImages::setImageObject(const QImage & image)
{
    m_staticImage->setImage(image);
}

double RecognitionImages::getProcentRecognition() const
{
    return m_staticImage->getProcentRecognition();
}



