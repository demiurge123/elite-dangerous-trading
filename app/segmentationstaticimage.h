#pragma once
#include <vector>
#include <QObject>
#include <QSharedPointer>
#include <QImage>
#include <QHash>
#include <opencv2/opencv.hpp>
#include "recognitionInterface.h"
#include "fields.h"

namespace Recognition {

class SegmentationStaticImage : public QObject,
        public RecognitionInterface
{
    Q_OBJECT
    Q_INTERFACES(Recognition::RecognitionInterface)
public:
    explicit SegmentationStaticImage(QObject *parent = 0);

    void crop() {}
    void distort() {}
    void invert() {}
    void curve() {}
    void clear() {}

    void threshold();
    void countour();
    void rectangle();

    void extract() {}

    const QImage &getImage();
    QHash<QString,QString> getData() const { return QHash<QString,QString>(); }
    QString getDataText() { return QString(); }

    void setImage(const QString &filename);
    void setImage(const QImage & image);

private:
    bool segmentationOK;
    QString dataPath;
    cv::Mat m_image;
    cv::Mat m_origin;
    std::vector< std::vector< cv::Point> > contours;

    bool verifyPosition(const cv::Rect &rect) const;
    bool verifySize(const cv::Rect &rect) const;
    cv::Mat preprocessChar(const cv::Mat &in);

    void addRectAndImage(cv::Rect mr);

    QList<QRect> segmentationAreas;
    QList<QRect> rects;
    QHash<QRect,QImage> images;
    QImage result;
};

}

