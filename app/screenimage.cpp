#include "screenimage.h"

#include <QPainter>

ScreenImage::ScreenImage()
{
}

ScreenImage::~ScreenImage()
{

}

QRectF ScreenImage::boundingRect() const
{
    return m_image.rect();
}

void ScreenImage::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(widget)
    Q_UNUSED(option)
    if (m_image.width()>0 && m_image.height()>0 ) {
        painter->drawImage(-237,-234,m_image.scaledToWidth(474));
    }

    if (gd_image.width()>0 && gd_image.height()>0 ) {
        painter->drawImage(-60,-40,gd_image.scaledToWidth(300));
    }
    if (st_image.width()>0 && st_image.height()>0 ) {
        painter->drawImage(-237,120,st_image.scaledToWidth(474));
    }
}

void ScreenImage::setImage(const QImage & image)
{
    m_image=image.copy();
}

void ScreenImage::setSegmentationImages(const QImage & stationImage, const QImage & goodsImage)
{
    st_image=stationImage.copy();
    gd_image=goodsImage.copy();
}

void ScreenImage::load()
{

}

void ScreenImage::save() const
{

}
