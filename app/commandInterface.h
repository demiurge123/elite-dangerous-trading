#pragma once
#include <QObject>
#include "recognitionInterface.h"
#include "fields.h"

namespace Recognition {

class Command {
public:
    virtual void execute() = 0;
    virtual void setFileNameObject(const QString &filename) = 0;
    virtual void setImageObject(const QImage & image) = 0;
    virtual RecognitionInterface* getStaticImage() const = 0;

    QString getNameAREA() const {return nameArea;}

protected:
    QString nameArea;
};

}
Q_DECLARE_INTERFACE(Recognition::Command, "pl.ibohatyrewicz.EliteDangerousTrading.Command")
