win32 {
    OPENCV_DIR = c:/opencv/build

    INCLUDEPATH += $$OPENCV_DIR/include

        !contains(QMAKE_HOST.arch, x86_64) {
            contains(QMAKE_COMPILER_DEFINES, _MSC_VER=1600) {
                LIBS += -L$$OPENCV_DIR/x86/vc10/lib
            }
            contains(QMAKE_COMPILER_DEFINES, _MSC_VER=1700) {
                LIBS += -L$$OPENCV_DIR/x86/vc11/lib
            }
            contains(QMAKE_COMPILER_DEFINES, _MSC_VER=1800) {
                LIBS += -L$$OPENCV_DIR/x86/vc12/lib
            }
        } else {
            contains(QMAKE_COMPILER_DEFINES, _MSC_VER=1600) {
                LIBS += -L$$OPENCV_DIR/x64/vc10/lib
            }
            contains(QMAKE_COMPILER_DEFINES, _MSC_VER=1700) {
                LIBS += -L$$OPENCV_DIR/x64/vc11/lib
            }
            contains(QMAKE_COMPILER_DEFINES, _MSC_VER=1800) {
                LIBS += -L$$OPENCV_DIR/x64/vc12/lib
            }
        }
    CONFIG(debug, debug|release) {
        DEFINES += _DEBUG

        LIBS += -lopencv_core249d -lopencv_imgproc249d -lopencv_ml249d -lpsapi
    } else {
        DEFINES += NDEBUG
       
        LIBS += -lopencv_core249 -lopencv_imgproc249 -lopencv_ml249 -lpsapi
    }
}

mac {
    INCLUDEPATH += /usr/local/Cellar/opencv/2.4.9/include
    LIBS += -L/usr/local/lib -lopencv_core -lopencv_imgproc -lopencv_ml
}
